import CollectionScreen from '@/components/collection'

const Collection = ({ params }: { params: { id: string } }) => {
  return (
    <main className='flex min-h-screen flex-col items-center justify-start p-4 md:px-8 md:py-0'>
      <CollectionScreen params={params} />
    </main>
  )
}

export default Collection
