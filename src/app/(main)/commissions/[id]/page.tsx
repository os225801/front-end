import Avatar from '@/components/shared/avatar'
import Image from 'next/image'
import Link from 'next/link'

const CommissionDetails = () => {
  return (
    <main className='flex h-full min-h-screen w-full flex-col items-center justify-start p-4 md:p-8'>
      <div className='w-full md:px-8'>
        <Link
          className='flex w-fit items-center gap-2 rounded-full bg-[#f8f7f4] px-5 py-[10px] text-sm font-semibold text-[#0d0c22] transition-all hover:opacity-80'
          href='/commissions'
        >
          <div className='h-[13px] w-[13px]'>
            <svg
              xmlns='http://www.w3.org/2000/svg'
              enableBackground='new 0 0 24 24'
              viewBox='0 0 24 24'
              role='img'
              className='icon fill-current'
            >
              <path d='m13.532 4.585-7.532 7.415 7.532 7.415c.792.779 2.081.779 2.873 0s.792-2.049 0-2.829l-4.659-4.586 4.659-4.587c.384-.378.595-.88.595-1.414s-.211-1.036-.595-1.414c-.792-.78-2.082-.78-2.873 0z'></path>
            </svg>
          </div>
          All Jobs
        </Link>{' '}
      </div>
      <div className='mt-8 flex h-full w-full flex-col justify-center pb-8 md:max-w-6xl md:flex-row'>
        {/* Left */}
        <div className='flex w-full max-w-[612px] flex-1 flex-col  self-center px-4'>
          <>
            <h1 className='line-clamp-5 text-3xl font-semibold'>Comm des full body </h1>
            <div className='inline-block items-center pt-2'>
              <span className='shrink-0 text-sm font-semibold'>
                Customer&nbsp;
                <Link href='/profile/NgyNiccccc'>
                  <span className='underline'>NgyNiccccc</span>
                </Link>
              </span>
              <span className='shrink-0 px-1 text-sm'>·</span>
              <span className='shrink-0 text-sm'>17 days ago</span>
              <span className='shrink-0 px-1 text-sm'>·</span>
              <span className='text-sm font-semibold'>
                <Link href='/commissions'>
                  <span className='underline'>Commissions</span>
                </Link>
              </span>
            </div>
          </>
          {/* Body */}
          <div className='flex w-full items-center justify-between py-6'>
            <h2 className='text-2xl font-medium'>Brief Information</h2>
          </div>
          <div className='flex flex-col'>
            <span className='pb-1 text-sm font-semibold'>Kinh phí</span>
            <span className='whitespace-pre-wrap pb-4 text-sm'>₫150,000&nbsp;-&nbsp;₫1,000,000</span>
          </div>
          <div className='flex flex-col'>
            <span className='text-m pb-1 font-semibold'>Nội dung</span>
            <p className='whitespace-pre-wrap text-sm'>
              We are building the infrastructure for decentralized AI development at scale. We aggregate global compute
              and enable researchers to collaboratively train state-of-the-art models through distributed training
              across clusters.
            </p>
          </div>
          <div className='mt-12 w-full'>
            <div className='grid grid-cols-3 gap-2'>
              <div className='flex aspect-square flex-shrink-0 flex-col items-center overflow-hidden rounded-xl'>
                <div className='group relative flex h-full w-full flex-col'>
                  <Image
                    className='h-auto w-full'
                    src='https://img.daisyui.com/images/stock/photo-1534528741775-53994a69daeb.jpg'
                    alt='Laundry App - Dark Mode'
                    fill
                    sizes='(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 33vw'
                    priority
                    style={{ objectFit: 'cover' }}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* Right */}
        <div className='ml-[60px] min-w-[336px] max-w-[360px]'>
          <div className='mb-12 box-border w-full rounded-lg border border-[#e7e7e9] p-8'>
            <div className='flex flex-col items-center justify-center'>
              <Avatar
                src='https://img.daisyui.com/images/stock/photo-1534528741775-53994a69daeb.jpg'
                width={72}
                height={72}
                className='mb-4 w-[72px]'
              />
              <h1 className='text-[24px] font-bold leading-[29px]'>Prime Intellect</h1>
              <Link className='mt-2 text-sm' target='_blank' rel='noopener noreferrer' href='/profile/1'>
                Visit Website
              </Link>
              <button className='mt-4 rounded-full bg-base-content px-5 py-[10px] text-[13px] font-semibold leading-[13px] text-base-100 transition-all hover:opacity-80'>
                Apply for this job
              </button>
              <div className='divider my-8'></div>
              <div className='w-full'>
                <div className='text-sm'>Deadline</div>
                <div className='mt-2 text-base font-medium'>Flexible</div>
              </div>
              <div className='mt-6 w-full'>
                <div className='text-sm'>Purpose</div>
                <div className='mt-2 text-base font-medium'>Non-commercial</div>
              </div>
              <div className='mt-6 w-full'>
                <div className='text-sm'>Job Type</div>
                <div className='mt-2 text-base font-medium'>Full-time</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>
  )
}

export default CommissionDetails
