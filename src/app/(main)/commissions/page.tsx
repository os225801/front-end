import CommissionsScreen from '@/components/commissions'

const Commisions = () => {
  return (
    <main className='flex min-h-screen flex-col items-center justify-start p-4 md:px-8 md:py-0'>
      <CommissionsScreen />
    </main>
  )
}

export default Commisions
