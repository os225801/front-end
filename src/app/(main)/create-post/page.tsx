import CreatePostScreen from '@/components/create-post'

const CreatePost = () => {
  return (
    <main className='flex min-h-screen flex-col items-center justify-start p-4 md:px-8 md:py-0'>
      <CreatePostScreen />
    </main>
  )
}

export default CreatePost
