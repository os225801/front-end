import CreatePostScreenV2 from '@/components/create-post/CreatePostScreenV2'

const CreatePostV2 = () => {
  return (
    <main className='flex min-h-screen flex-col items-center justify-start p-4 md:px-8 md:py-0'>
      <CreatePostScreenV2 />
    </main>
  )
}

export default CreatePostV2
