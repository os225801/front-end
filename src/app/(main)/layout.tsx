import Footer from '@/components/shared/footer'
import { Header } from '@/components/shared/header'
import React from 'react'

const MainLayout = ({
  children,
}: Readonly<{
  children: React.ReactNode
}>) => {
  return (
    <div>
      <Header />
      {children}
      <Footer />
    </div>
  )
}

export default MainLayout
