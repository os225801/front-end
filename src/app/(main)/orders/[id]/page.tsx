import { OrderScreen } from '@/components/order'

export default async function Order({ params }: { params: { id: string } }) {
  return <OrderScreen orderId={params.id} />
}
