import Banner from '@/components/banner'
import HomeScreen from '@/components/home'

export default function Home() {
  return (
    <main className='flex w-full flex-col gap-4'>
      <Banner />
      <div className='flex min-h-screen flex-col items-center justify-start p-4 md:px-8 md:py-0'>
        <HomeScreen />
      </div>
    </main>
  )
}
