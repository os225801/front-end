import DetailPost from '@/components/post/detail'
import PostRecommendation from '@/components/post/post-recommendation'
import { commentService } from '@/services/comment.service'
import { postService } from '@/services/post.service'
import { userService } from '@/services/user.service'
import { dehydrate, HydrationBoundary, QueryClient } from '@tanstack/react-query'

const Demo = async ({ params }: { params: { id: string } }) => {
  const queryClient = new QueryClient()

  await queryClient.prefetchInfiniteQuery({
    queryKey: ['comments', params.id],
    queryFn: async () => {
      const posts = await commentService.getComments({ postId: params.id, pageNumber: 1, pageSize: 10 })
      return posts
    },
    initialPageParam: 1,
    getNextPageParam: (lastPage: { hasNextPage: boolean; pageNumber: number }) => {
      return lastPage?.hasNextPage ? lastPage?.pageNumber + 1 : undefined
    },
  })

  await queryClient.prefetchQuery({
    queryKey: ['current-user'],
    queryFn: async () => {
      const currentUser = await userService.getCurrentUser()
      return currentUser
    },
  })

  await queryClient.prefetchQuery({
    queryKey: ['post', params.id],
    queryFn: async () => {
      const postInfo = await postService.getPostInfo({
        postId: params.id,
      })
      return postInfo
    },
  })

  return (
    <HydrationBoundary state={dehydrate(queryClient)}>
      <div className='mx-auto min-h-screen w-full flex-grow lg:max-w-[1248px] xl:max-w-[1440px]'>
        <div className='mt-[3px] w-full px-4 sm:mt-0 sm:px-6'>
          <DetailPost params={params} />
          <PostRecommendation params={params} />
        </div>
      </div>
    </HydrationBoundary>
  )
}

export default Demo
