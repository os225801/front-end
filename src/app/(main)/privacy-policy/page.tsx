const PrivacyPolicy = () => {
  return (
    <div className='flex min-h-screen flex-col items-center justify-start p-4 md:px-8 md:py-0'>
      <div className='mx-auto max-w-3xl p-6'>
        <h1 className='mb-4 text-3xl font-bold'>Chính sách Bảo mật</h1>

        <p>
          Chính sách Bảo mật này mô tả cách chúng tôi thu thập, xử lý và công khai dữ liệu cá nhân của người dùng nếu họ
          chọn sử dụng website{' '}
          <a href='https://omnistroke.yp2743.me' className='text-blue-500 hover:underline'>
            https://omnistroke.yp2743.me
          </a>
          . Bằng cách sử dụng website{' '}
          <a href='https://omnistroke.yp2743.me' className='text-blue-500 hover:underline'>
            https://omnistroke.yp2743.me
          </a>
          , bạn đồng ý với việc thu thập và xử lý dữ liệu được mô tả trong Chính sách này. Các thuật ngữ được dùng trong
          Chính sách Bảo mật này có ý nghĩa tương tự như trong Điều khoản sử dụng, trừ khi được định nghĩa khác trong
          Chính sách này.
        </p>

        <h2 className='mb-4 mt-6 text-xl font-semibold'>Về việc thu thập và sử dụng dữ liệu</h2>

        <p>
          Để đem đến trải nghiệm tốt hơn khi dùng ứng dụng, chúng tôi có thể yêu cầu bạn cung cấp các thông tin nhận
          dạng cá nhân, bao gồm nhưng không giới hạn địa chỉ email, số điện thoại, tên, ngày sinh. Chúng tôi không bán,
          trao đổi hoặc cho thuê thông tin nhận dạng cá nhân của người dùng cho người khác. Tuy nhiên, chúng tôi có sử
          dụng dịch vụ của các bên thứ ba để phát triển ứng dụng và có thể chia sẻ thông tin của bạn với các bên thứ ba
          với mục đích vận hành và phát triển ứng dụng.
        </p>

        <p>
          Ngoài ra, chúng tôi có thể liên hệ với bạn bằng những thông tin mà bạn cung cấp để thu thập những đánh giá, ý
          kiến và quan điểm của bạn về ứng dụng.
        </p>

        <h3 className='mb-4 mt-6 text-lg font-semibold'>Đường link tới các bên thứ ba mà ứng dụng sử dụng:</h3>

        <ul className='list-disc pl-6'>
          <li>Google Analytics</li>
          <li>Auth0</li>
        </ul>

        <h2 className='mb-4 mt-6 text-xl font-semibold'>Nhật ký dữ liệu (Log Data)</h2>

        <p>
          Khi website bị lỗi, chúng tôi sẽ thu thập dữ liệu và thông tin (thông qua dịch vụ của một bên thứ ba) trên
          thiết bị di động của bạn thông qua Nhật ký dữ liệu (Log Data). Nhật ký dữ liệu có thể bao gồm thông tin như
          địa chỉ IP của thiết bị, tên thiết bị, hệ điều hành, cấu hình thiết bị khi sử dụng ứng dụng, thời gian truy
          cập vào ứng dụng và các dữ liệu thống kê khác.
        </p>

        <h2 className='mb-4 mt-6 text-xl font-semibold'>Về việc sử dụng cookies</h2>

        <p>
          Cookie là những thư mục dữ liệu được lưu trữ tạm thời hoặc lâu dài trong ổ cứng máy tính của Khách hàng.
          Website dùng cookies trực tiếp, và bạn có thể chấp nhận hoặc từ chối sử dụng những cookies này. Nếu bạn từ
          chối sử dụng cookies, bạn có thể không sử dụng được một số phần nhất định của website.
        </p>

        <h2 className='mb-4 mt-6 text-xl font-semibold'>Về việc sử dụng các nhà cung cấp dịch vụ</h2>

        <p>Chúng tôi có thể sử dụng dịch vụ của các bên thứ ba vì những lý do sau:</p>

        <ul className='list-disc pl-6'>
          <li>Để phát triển ứng dụng của chúng tôi;</li>
          <li>Để cung cấp dịch vụ thay mặt chúng tôi;</li>
          <li>Để thực hiện các dịch vụ liên quan, hoặc</li>
          <li>Để hỗ trợ chúng tôi phân tích thông tin thu thập bởi ứng dụng</li>
        </ul>

        <p>
          Các bên thứ ba này có quyền truy cập vào thông tin cá nhân của bạn với mục đích cung cấp dịch vụ cho chúng
          tôi. Tuy nhiên, các bên thứ ba có nghĩa vụ không tiết lộ hoặc sử dụng thông tin của bạn cho bất kỳ mục đích
          nào khác.
        </p>

        <h2 className='mb-4 mt-6 text-xl font-semibold'>Bảo mật</h2>

        <p>
          Chúng tôi đã triển khai các tiêu chuẩn chung được công nhận về bảo mật công nghệ và vận hành để bảo vệ các dữ
          liệu cá nhân khỏi bị mất, lạm dụng, thay đổi hoặc tiêu hủy. Chỉ những người có thẩm quyền trong đội ngũ của
          chúng tôi và các nhà cung cấp dịch vụ bên thứ ba của chúng tôi mới được cấp quyền truy cập vào dữ liệu cá nhân
          của bạn. Các nhân viên cùng các nhà cung cấp dịch vụ bên thứ ba này được yêu cầu bảo mật thông tin này. Dù đã
          áp dụng các biện pháp phòng ngừa, nhưng chúng tôi không thể bảo đảm 100% những người không có thẩm quyền không
          thể truy cập vào dữ liệu cá nhân của bạn.
        </p>
        <h2 className='mb-4 mt-6 text-xl font-semibold'>Liên kết đến các trang web khác</h2>

        <p>
          Website này có thể chứa các liên kết đến các trang web khác. Nếu bạn nhấp vào liên kết của bên thứ ba, bạn sẽ
          được dẫn đến trang web đó. Lưu ý rằng các trang web bên ngoài này không do chúng tôi điều hành. Do đó, chúng
          tôi khuyến khích bạn xem lại Chính sách Bảo mật của các trang web này. Chúng tôi không kiểm soát và không chịu
          trách nhiệm về nội dung, chính sách bảo mật hoặc cách vận hành của bất kỳ trang web hoặc dịch vụ của bên thứ
          ba nào.
        </p>

        <h2 className='mb-4 mt-6 text-xl font-semibold'>Quyền riêng tư của trẻ em</h2>

        <p>
          Website này không liên quan đến bất kỳ ai dưới 13 tuổi. Chúng tôi không cố ý thu thập thông tin nhận dạng cá
          nhân từ trẻ em dưới 13 tuổi. Trong trường hợp chúng tôi phát hiện rằng trẻ em dưới 13 tuổi đã cung cấp thông
          tin cá nhân cho chúng tôi, chúng tôi sẽ ngay lập tức xóa thông tin này khỏi máy chủ của chúng tôi. Nếu bạn là
          cha mẹ hoặc người giám hộ và bạn biết rằng con bạn đã cung cấp thông tin cá nhân cho chúng tôi, vui lòng liên
          hệ với chúng tôi để chúng tôi có thể thực hiện các hành động cần thiết.
        </p>

        <h2 className='mb-4 mt-6 text-xl font-semibold'>Xoá tài khoản OmniStorke của bạn</h2>

        <p>
          Bạn có thể xoá tài khoản OmniStorke của mình bất cứ lúc nào, bằng cách liên hệ địa chỉ email{' '}
          <a href='mailto:support@omnistroke.yp2743.me' className='text-blue-500 hover:underline'>
            support@omnistroke.yp2743.me
          </a>
          . Khi xoá tài khoản, tất cả thông tin và dữ liệu của bạn trong quá trình sử dụng OmniStorke sẽ được xoá khỏi
          hệ thống dữ liệu và máy chủ của chúng tôi.
        </p>

        <h2 className='mb-4 mt-6 text-xl font-semibold'>Các thay đổi đối với Chính sách Bảo mật này</h2>

        <p>
          Chúng tôi có thể cập nhật Chính sách Bảo mật của chúng tôi theo thời gian. Do đó, bạn nên xem lại Chính sách
          này định kỳ để cập nhật các thay đổi mới.
        </p>

        <p className='mt-6'>Chính sách Bảo mật này có hiệu lực từ ngày 01/6/2024.</p>

        <h2 className='mb-4 mt-6 text-xl font-semibold'>Liên hệ chúng tôi</h2>

        <p>
          Nếu bạn có bất kỳ câu hỏi hoặc đề xuất nào về Chính sách Bảo mật của chúng tôi, vui lòng liên hệ với chúng tôi
          theo địa chỉ email:{' '}
          <a href='mailto:support@omnistroke.yp2743.me' className='text-blue-500 hover:underline'>
            support@omnistroke.yp2743.me
          </a>
          .
        </p>
      </div>
    </div>
  )
}

export default PrivacyPolicy
