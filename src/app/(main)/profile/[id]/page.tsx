import ProfileScreen from '@/components/profile'

const Profile = ({ params }: { params: { id: string } }) => {
  return (
    <main className='flex min-h-screen flex-col items-center justify-start p-4 md:px-8 md:py-0'>
      <ProfileScreen params={params} />
    </main>
  )
}

export default Profile
