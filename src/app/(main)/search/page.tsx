import SearchScreen from '@/components/search'

const Search = () => {
  return (
    <main className='flex min-h-screen flex-col items-center justify-start'>
      <SearchScreen />
    </main>
  )
}

export default Search
