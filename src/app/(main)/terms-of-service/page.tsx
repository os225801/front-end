const TermsOfService = () => {
  return (
    <div className='flex min-h-screen flex-col items-center justify-start p-4 md:px-8 md:py-0'>
      <div className='container mx-auto bg-base-200 p-6'>
        <div
          className='relative mb-6 h-64 w-full bg-cover bg-center'
          style={{ backgroundImage: "url('https://source.unsplash.com/random/1920x1080')" }}
        >
          <div className='absolute inset-0 bg-black opacity-50'></div>
          <div className='relative z-10 flex h-full items-center justify-center'>
            <h1 className='text-4xl font-bold text-white'>Điều khoản Mua Bán Tranh Ảnh Digital trên Omnistroke</h1>
          </div>
        </div>

        <div className='rounded-lg p-4 shadow-md'>
          <section className='mb-6'>
            <h2 className='mb-2 text-3xl font-semibold text-primary'>Tổng quan</h2>
            <p className=''>
              Chỉ người dùng đã đăng ký mới có thể mua và bán trên Omnistroke. Việc đăng ký tài khoản là miễn phí và
              người dùng phải cung cấp thông tin chính xác, đầy đủ và cập nhật. Người dùng chịu trách nhiệm cho hoạt
              động của tài khoản và bảo mật mật khẩu. Omnistroke không chịu trách nhiệm cho bất kỳ hành động hoặc sự bất
              tín nhiệm nào của người dùng liên quan đến tài khoản của họ.
            </p>
          </section>
          <section className='mb-6'>
            <h2 className='mb-2 text-3xl font-semibold text-primary'>Người bán (Họa sĩ)</h2>
            <ul className='list-inside list-disc '>
              <li>Người bán có thể đăng bán các tác phẩm tranh ảnh digital trên nền tảng Omnistroke.</li>
              <li>
                Người bán phải cung cấp mô tả chính xác về tác phẩm, bao gồm cả giá cả và các điều kiện sử dụng (ví dụ:
                sử dụng cá nhân hoặc thương mại).
              </li>
              <li>Omnistroke sẽ giữ lại một khoản phí dịch vụ từ mỗi giao dịch bán hàng.</li>
              <li>
                Thu nhập của Người bán sẽ được ghi nhận vào tài khoản của họ sau khi giao dịch hoàn tất và được xác nhận
                bởi Người mua.
              </li>
            </ul>
          </section>
          <section className='mb-6'>
            <h2 className='mb-2 text-3xl font-semibold text-primary'>Người mua</h2>
            <ul className='list-inside list-disc '>
              <li>Người mua có thể duyệt và mua các tác phẩm tranh ảnh digital trên Omnistroke.</li>
              <li>Người mua phải thanh toán cho tác phẩm trước khi tải xuống hoặc nhận tác phẩm từ Người bán.</li>
              <li>
                Mọi giao dịch thanh toán phải được thực hiện thông qua nền tảng Omnistroke. Việc đề nghị thanh toán bên
                ngoài nền tảng là không được phép.
              </li>
              <li>
                Khi mua một tác phẩm, Người mua sẽ nhận được quyền sử dụng theo điều kiện được mô tả bởi Người bán. Một
                số tác phẩm có thể yêu cầu phí bổ sung cho Giấy phép Sử dụng Thương mại.
              </li>
            </ul>
          </section>
          <section className='mb-6'>
            <h2 className='mb-2 text-3xl font-semibold text-primary'>Quyền Sở hữu và Sử dụng</h2>
            <ul className='list-inside list-disc '>
              <li>
                Người mua được cấp quyền sử dụng đầy đủ cho tác phẩm đã mua trừ khi có thỏa thuận khác từ Người bán.
              </li>
              <li>
                Nếu Người mua muốn sử dụng tác phẩm cho mục đích thương mại, họ có thể phải trả thêm phí cho Giấy phép
                Sử dụng Thương mại.
              </li>
              <li>
                Việc sở hữu bản quyền và quyền công bố tác phẩm sẽ được chuyển nhượng cho Người mua sau khi thanh toán
                đầy đủ.
              </li>
            </ul>
          </section>
          <section className='mb-6'>
            <h2 className='mb-2 text-3xl font-semibold text-primary'>Chính sách Hủy và Hoàn Tiền</h2>
            <p className=''>
              Omnistroke khuyến khích Người mua và Người bán cố gắng giải quyết mọi xung đột trực tiếp. Nếu không thể
              giải quyết, người dùng có thể liên hệ với Bộ phận Hỗ trợ khách hàng của Omnistroke để được hỗ trợ. Điều
              khoản về hủy giao dịch và hoàn tiền có thể được tham khảo trong phần Điều khoản Thanh toán của Omnistroke.
            </p>
          </section>
          <section className='mb-6'>
            <h2 className='mb-2 text-3xl font-semibold text-primary'>Quy định về Nội dung và Vi phạm</h2>
            <ul className='list-inside list-disc '>
              <li>
                Omnistroke có quyền loại bỏ các tác phẩm và/hoặc người dùng vi phạm Điều khoản Dịch vụ và Tiêu chuẩn
                Cộng đồng của nền tảng.
              </li>
              <li>
                Vi phạm có thể bao gồm: dịch vụ bất hợp pháp hoặc gian lận, vi phạm bản quyền, nội dung không thích hợp
                hoặc khiêu dâm, sao chép cố ý các tác phẩm khác, và các hành vi vi phạm khác.
              </li>
              <li>Các tài khoản vi phạm có thể bị tạm ngừng hoạt động hoặc bị xóa vĩnh viễn khỏi nền tảng.</li>
            </ul>
          </section>
          <section className='mb-6'>
            <h2 className='mb-2 text-3xl font-semibold text-primary'>Chính sách Riêng tư</h2>
            <p className=''>
              Omnistroke cam kết bảo vệ quyền riêng tư của người dùng. Chi tiết về chính sách riêng tư có thể được tìm
              thấy trong Chính sách Riêng tư của Omnistroke.
            </p>
          </section>
          <section className='mb-6'>
            <h2 className='mb-2 text-3xl font-semibold text-primary'>Tiêu chuẩn Cộng đồng</h2>
            <p className=''>
              Người dùng cam kết tuân thủ Tiêu chuẩn Cộng đồng của Omnistroke, bao gồm các quy tắc và hướng dẫn về hành
              vi áp dụng cho cộng đồng và thị trường Omnistroke.
            </p>
          </section>
        </div>
      </div>
    </div>
  )
}

export default TermsOfService
