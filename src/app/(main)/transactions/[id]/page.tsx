'use client'

import { Button } from '@/components/shared/button'
import { transactionService } from '@/services/transactions.service'
import { useQuery } from '@tanstack/react-query'
import Link from 'next/link'

const Transaction = ({ params }: { params: { id: string } }) => {
  const fetchTransaction = async () => {
    const transaction = await transactionService.getTransactionById(params.id)
    return transaction as Transaction
  }

  const { data } = useQuery({
    queryKey: ['transactions', params.id],
    queryFn: fetchTransaction,
  })

  return (
    <div className='my-[24px] min-h-[calc(100vh-186px)]'>
      <article className='mx-auto flex max-w-[574.56px] flex-col gap-[32px] rounded-[39.969px] px-[40px] py-[24px] shadow-lg'>
        <div className='flex flex-col items-center gap-[24px]'>
          <svg width='94' height='94' viewBox='0 0 94 94' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <circle cx='46.9996' cy='46.631' r='46.631' fill='#23A26D' fillOpacity='0.12' />
            <path
              d='M46.1671 24.4258C33.9321 24.4258 23.9619 34.3959 23.9619 46.631C23.9619 58.8661 33.9321 68.8362 46.1671 68.8362C58.4022 68.8362 68.3724 58.8661 68.3724 46.631C68.3724 34.3959 58.4022 24.4258 46.1671 24.4258ZM56.7812 41.5238L44.1909 54.1142C43.88 54.425 43.4581 54.6027 43.014 54.6027C42.5699 54.6027 42.148 54.425 41.8371 54.1142L35.553 47.8301C34.9091 47.1861 34.9091 46.1203 35.553 45.4763C36.197 44.8324 37.2628 44.8324 37.9068 45.4763L43.014 50.5835L54.4275 39.1701C55.0714 38.5261 56.1373 38.5261 56.7812 39.1701C57.4252 39.814 57.4252 40.8577 56.7812 41.5238Z'
              fill='#23A26D'
            />
          </svg>

          <div className='flex flex-col items-center gap-3'>
            <h3 className='text-center text-[24px] leading-[40px]'>Payment Success!</h3>
            <h2 className='text-center text-[32px] font-semibold leading-[52px]'>{data?.amount} VND</h2>
          </div>
        </div>
        <div className='h-[1.665px] w-full'></div>
        <div className='flex flex-col gap-[24px]'>
          <div className='flex flex-col gap-[16px]'>
            <div className='flex w-full items-center justify-between'>
              <span className='text-[16px] leading-[30px]'>Transaction ID</span>
              <span className='text-[16px] font-medium leading-[30px]'>{data?.id}</span>
            </div>
            <div className='flex w-full items-center justify-between'>
              <span className='text-[16px] leading-[30px]'>Payment Time</span>
              <span className='text-[16px] font-medium leading-[30px]'>
                {new Date(data?.transactionDate!).toLocaleString('en-US', {
                  weekday: 'short',
                  day: 'numeric',
                  month: 'short',
                  year: 'numeric',
                  timeZone: 'Asia/Bangkok',
                  hour: 'numeric',
                  minute: '2-digit',
                  hour12: true,
                })}
              </span>
            </div>
            <div className='flex w-full items-center justify-between'>
              <span className='text-[16px] leading-[30px]'>Payment Method</span>
              <span className='text-[16px] font-medium leading-[30px]'>Bank Transfer</span>
            </div>
          </div>
          <div className='h-[1.665px] w-full'></div>
          <div className='flex w-full items-center justify-between'>
            <Link href='/'>
              <Button variant='white'>Go back to homepage</Button>
            </Link>
          </div>
        </div>
      </article>
    </div>
  )
}

export default Transaction
