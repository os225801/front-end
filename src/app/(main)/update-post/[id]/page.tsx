import UpdatePostSection from '@/components/update-post/update-post-section'
import { postService } from '@/services/post.service'
import { HydrationBoundary, QueryClient, dehydrate } from '@tanstack/react-query'

const UpdatePost = async ({ params }: { params: { id: string } }) => {
  const queryClient = new QueryClient()

  await queryClient.prefetchQuery({
    queryKey: ['post', params.id],
    queryFn: async () => {
      const post = await postService.getPostById(params.id) // Pass an object with the userId property
      return post
    },
  })

  return (
    <HydrationBoundary state={dehydrate(queryClient)}>
      <UpdatePostSection id={params.id} />
    </HydrationBoundary>
  )
}

export default UpdatePost
