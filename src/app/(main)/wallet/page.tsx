import WalletScreen from '@/components/wallet'

const Wallet = () => {
  return (
    <main className='m-auto flex min-h-screen max-w-[1440px] flex-col items-center justify-start px-4 md:px-8'>
      <WalletScreen />
    </main>
  )
}

export default Wallet
