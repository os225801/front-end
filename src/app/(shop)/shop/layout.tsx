import { Header } from '@/components/shared/header'
import React from 'react'

const ShopLayout = ({
  children,
}: Readonly<{
  children: React.ReactNode
}>) => {
  return (
    <div>
      <Header />
      {children}
    </div>
  )
}

export default ShopLayout
