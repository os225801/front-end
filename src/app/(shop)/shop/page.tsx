import ShopScreen from '@/components/shop'

export default function Shop() {
  return (
    <main className='flex min-h-screen flex-col items-center justify-start'>
      <ShopScreen />
    </main>
  )
}
