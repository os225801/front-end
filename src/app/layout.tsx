import ClientThemeWrapper from '@/context/client-theme-wrapper'
import { SessionProvider } from '@/context/session-context'
import RootProvider from '@/lib/utils/providers'
import { GoogleAnalytics } from '@next/third-parties/google'
import type { Metadata } from 'next'
import 'photoswipe/dist/photoswipe.css'
import { Suspense } from 'react'
import { Toaster } from 'react-hot-toast'
import 'swiper/css'
import 'swiper/css/navigation'
import 'swiper/css/pagination'
import './globals.css'

export const metadata: Metadata = {
  // metadataBase: new URL(`${process.env.NEXT_APP_HOST}`),
  title: 'OmniStroke',
  description: 'Elevate Your Creativity, Monetize Your Passion',
  openGraph: {
    title: 'OmniStroke: Elevate Your Creativity, Monetize Your Passion',
    siteName: 'OmniStroke: Elevate Your Creativity, Monetize Your Passion',
    url: 'https://omnistroke.yp2743.me/',
    description: 'Elevate Your Creativity, Monetize Your Passion',
    type: 'website',
  },
}

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode
}>) {
  return (
    <html lang='en'>
      <body>
        <RootProvider>
          <SessionProvider>
            <ClientThemeWrapper>
              <Suspense>{children}</Suspense>
            </ClientThemeWrapper>
            <Toaster />
          </SessionProvider>
        </RootProvider>
        <GoogleAnalytics gaId='G-GELPVTCX4Z' />
      </body>
    </html>
  )
}
