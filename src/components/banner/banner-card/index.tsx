import { useRouter } from 'next/navigation'
type BannerCardProps = {
  id: string
  img_url: string
}

const BannerCard = ({ id, img_url }: BannerCardProps) => {
  const router = useRouter()
  return (
    <div onClick={() => router.push(`/posts/${id}`)} className='relative h-[300px] overflow-hidden rounded-lg'>
      <img src={img_url} alt='Slide' className='h-full w-full object-cover' />
    </div>
  )
}

export default BannerCard
