'use client'
import { useTopPosts } from '@/hooks/use-post-query'
import SwiperCore from 'swiper'
import { Autoplay, EffectCoverflow, Pagination } from 'swiper/modules'
import { Swiper, SwiperSlide } from 'swiper/react'
import BannerCard from './banner-card'

SwiperCore.use([EffectCoverflow, Pagination, Autoplay])

const Banner = () => {
  const { data, isLoading } = useTopPosts()

  return (
    <div className='w-full'>
      {isLoading ? (
        <div className='skeleton flex h-[300px] w-full items-center justify-center'></div>
      ) : (
        <Swiper
          effect={'coverflow'}
          // autoplay={{ delay: 2500 }}
          grabCursor={true}
          centeredSlides={true}
          loop={true}
          slidesPerView={3}
          coverflowEffect={{
            rotate: 30,
            stretch: 0,
            depth: 100,
            modifier: 1,
            slideShadows: true,
          }}
          pagination={true}
        >
          {data?.items?.slice(0, 5).map((art: Post, index: number) => (
            <SwiperSlide key={index}>
              <BannerCard id={art?.id} img_url={art?.artwork?.image} />
            </SwiperSlide>
          ))}
        </Swiper>
      )}
    </div>
  )
}

export default Banner
