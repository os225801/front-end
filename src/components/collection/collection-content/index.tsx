import { Card } from '@/components/shared/card'
import { Fragment } from 'react'

const CollectionContent = ({ posts }: { posts: Posts }) => {
  return (
    <div className='shots-grid w-full pt-10 md:px-[40px] md:pt-[72px]'>
      {posts?.items?.map((post) => (
        <Fragment key={post.id}>
          <Card {...post} />
        </Fragment>
      ))}
    </div>
  )
}

export default CollectionContent
