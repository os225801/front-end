import Avatar from '@/components/shared/avatar'
import { useToast } from '@/components/shared/toast'
import { useDeleteCollection } from '@/hooks/use-collection-query'
import { getSession } from '@/lib/utils/getSession'
import { useCollectionStoreActions } from '@/store/collection.store'

const CollectionHeader = ({ data }: { data: CollectionDetail }) => {
  const { showToast } = useToast()
  const { setIsOpenUpdateCollectionModal } = useCollectionStoreActions()
  const session = getSession()
  const { deleteCurrentCollection } = useDeleteCollection()
  const copyLinkToClipboard = () => {
    navigator.clipboard.writeText(window.location.href)
    showToast('Link copied to clipboard', 'success')
  }

  const currentUser = JSON.parse(session?.user || '{}')

  return (
    <div className='w-full md:px-[40px]'>
      <div className='flex flex-col justify-between md:flex-row'>
        <h1 className='mr-[40px] max-w-[736px] text-[24px] font-bold leading-[29px] md:text-[48px] md:leading-[52px]'>
          {data?.collectionName}
        </h1>
        <button
          onClick={copyLinkToClipboard}
          className='btn hidden h-[40px] min-h-[40px] w-fit min-w-[100px] flex-col rounded-full px-5 py-[10px] md:flex'
        >
          <div className='max-h-[13px] w-full max-w-[13px]'>
            <svg
              xmlns='http://www.w3.org/2000/svg'
              enableBackground='new 0 0 24 24'
              viewBox='0 0 24 24'
              role='img'
              className='icon fill-current text-base-content'
            >
              <path d='m7.438 16.562c.293.293.677.44 1.061.44s.768-.147 1.061-.44l7.002-7.002c.586-.586.586-1.536 0-2.122s-1.536-.586-2.122 0l-7.002 7.002c-.586.586-.586 1.536 0 2.122zm3.501 3.078c-1.813 1.814-4.765 1.814-6.58 0-1.814-1.814-1.814-4.765 0-6.579l3.29-3.29-2.121-2.121-3.29 3.29c-2.984 2.984-2.984 7.839 0 10.823 1.492 1.491 3.452 2.237 5.412 2.237s3.92-.746 5.411-2.238l3.29-3.29-2.122-2.122zm10.823-17.402c-2.983-2.984-7.839-2.984-10.823 0l-3.29 3.29 2.122 2.122 3.29-3.29c.907-.907 2.098-1.36 3.289-1.36s2.383.454 3.29 1.361c1.814 1.814 1.814 4.765 0 6.579l-3.29 3.29 2.122 2.122 3.29-3.29c2.984-2.985 2.984-7.84 0-10.824z'></path>
            </svg>
          </div>
          <span>Copy</span>
        </button>
      </div>
      <div className='mt-3 text-sm md:mt-5'>1 Shot</div>
      <div className='mt-3 text-sm md:mt-5'>{data?.description}</div>
      <div className='mt-3 flex items-center md:mt-4'>
        <Avatar src={data?.avatarLink} width={24} height={24} className='mr-2' />
        <span className='text-[16px] font-medium leading-[22px]'>{data?.username}</span>
      </div>
      <button
        onClick={copyLinkToClipboard}
        className='btn mt-4 flex h-[40px] min-h-[40px] w-fit min-w-[100px] flex-col rounded-full px-5 py-[10px] md:hidden'
      >
        <div className='max-h-[13px] w-full max-w-[13px]'>
          <svg
            xmlns='http://www.w3.org/2000/svg'
            enableBackground='new 0 0 24 24'
            viewBox='0 0 24 24'
            role='img'
            className='icon fill-current text-base-content'
          >
            <path d='m7.438 16.562c.293.293.677.44 1.061.44s.768-.147 1.061-.44l7.002-7.002c.586-.586.586-1.536 0-2.122s-1.536-.586-2.122 0l-7.002 7.002c-.586.586-.586 1.536 0 2.122zm3.501 3.078c-1.813 1.814-4.765 1.814-6.58 0-1.814-1.814-1.814-4.765 0-6.579l3.29-3.29-2.121-2.121-3.29 3.29c-2.984 2.984-2.984 7.839 0 10.823 1.492 1.491 3.452 2.237 5.412 2.237s3.92-.746 5.411-2.238l3.29-3.29-2.122-2.122zm10.823-17.402c-2.983-2.984-7.839-2.984-10.823 0l-3.29 3.29 2.122 2.122 3.29-3.29c.907-.907 2.098-1.36 3.289-1.36s2.383.454 3.29 1.361c1.814 1.814 1.814 4.765 0 6.579l-3.29 3.29 2.122 2.122 3.29-3.29c2.984-2.985 2.984-7.84 0-10.824z'></path>
          </svg>
        </div>
        <span>Copy</span>
      </button>
      {data?.userId === currentUser?.id && (
        <div className='flex w-full items-center justify-start gap-4 md:justify-end'>
          <button
            onClick={() => {
              if (confirm('Are you sure you want to delete this collection?')) {
                deleteCurrentCollection(data.id)
              }
            }}
            className='btn mt-4 flex h-[40px] min-h-[40px] w-fit min-w-[100px] flex-col rounded-full px-5 py-[10px]'
          >
            <span>Delete</span>
          </button>
          <button
            onClick={() => {
              setIsOpenUpdateCollectionModal(true)
            }}
            className='btn btn-outline mt-4 flex h-[40px] min-h-[40px] w-fit min-w-[100px] flex-col rounded-full px-5 py-[10px]'
          >
            <span>Update</span>
          </button>
        </div>
      )}
    </div>
  )
}

export default CollectionHeader
