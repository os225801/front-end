'use client'
import CollectionModal from '@/components/shared/card/collection-modal'
import Empty from '@/components/shared/list-posts/empty'
import { useCollectionDetail } from '@/hooks/use-collection-query'
import { usePostsInCollection } from '@/hooks/use-post-query'
import CollectionContent from '../collection-content'
import CollectionHeader from '../collection-header'
import UpdateCollectionModal from '../update-collection-modal'

const CollectionSection = ({ params }: { params: { id: string } }) => {
  const { data, isError } = useCollectionDetail({ collectionId: params.id })
  const { data: posts } = usePostsInCollection(params.id)

  if (isError) return <Empty />

  return (
    <>
      <CollectionHeader data={data} />
      <CollectionContent posts={posts} />
      <CollectionModal />
      <UpdateCollectionModal />
    </>
  )
}

export default CollectionSection
