import { collectionService } from '@/services/collection.service'
import { HydrationBoundary, QueryClient, dehydrate } from '@tanstack/react-query'
import CollectionSection from './collection-section'

const CollectionScreen = async ({ params }: { params: { id: string } }) => {
  const queryClient = new QueryClient()

  await queryClient.prefetchQuery({
    queryKey: ['collections', params.id],
    queryFn: async () => {
      const user = await collectionService.getCollectionDetail({ collectionId: String(params.id) })
      return user
    },
  })
  return (
    <HydrationBoundary state={dehydrate(queryClient)}>
      <CollectionSection params={params} />
    </HydrationBoundary>
  )
}

export default CollectionScreen
