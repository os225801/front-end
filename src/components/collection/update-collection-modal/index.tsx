import FormFieldInput from '@/components/shared/form-input'
import Textarea from '@/components/shared/form/textarea'
import Modal from '@/components/shared/modal'
import { useToast } from '@/components/shared/toast'
import { CreateCollectionSchema } from '@/schema/create-collection.schema'
import { collectionService } from '@/services/collection.service'
import {
  useCollectionStoreActions,
  useFormCreateCollection,
  useIsOpenUpdateCollectionModal,
} from '@/store/collection.store'
import { zodResolver } from '@hookform/resolvers/zod'
import { useQueryClient } from '@tanstack/react-query'
import { useParams } from 'next/navigation'
import { SubmitHandler, useForm } from 'react-hook-form'
import { z } from 'zod'

type FormFields = z.infer<typeof CreateCollectionSchema>

const UpdateCollectionModal = () => {
  const isOpen = useIsOpenUpdateCollectionModal()
  const { id } = useParams()
  const { setIsOpenUpdateCollectionModal } = useCollectionStoreActions()
  const formCreateCollection = useFormCreateCollection()
  const { showToast } = useToast()

  const close = () => {
    setIsOpenUpdateCollectionModal(false)
  }

  const {
    register,
    handleSubmit,
    reset,
    formState: { errors, isSubmitting },
  } = useForm<FormFields>({
    resolver: zodResolver(CreateCollectionSchema),
  })

  const queryClient = useQueryClient()
  const onSubmit: SubmitHandler<FormFields> = async (data) => {
    const payload = {
      name: data.title,
      description: data.description,
    }
    const newCollection = await collectionService.updateCollection({
      collectionId: id as string,
      data: payload,
    })
    if (newCollection) {
      queryClient.invalidateQueries({
        queryKey: ['collections'],
      })
      close()
      showToast('Collection updated successfully', 'success')
      reset()
    } else {
      return showToast('Failed to update collection', 'error')
    }
  }

  return (
    <Modal title={formCreateCollection ? 'Create a new collection' : 'Collection'} closeModal={close} isOpen={isOpen}>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className='relative mb-[25px] h-[300px] overflow-auto'>
          <FormFieldInput
            lable='Title'
            name='title'
            error={errors.title?.message}
            placeholder='Enter title'
            type='text'
            register={register}
          />
          <Textarea
            lable='Description'
            name='description'
            error={errors.description?.message}
            placeholder='Enter description'
            register={register}
          />
        </div>
        <div className='flex w-full items-center gap-2'>
          <button type='submit' disabled={isSubmitting} className='btn btn-outline'>
            {isSubmitting ? 'Loading...' : 'Update collection'}
          </button>
          <button type='button' onClick={close} className='btn btn-active'>
            Cancel
          </button>
        </div>
      </form>
    </Modal>
  )
}

export default UpdateCollectionModal
