'use client'
import Avatar from '@/components/shared/avatar'
import Image from 'next/image'
import Link from 'next/link'
import { useRouter } from 'next/navigation'

const CommissionsItem = () => {
  const router = useRouter()

  const handleNavigation = (url: string) => {
    router.push(url)
  }

  return (
    <Link href={`/commissions/1`}>
      <div className='border-greyscale-g3 hover:border-greyscale-g4 mb-4 w-full cursor-pointer break-inside-avoid-column rounded-xl border p-4 hover:z-10 hover:shadow-md'>
        {/* PC */}
        <div className='hidden flex-col md:flex'>
          <div className='flex flex-row pb-4'>
            <div className='flex w-full flex-col pr-4'>
              <h2 className='line-clamp-2 font-semibold'>Comm Logo</h2>
              <div className='flex items-center'>
                <span className='text-content-t300 text-xs'>1 day ago</span>
              </div>
              <div className='mt-4 line-clamp-5 break-words'>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
                standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it
                to make a type specimen book. It has survived not only five centuries, but also the leap into electronic
                typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of
                Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like
                Aldus PageMaker including versions of Lorem Ipsum. 🙇‍♀️
              </div>
            </div>
            <div className='relative block h-40 w-40 shrink-0 overflow-hidden rounded-lg'>
              <Image
                className='h-auto w-full rounded-lg '
                src='https://img.daisyui.com/images/stock/photo-1534528741775-53994a69daeb.jpg'
                alt='Laundry App - Dark Mode'
                fill
                sizes='(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 33vw'
                priority
                style={{ objectFit: 'cover' }}
              />
            </div>
          </div>
          <div className='flex flex-row items-end justify-between'>
            <div className='flex'>
              <Avatar
                src='https://img.daisyui.com/images/stock/photo-1534528741775-53994a69daeb.jpg'
                width={48}
                height={48}
                className='mr-2 w-12'
              />
              <div className='ml-3 flex flex-col space-y-1'>
                <div
                  onClick={(e) => {
                    e.stopPropagation()
                    handleNavigation('/profile/Metiour.253')
                  }}
                  className='flex flex-col'
                >
                  <span className='text-sm font-semibold hover:underline'>Metiour.253</span>
                </div>
                <div className='flex space-x-2'>
                  <span className='text-sm font-medium'>New Customer</span>
                </div>
              </div>
            </div>
            <span className='text-right text-2xl font-semibold'>₫100,000&nbsp;-&nbsp;₫500,000</span>
          </div>
        </div>

        {/* Mobile */}
        <div className='flex flex-col md:hidden'>
          <div className='flex'>
            <Avatar
              src='https://img.daisyui.com/images/stock/photo-1534528741775-53994a69daeb.jpg'
              width={40}
              height={40}
              className='w-10'
            />
            <div className='ml-2 flex flex-col'>
              <h2 className='line-clamp-2 font-semibold'>Comm Logo</h2>
              <div className='flex flex-row items-center space-x-1'>
                <div
                  className='flex'
                  onClick={(e) => {
                    e.stopPropagation()
                    handleNavigation('/profile/Metiour.253')
                  }}
                >
                  <span className='text-xs font-semibold hover:underline'>Metiour.253</span>
                </div>
                <span className='text-content-t200 text-xs'>• 0 đánh giá</span>
                <span className='text-content-t200 text-xs'>• một ngày trước</span>
              </div>
            </div>
          </div>
          <div className='flex flex-col'>
            <div
              className='relative mt-4 h-60 max-w-full shrink-0 overflow-hidden rounded-lg'
              style={{ aspectRatio: '1.77778 / 1' }}
            >
              <Image
                className='h-auto w-full rounded-lg '
                src='https://img.daisyui.com/images/stock/photo-1534528741775-53994a69daeb.jpg'
                alt='Laundry App - Dark Mode'
                fill
                sizes='(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 33vw'
                priority
                style={{ objectFit: 'cover' }}
              />
            </div>
            <div className='mt-4 line-clamp-5 break-words'>Tìm comm des được logo kiểu game như dưới ạ 🙇‍♀️</div>
            <span className='text-l text-content-t100 mt-3 w-full font-semibold'>
              <span className='text-m'>Kinh phí:</span> ₫100,000 &nbsp;-&nbsp;₫500,000
            </span>
          </div>
        </div>
      </div>
    </Link>
  )
}

export default CommissionsItem
