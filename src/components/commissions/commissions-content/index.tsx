import CommissionsItem from './commissions-item'

const CommisssionsContent = () => {
  return (
    <div className='mt-24 flex w-full flex-col justify-start'>
      <div className='md:columns-2'>
        <CommissionsItem />
        <CommissionsItem />
        <CommissionsItem />
      </div>
    </div>
  )
}

export default CommisssionsContent
