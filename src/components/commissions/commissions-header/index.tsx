const CommissionsHeader = () => {
  return (
    <div className='w-full'>
      <h2 className='text-[18px] font-bold leading-[24px]'>Commissions Market</h2>
      <h1 className='my-4 max-w-[600px] text-[32px] font-bold leading-[48px]'>
        Assists you in discovering the ideal commission you seek.
      </h1>
      <p className='text-neutra my-4 max-w-[900px]	text-[18px] leading-[28px]'>
        Our service connects you with the perfect artist for the commission you seek, offering personalized
        recommendations, comprehensive artist profiles, and expert guidance tailored to your specific preferences and
        requirements.
      </p>
      <button className='btn btn-neutral'>Create Commission</button>
    </div>
  )
}

export default CommissionsHeader
