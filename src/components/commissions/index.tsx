import CommisssionsContent from './commissions-content'
import CommissionsHeader from './commissions-header'

const CommissionsScreen = () => {
  return (
    <>
      <CommissionsHeader />
      <CommisssionsContent />
    </>
  )
}

export default CommissionsScreen
