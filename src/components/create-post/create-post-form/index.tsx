'use client'

import FormFieldInput from '@/components/shared/form-input'
import { useIsDisable } from '@/store/disable.store'
import { useEffect, useState } from 'react'
import { useFormContext } from 'react-hook-form'
import TagInput from '../tag-input'

const FormInput = () => {
  const { register, getValues, formState, setValue } = useFormContext()

  const [buyable, setBuyable] = useState<boolean>(!getValues('isBuyable'))

  const isDisable = useIsDisable()

  const { errors } = formState

  useEffect(() => {
    if (isNaN(getValues('price'))) {
      setValue('price', 0)
    }
  }, [getValues('isBuyable')])

  useEffect(() => {
    setValue('isBuyable', !buyable)
  }, [buyable])

  return (
    <div className='flex w-full flex-col items-start gap-3 md:max-w-[600px]'>
      <FormFieldInput
        lable='Title'
        name='title'
        error={errors.title?.message}
        placeholder='Enter title'
        type='text'
        register={register}
      />

      <div className='w-full'>
        <div className='label'>
          <span className='label-text'>Description</span>
        </div>
        <textarea
          disabled={isDisable}
          id=''
          rows={4}
          className='textarea textarea-bordered w-[95%] resize-none rounded-[16px] text-[16px]'
          placeholder='enter here ...'
          {...register('description', { required: true })}
        />
        {errors?.description && (
          <div className='label'>
            <span className='label-text-alt text-[12px] text-red-500'>{errors?.description?.message?.toString()}</span>
          </div>
        )}
      </div>

      <TagInput />

      <div className='form-control'>
        <label className='label cursor-pointer'>
          <input
            type='checkbox'
            className='toggle'
            disabled={isDisable}
            checked={buyable}
            onChange={() => {
              setBuyable(!buyable)
            }}
          />
          <span className='label-text ml-3'>Allow people dowload for free</span>
        </label>
      </div>

      {!buyable && (
        <FormFieldInput
          lable='Price'
          name='price'
          error={errors.price?.message}
          placeholder='enter price'
          type='number'
          register={register}
          valueAsNumber
          subfix='VND'
        />
      )}

      <div className='flex w-full justify-center px-5 py-3'>
        <button className='btn btn-outline' type='submit' disabled={isDisable}>
          {isDisable && <span className='loading loading-spinner'></span>}
          <div className='flex w-[100px] items-center justify-center gap-3'>Post</div>
        </button>
      </div>
    </div>
  )
}

export default FormInput
