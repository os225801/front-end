'use client'
import { cn } from '@/lib/utils/cn'
import { useCallback, useState } from 'react'
import { useDropzone } from 'react-dropzone'
import { useFormContext } from 'react-hook-form'

const ImageInput = () => {
  const [preview, setPreview] = useState<string | ArrayBuffer | null>(null)

  const {
    setValue,
    clearErrors,
    setError,
    formState: { errors },
  } = useFormContext()
  //30000000
  const onDrop = useCallback((acceptedFiles: Array<File>) => {
    const validExtensions = ['image/jpeg', 'image/png']
    if (!validExtensions.includes(acceptedFiles[0].type)) {
      setError('image', { message: 'Image must be formatted as jpeg or png' })
      return
    }
    if (acceptedFiles[0].size > 50000000) {
      setError('image', { message: 'Image must be less than 30Mb' })
      return
    }
    const file = new FileReader()
    file.onload = function () {
      setPreview(file.result)
    }
    file.readAsDataURL(acceptedFiles[0])
    clearErrors(['image'])

    setValue('image', acceptedFiles)
  }, [])

  const { acceptedFiles, getRootProps, getInputProps, isDragActive } = useDropzone({
    onDrop,
  })

  // const handleClearImg = () => {
  //   setPreview(null)
  // }

  return (
    <div className='relative flex max-w-[700px] flex-col items-center justify-center sm:w-full'>
      <div
        {...getRootProps()}
        className={cn(
          {
            'flex h-[450px] w-full min-w-[375px] max-w-[600px] flex-col items-center justify-center rounded-[32px] border-[2px] border-dashed border-[#0000001A]':
              true,
          },
          { 'h-min border-none': preview },
          { 'bg-gray-200': !preview }
        )}
      >
        <input {...getInputProps()} />
        {isDragActive
          ? !preview && <p className='font-inter text-[12px] font-[700] text-[#16173880]'>Drop here</p>
          : !preview && <p className='font-inter text-[12px] font-[700] text-[#16173880]'>Drop your image here</p>}
        {preview && (
          <div className='relative h-min max-h-[450px] border-[1px] border-dashed border-[#0000001A]'>
            <img className='h-min max-h-[450px]' src={preview as string} alt='Upload preview' />
          </div>
        )}
      </div>
      {errors?.image && (
        <div className='label'>
          <span className='label-text-alt text-[12px] text-red-500'>{errors?.image?.message?.toString()}</span>
        </div>
      )}
    </div>
  )
}

export default ImageInput
