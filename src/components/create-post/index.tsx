'use client'

import { CreatePostSchema } from '@/schema/create-post.schema'
import { artworkService } from '@/services/artwork.service'
import { postService } from '@/services/post.service'
import { useIsDisableStoreActions } from '@/store/disable.store'
import { zodResolver } from '@hookform/resolvers/zod'
import { useRouter } from 'next/navigation'
import { FormProvider, useForm } from 'react-hook-form'
import { useToast } from '../shared/toast'
import FormInput from './create-post-form'
import ImageInput from './image-input'

const CreatePostScreen = () => {
  const router = useRouter()
  const { showToast } = useToast()
  const { setIsDisable } = useIsDisableStoreActions()

  const param: CreatePostForm = {
    title: '',
    description: '',
    image: [],
    isBuyable: false,
    price: 0,
    tags: [],
  }

  const methods = useForm<CreatePostForm>({ defaultValues: param, resolver: zodResolver(CreatePostSchema) })
  const {
    handleSubmit,
    formState: { errors },
  } = methods

  const submitForm = async (form: CreatePostForm) => {
    setIsDisable(true)
    const data = await artworkService.uploadArtWork(form.image[0])

    if (!data || !data?.artworkId) {
      //handle error
      showToast('Can not upload image !', 'error')
      setIsDisable(false)
      return
    }

    const temptagsid: string[] = []

    form.tags.forEach((x: TagInput) => {
      temptagsid.push(x.id)
    })

    const payload: CreatePostPayload = {
      title: form.title,
      description: form.description,
      artworkId: data?.artworkId,
      isBuyable: form.isBuyable,
      price: form.price,
      tagIds: temptagsid,
    }

    const newPost = await postService.createPost(payload)

    if (!newPost) {
      showToast('Can not create new post !', 'error')
      setIsDisable(false)
      await artworkService.deleteArtwork(data?.artworkId)
      return
    }
    showToast('Create post successful ! Redirecting to post', 'success')
    setIsDisable(false)
    router.push(`/posts/${newPost.postId}`)
  }

  return (
    <div className='relative w-full'>
      <FormProvider {...methods}>
        <form className='mt-10 flex w-full flex-wrap items-start justify-evenly' onSubmit={handleSubmit(submitForm)}>
          <ImageInput />
          <FormInput />
        </form>
      </FormProvider>
    </div>
  )
}

export default CreatePostScreen
