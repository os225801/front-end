'use client'

import { useTags } from '@/hooks/use-tag-query'
import { useIsDisable } from '@/store/disable.store'
import { useEffect, useState } from 'react'
import { useFormContext } from 'react-hook-form'

const Tag = ({ tag, handleDeleteTag }: { tag: TagInput; handleDeleteTag: (tag: TagInput) => void }) => {
  const handleRemoveTag = () => {
    // const arr = [...selectedTag]

    // const index = arr.indexOf(tag)

    // arr.splice(index, 1)

    // setSelectedTag(arr)
    handleDeleteTag(tag)
  }

  return (
    <div className='inline-block'>
      <div className='flex min-h-9 items-center justify-center gap-2 rounded-full  bg-base-200 px-4 py-2 text-sm font-semibold text-base-content transition-all'>
        <p className='font-sans font-[400]'>{tag.tagName}</p>
        <div
          className='flex cursor-pointer items-center justify-center'
          onClick={() => {
            handleRemoveTag()
          }}
        >
          <svg
            width='18'
            height='18'
            viewBox='0 0 20 20'
            fill='none'
            xmlns='http://www.w3.org/2000/svg'
            svg-inline=''
            role='presentation'
            focusable='false'
          >
            <path
              d='M17 7L7 17M7 7l10 10'
              stroke='currentColor'
              strokeWidth='1.5'
              strokeLinecap='round'
              strokeLinejoin='round'
            ></path>
          </svg>
        </div>
      </div>
    </div>
  )
}

const TagInput = () => {
  const {
    setValue,
    getValues,
    clearErrors,
    formState: { errors },
  } = useFormContext()
  const [searchTerm, setSearchTerm] = useState<string>('')

  const isDisable = useIsDisable()

  const [open, setOpen] = useState<boolean>(false)

  const { data } = useTags({ page: 1, size: 5, searchTerm: searchTerm })

  const [selectedTag, setSelectedTag] = useState<TagInput[]>(getValues('tags'))

  const handleAddTag = (tag: TagInput) => {
    const isDup = selectedTag.find((x) => x.id === tag.id)
    setSearchTerm('')
    setOpen(false)
    if (isDup) {
      return
    }
    setSelectedTag([...selectedTag, tag])
  }

  const handleDeleteTag = (tag: TagInput) => {
    const arr = [...selectedTag]
    setSearchTerm('')
    setOpen(false)
    const isDup = selectedTag.find((x) => x.id === tag.id)
    if (!isDup) {
      return
    }
    const index = arr.indexOf(isDup)
    arr.splice(index, 1)

    setSelectedTag(arr)
  }
  useEffect(() => {
    clearErrors(['tags'])
    setValue('tags', selectedTag)
  }, [selectedTag])

  return (
    <>
      <div className='relative w-full'>
        <div className='label'>
          <span className='label-text'>Tag</span>
        </div>

        {open && (
          <ul className='menu absolute top-full mt-[1px] w-[95%] rounded-box bg-base-200'>
            {data?.items?.length ? (
              data?.items?.map((x) => {
                return (
                  <li key={x.id}>
                    <p
                      onClick={() => {
                        handleAddTag({ id: x.id, tagName: x.tagName })
                      }}
                    >
                      {x.tagName}
                    </p>
                  </li>
                )
              })
            ) : (
              <li key={'empty'}>
                <p>Not Found</p>
              </li>
            )}
          </ul>
        )}
        <input
          type='text'
          value={searchTerm}
          disabled={isDisable}
          name=''
          id=''
          className='input input-bordered w-[95%] text-wrap rounded-[16px] text-[16px] outline-none'
          placeholder='enter here ...'
          onChange={(e) => {
            setSearchTerm(e.target.value)
          }}
          onFocus={() => {
            setOpen(true)
          }}
          onBlur={() =>
            setTimeout(() => {
              setOpen(false)
            }, 200)
          }
        />
        {errors?.tags && (
          <div className='label'>
            <span className='label-text-alt text-[12px] text-red-500'>{errors?.tags?.message?.toString()}</span>
          </div>
        )}
      </div>
      <div className='flex w-full flex-wrap gap-3'>
        {selectedTag.map((x) => {
          return <Tag tag={{ id: x.id, tagName: x.tagName }} key={x.id} handleDeleteTag={handleDeleteTag} />
        })}
      </div>
    </>
  )
}

export default TagInput
