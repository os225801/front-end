/* eslint-disable @typescript-eslint/no-explicit-any */
'use client'
import Avatar from '@/components/shared/avatar'
import Modal from '@/components/shared/modal'
import { useToast } from '@/components/shared/toast'
import { useCurrentUser } from '@/hooks/use-profile-query'
import { UpdateProfileSchema } from '@/schema/update-profile.schema'
import { userService } from '@/services/user.service'
import { useIsOpenEditProfileModal, useProfileStoreActions } from '@/store/profile.store'
import { zodResolver } from '@hookform/resolvers/zod'
import Image from 'next/image'
import { useEffect, useState } from 'react'
import { SubmitHandler, useForm } from 'react-hook-form'
import { z } from 'zod'
import InputField from './input-field'
import SelectBank from './select-bank'

type FormFields = z.infer<typeof UpdateProfileSchema>
const EditProfileModal = () => {
  const isOpen = useIsOpenEditProfileModal()
  const [bannerFile, setBannerFile] = useState<File | null>(null)
  const [avatarFile, setAvatarFile] = useState<File | null>(null)
  const { setIsOpenEditProfileModal } = useProfileStoreActions()
  const { data } = useCurrentUser()
  const { showToast } = useToast()
  const [check, setCheck] = useState(false)
  const [selectedBank, setSelectedBank] = useState('')
  const [loading, setLoading] = useState(false)
  const {
    register,
    handleSubmit,
    getValues,
    setValue,
    formState: { errors },
  } = useForm<FormFields>({
    resolver: zodResolver(UpdateProfileSchema),
  })

  const close = () => {
    setIsOpenEditProfileModal(false)
  }

  const fetchAccountName = async (bin: string, accountNumber: string) => {
    const clientId = '071687ac-bf06-47bd-8515-7d07f89cacc1'
    const apiKey = 'c1a6ae8e-1268-49df-895b-318a5201d79b'

    const requestOptions = {
      method: 'POST',
      headers: {
        'x-client-id': clientId,
        'x-api-key': apiKey,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ bin, accountNumber }),
    }

    try {
      const response = await fetch('https://api.vietqr.io/v2/lookup', requestOptions)
      const data = await response.json()
      console.log(data?.data?.accountName)
      setCheck(false)
      return data?.data?.accountName
    } catch (error) {
      console.error('Error fetching account name:', error)
      return null
    }
  }

  useEffect(() => {
    if ((getValues('bankName'), getValues('accountNumber'))) {
      if (check === true) {
        if (selectedBank) {
          fetchAccountName(String(JSON?.parse(selectedBank)?.bin), String(getValues('accountNumber'))).then(
            (accountName) => {
              setValue('accountName', accountName)
            }
          )
        } else {
          showToast('Please select a bank', 'error')
        }
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [check])

  const onSubmit: SubmitHandler<FormFields> = async (data) => {
    setLoading(true)
    const payload = {
      name: data.name,
      dateOfBirth: '01/01/2003',
      bankName: data.bankName,
      accountNumber: data.accountNumber,
      accountName: data.accountName,
    }

    // Include file uploads if present
    const formData = new FormData()
    if (bannerFile) formData.append('banner', bannerFile)
    if (avatarFile) formData.append('avatar', avatarFile)

    try {
      await userService.updateProfileInfo(payload)
      if (bannerFile || avatarFile) {
        await userService.updateProfilePicture(formData)
      }
      showToast('Profile updated successfully', 'success')
      window.location.reload()
      setAvatarFile(null)
      setBannerFile(null)
      setLoading(false)
      setIsOpenEditProfileModal(false)
    } catch (error) {
      showToast('Failed to update profile', 'error')
      setLoading(false)
      setAvatarFile(null)
      setBannerFile(null)
    }
  }

  useEffect(() => {
    if (data) {
      setValue('name', data.name)
      setValue('bankName', data.bankName)
      setValue('accountNumber', data.accountNumber)
      setValue('accountName', data.accountName)
    }
  }, [data, setValue])

  const handleBannerChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setBannerFile(event.target.files?.[0] || null)
  }
  const handleAvatarChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setAvatarFile(event.target.files?.[0] || null)
  }

  return (
    <Modal title={'Manage account'} isOpen={isOpen} closeModal={close}>
      <form onSubmit={handleSubmit(onSubmit)} className='relative mb-[25px] h-[500px] overflow-auto'>
        <div className='group relative w-full max-w-[578px] rounded-[8px] before:block before:pt-[30%] md:min-w-[326px]'>
          <Image
            className='h-full w-full rounded-[8px] object-cover transition-transform duration-300 ease-in-out group-hover:opacity-80'
            fill
            loader={({ src }) => src}
            style={{ objectFit: 'cover' }}
            src={bannerFile ? URL.createObjectURL(bannerFile) : data?.bannerLink}
            alt='edit-banner'
          />
          <input type='file' onChange={handleBannerChange} className='absolute inset-0 cursor-pointer opacity-0' />
        </div>
        <div className='my-6 flex items-center gap-5'>
          <Avatar
            src={avatarFile ? URL.createObjectURL(avatarFile) : data?.avatarLink}
            width={70}
            height={70}
            className='w-[70px]'
          />
          <button
            type='button'
            className='relative cursor-pointer rounded-[10px] border bg-[#EFEFEF] px-3 py-1 transition-all hover:bg-slate-50'
          >
            <input type='file' onChange={handleAvatarChange} className='absolute inset-0 cursor-pointer opacity-0' />
            <span className='text-xs font-medium'>CHANGE</span>
          </button>
        </div>
        <InputField
          lable='Name'
          name='name'
          error={errors.name?.message}
          placeholder='Enter name'
          type='text'
          register={register}
        />
        <InputField
          lable='Account Number'
          name='accountNumber'
          error={errors.accountNumber?.message}
          placeholder='Enter Account Number'
          type='text'
          register={register}
        />
        <SelectBank
          error={errors.bankName?.message}
          setCheck={setCheck}
          setValue={setValue}
          selectedBank={selectedBank}
          setSelectedBank={setSelectedBank}
        />
        <InputField
          lable='Account Name'
          name='accountName'
          isDisable
          error={errors.accountName?.message}
          placeholder='Enter Account Name'
          type='text'
          register={register}
        />
        <button
          disabled={loading}
          className='ring-offset-background focus-visible:ring-ring inline-flex h-10 items-center justify-center rounded-[10px] bg-black px-4 py-2 text-[14px] font-medium leading-none text-white transition-colors hover:opacity-70 focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-offset-2 disabled:pointer-events-none disabled:opacity-50'
          type='submit'
        >
          {loading ? 'LOADING...' : 'DONE'}
        </button>
      </form>
    </Modal>
  )
}

export default EditProfileModal
