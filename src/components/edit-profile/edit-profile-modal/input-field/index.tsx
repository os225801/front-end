const InputField: React.FC<FormFieldProps> = ({
  prefix,
  subfix,
  type,
  placeholder,
  name,
  register,
  error,
  valueAsNumber,
  lable,
  isDisable,
}) => {
  return (
    <div className='w-full'>
      <span className='label-text'>{lable}</span>
      <label
        style={{
          background: isDisable ? '#f5f5f5' : 'white',
        }}
        className='bg-background ring-offset-background placeholder:text-muted-foreground focus-visible:border-ring focus-visible:ring-ring flex h-10 w-full rounded-md border border-neutral-300 px-3 py-2 text-base file:border-0 file:bg-transparent file:text-base file:font-medium focus-visible:border-2 focus-visible:outline-none focus-visible:ring-0 focus-visible:ring-offset-0 disabled:cursor-not-allowed disabled:opacity-50'
      >
        {prefix && prefix}
        <input
          {...register(name, { valueAsNumber })}
          min={0}
          type={type}
          id=''
          className='grow text-base focus:outline-none'
          placeholder={placeholder}
          disabled={isDisable}
        />
        {subfix && subfix}
      </label>
      <div className='label'>
        <span className='label-text-alt text-[12px] text-red-500'>{error}</span>
      </div>
    </div>
  )
}

export default InputField
