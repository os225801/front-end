/* eslint-disable no-unused-vars */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { useEffect, useState } from 'react'

export default function SelectBank({
  selectedBank,
  setSelectedBank,
  setValue,
  setCheck,
  error,
}: {
  selectedBank: string
  setSelectedBank: (value: any) => void
  setValue: any
  setCheck: any
  error: string | undefined
}) {
  const [banks, setBanks] = useState<any[]>([])

  useEffect(() => {
    const fetchBanks = async () => {
      try {
        const response = await fetch('https://api.vietqr.io/v2/banks')
        const data = await response.json()
        setBanks(data)
      } catch (error) {
        console.error('Error fetching banks:', error)
      }
    }

    fetchBanks()
  }, [])

  return (
    <div className='flex items-center gap-5'>
      <div className='flex w-full flex-col items-start gap-1'>
        <label htmlFor='bank' className='label-text'>
          Select Bank
        </label>
        <select
          id='bank'
          value={selectedBank}
          className='bg-background ring-offset-background placeholder:text-muted-foreground focus-visible:border-ring focus-visible:ring-ring flex h-10 w-full rounded-md border border-neutral-300 px-3 py-2 text-base file:border-0 file:bg-transparent file:text-base file:font-medium focus-visible:border-2 focus-visible:outline-none focus-visible:ring-0 focus-visible:ring-offset-0 disabled:cursor-not-allowed disabled:opacity-50'
          onChange={(e) => {
            setSelectedBank(e.target.value)
            setValue('bankName', JSON.parse(e.target.value)?.code)
          }}
        >
          <option value=''>Select a bank</option>
          {(banks as any)?.data?.map((bank: any) => (
            <option
              key={bank.id}
              value={JSON.stringify({
                code: bank.code,
                bin: bank.bin,
              })}
            >
              {bank.shortName}
            </option>
          ))}
        </select>
        <div className='label'>
          <span className='label-text-alt text-[12px] text-red-500'>{error}</span>
        </div>
      </div>
      <button
        type='button'
        onClick={() => {
          setCheck(true)
        }}
        className='relative cursor-pointer rounded-[10px] border bg-[#EFEFEF] px-3 py-1 transition-all hover:bg-slate-50'
      >
        <span className='text-xs font-medium'>CHECK</span>
      </button>
    </div>
  )
}
