import { postService } from '@/services/post.service'
import { HydrationBoundary, QueryClient, dehydrate } from '@tanstack/react-query'
import ListPosts from '../shared/list-posts'

const HomeScreen = async () => {
  const queryClient = new QueryClient()

  await queryClient.prefetchQuery({
    queryKey: ['posts'],
    queryFn: async () => {
      const posts = await postService.getAllPosts({
        page: 1,
        size: 20,
        keyword: '',
        sortBy: 'createdAt',
        sortOrder: 'DESC',
        isBuyable: false,
      })
      return posts
    },
  })

  await queryClient.prefetchQuery({
    queryKey: ['tags'],
    queryFn: async () => {
      const tags = await postService.getTags({
        page: 1,
        size: 10,
        searchTerm: '',
      })
      return tags
    },
  })

  return (
    <HydrationBoundary state={dehydrate(queryClient)}>
      <ListPosts />
    </HydrationBoundary>
  )
}

export default HomeScreen
