'use client'

import { useInfiniteOrderHistory } from '@/hooks/use-order-query'
import { cn } from '@/lib/utils/cn'
import { useQueryClient } from '@tanstack/react-query'
import { Fragment, useEffect, useState } from 'react'
import { IoMdInformationCircleOutline } from 'react-icons/io'
import { useDebouncedCallback } from 'use-debounce'
import { Button } from '../shared/button'
import Empty from '../shared/list-posts/empty'
import { OrderItem } from './order-item'

export const OrderList = () => {
  const currentUser: CurrentUser | null = JSON.parse(localStorage.getItem('user') || 'null')
  const [showInfo, setShowInfo] = useState(false)
  const [orderStatus, setStatus] = useState(1)
  const queryClient = useQueryClient()
  const {
    data: orders,
    isError,
    isLoading,
    fetchNextPage,
    hasNextPage,
    isFetchingNextPage,
    status,
    isFetching,
  } = useInfiniteOrderHistory(currentUser?.id!, orderStatus)

  const handleScroll = useDebouncedCallback(() => {
    const { scrollTop, clientHeight, scrollHeight } = document.documentElement
    if (isFetchingNextPage || !hasNextPage || isFetching) {
      return
    } else if (scrollTop + clientHeight >= scrollHeight - 250) {
      fetchNextPage()
    }
  }, 1000)

  useEffect(() => {
    window.addEventListener('scroll', handleScroll)
    return () => {
      window.removeEventListener('scroll', handleScroll)
    }
  }, [isFetchingNextPage])

  return (
    <div className='mb-4 flex h-fit min-h-screen w-full flex-col gap-3'>
      <p className='mb-6 self-center text-4xl font-bold'>Your Orders</p>
      <div className='flex h-fit w-full items-center justify-center gap-4'>
        <div className='h-fit w-fit'>
          <Button
            onClick={() => {
              setStatus(1)
              queryClient.invalidateQueries({ queryKey: ['orders'] })
            }}
            variant='white-no-border'
            className={cn({ 'h-fit w-fit rounded-full text-lg font-bold': true }, { 'bg-gray-300': orderStatus === 1 })}
          >
            Pending
          </Button>
        </div>
        <div className='h-fit w-fit'>
          <Button
            onClick={() => {
              setStatus(2)
              queryClient.invalidateQueries({ queryKey: ['orders'] })
            }}
            variant='white-no-border'
            className={cn({ 'h-fit w-fit rounded-full text-lg font-bold': true }, { 'bg-gray-300': orderStatus === 2 })}
          >
            Success
          </Button>
        </div>
        <div className='h-fit w-fit'>
          <Button
            onClick={() => {
              setStatus(3)
              queryClient.invalidateQueries({ queryKey: ['orders'] })
            }}
            variant='white-no-border'
            className={cn({ 'h-fit w-fit rounded-full text-lg font-bold': true }, { 'bg-gray-300': orderStatus === 3 })}
          >
            Failure
          </Button>
        </div>
      </div>
      <hr className='my-5 h-[2px] w-[60%] self-center border-none bg-gray-300' />
      {orders?.pages[0]?.items ? (
        <div className=' flex w-[75%] flex-col gap-8 self-center'>
          {orders?.pages?.map((group, i) => (
            <Fragment key={i}>
              {group?.items ? (
                <>
                  {group?.items?.map((order: Order) => (
                    <Fragment key={order.id}>
                      <OrderItem order={order} />
                    </Fragment>
                  ))}
                </>
              ) : null}
            </Fragment>
          ))}
        </div>
      ) : (
        <Empty />
      )}
      {isFetchingNextPage && hasNextPage && (
        <p className='self-center text-lg font-semibold text-base-content'>Loading...</p>
      )}
      <div
        className={cn(
          {
            'fixed bottom-24 left-[59.4%] mt-[-102px] h-fit w-fit rounded-3xl border-[1px] bg-white px-3 py-2 text-black shadow-md transition-all duration-200 ease-in-out':
              true,
          },
          {
            '-z-50 opacity-0': !showInfo,
          },
          {
            'opacity-100': showInfo,
          }
        )}
      >
        <p className='self-center text-center text-lg'>
          Your orders will be processsed within 24 hours from the order time.
          <br />
          Should anything unexpected were to happen, please contact us via email:
          <br />
          support@omnistroke.yp2743.me
        </p>
      </div>
      <div className='fixed bottom-12 left-[95%] mt-[-40px] h-10 w-10'>
        <Button variant='info' onClick={() => setShowInfo(!showInfo)}>
          <IoMdInformationCircleOutline className='h-6 w-6' />
        </Button>
      </div>
    </div>
  )
}
