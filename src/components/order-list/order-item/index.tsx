'use client'

import { PurchaseArtworkModal } from '@/components/post/purchase-artwork-modal'
import { Button } from '@/components/shared/button'
import { useToast } from '@/components/shared/toast'
import { useOrder } from '@/hooks/use-order-query'
import { useProfile } from '@/hooks/use-profile-query'
import { cn } from '@/lib/utils/cn'
import { orderService } from '@/services/order.service'
import Image from 'next/image'
import { useRouter } from 'next/navigation'
import { useState } from 'react'
import { FaCircle } from 'react-icons/fa'

export const OrderItem = (props: { order: Order }) => {
  const router = useRouter()
  const { data: seller } = useProfile({ params: { id: props.order.sellerId } })
  const [isOpen, setOpen] = useState(false)
  const { data: order } = useOrder(props.order.id)
  const { showToast } = useToast()

  return (
    <div className='flex h-96 flex-col rounded-lg border-[1px] shadow-lg'>
      <div className='flex h-[20%] items-center px-5 py-2 text-xl'>
        <div className='flex items-center gap-2'>
          <div className='relative h-10 w-10 rounded-full'>
            <Image
              src={seller?.avatarLink}
              fill
              style={{ objectFit: 'cover' }}
              alt='Avatar'
              loader={({ src }) => src}
              className='rounded-full'
            />
          </div>
          <p className='h-fit'>{seller?.username}</p>
        </div>
      </div>
      <div
        className={cn(
          { 'flex h-[60%] w-full border-b-[1px] border-t-[1px] px-5 py-2': true },
          { 'cursor-pointer': true }
        )}
        onClick={() => {
          if (props.order.status === 2) {
            router.push(`/posts/${props.order.postId}`)
          } else {
            router.push(`/orders/${props.order.id}`)
          }
        }}
      >
        <div className='flex h-full w-full'>
          <div className='relative min-h-fit w-[30%]'>
            <Image
              className='w-full rounded-lg '
              loader={({ src }) => src}
              src={props.order.image}
              alt='Image'
              fill={true}
              priority
              sizes='(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 33vw'
              style={{ objectFit: 'contain' }}
            />
          </div>
          <div className='ml-5 flex h-fit w-[80%] flex-col gap-3 py-3 text-2xl'>
            <p className='mb-5 overflow-ellipsis text-5xl font-bold'>{props.order.orderName}</p>
            <div className='flex justify-between'>
              <p className='font-bold'>Order date</p>
              <p>
                {`${new Date(Date.parse(props.order.orderDate)).toLocaleString('en-US', {
                  weekday: 'short',
                  day: 'numeric',
                  month: 'short',
                  year: 'numeric',
                  timeZone: 'Asia/Bangkok',
                  hour: 'numeric',
                  minute: '2-digit',
                  hour12: true,
                })}`}
              </p>
            </div>
            <div className='flex justify-between'>
              <p className='font-bold'>Price</p>
              <p>{`${props.order.amount} VND`}</p>
            </div>
          </div>
        </div>
      </div>
      <div className='flex h-[20%] w-full items-center justify-between px-5 py-2 text-xl'>
        <div>
          {props.order.status === 1 && (
            <p className='flex items-center gap-1 text-orange-400'>
              <FaCircle className='h-[12px] w-[12px]' />
              Pending
            </p>
          )}
          {props.order.status === 2 && (
            <p className='flex items-center gap-1 text-green-600'>
              <FaCircle className='h-[12px] w-[12px]' />
              Success
            </p>
          )}
          {props.order.status === 3 && (
            <p className='flex items-center gap-1 text-red-500'>
              <FaCircle className='h-[12px] w-[12px]' />
              Failure
            </p>
          )}
        </div>
        {props.order.status === 1 && (
          <div className='h-full w-[150px]'>
            <Button
              variant='blue'
              className='h-full w-full font-bold'
              onClick={() => {
                if (order.paymentLink !== '') {
                  router.push(order.paymentLink)
                }
              }}
            >
              Pay Now
            </Button>
          </div>
        )}
        {props.order.status === 3 && (
          <div className='h-full w-[150px]'>
            <Button
              variant='blue'
              className='h-full w-full font-bold'
              onClick={() => {
                setOpen(true)
              }}
            >
              Reorder
            </Button>
          </div>
        )}
      </div>
      <PurchaseArtworkModal
        isOpen={isOpen}
        close={() => {
          setOpen(false)
        }}
        onClickConfirm={async () => {
          const res = await orderService.createOrder(props.order.artworkId)
          if (res.paymentLink) {
            router.push(res.paymentLink)
          } else {
            showToast('You have already ordered this artwork', 'success')
          }
        }}
      />
    </div>
  )
}
