'use client'

import { useRouter } from 'next/navigation'
import { Button } from '../shared/button'
import { OrderContent } from './order-content'
import { OrderPayment } from './order-payment'

export const OrderScreen = (props: { orderId: string }) => {
  const router = useRouter()

  return (
    <div className='flex h-screen w-full flex-col items-center justify-center gap-5'>
      <p className='text-5xl font-bold'>Order Detail</p>
      <div className='flex h-[80%] w-[80%] rounded-lg border border-gray-400'>
        <OrderContent orderId={props.orderId} />
        <OrderPayment orderId={props.orderId} />
      </div>
      <div className='flex h-9 justify-between gap-2 px-5'>
        <div className='h-10 w-40 flex items-center justify-center'>
          <Button
            variant='white'
            className='text-lg font-bold'
            onClick={() => {
              router.push('/orders')
            }}
          >
            Back to orders
          </Button>
        </div>
        <div className='h-10 w-52 flex items-center justify-center'>
          <Button
            variant='white'
            className='text-lg font-bold'
            onClick={() => {
              router.push('/')
            }}
          >
            Back to homepage
          </Button>
        </div>
      </div>
    </div>
  )
}
