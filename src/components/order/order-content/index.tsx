'use client'

import { useOrder } from '@/hooks/use-order-query'
import Image from 'next/image'

export const OrderContent = (props: { orderId: string }) => {
  const { data: order } = useOrder(props.orderId)
  return (
    <div className='flex h-full w-[75%] flex-col border-r-2 border-gray-400'>
      <div className='h-full w-full border-gray-400 px-5 py-8'>
        <div className='relative h-full w-full'>
          <Image
            className='h-full w-full rounded-lg '
            loader={({ src }) => src}
            src={order?.image}
            alt='Image'
            fill
            priority
            sizes='(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 33vw'
            style={{ objectFit: 'contain' }}
          />
        </div>
      </div>
    </div>
  )
}
