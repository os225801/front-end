'use client'

import Avatar from '@/components/shared/avatar'
import { Button } from '@/components/shared/button'
import { useOrder } from '@/hooks/use-order-query'
import { userService } from '@/services/user.service'
import { useQuery } from '@tanstack/react-query'
import { useRouter } from 'next/navigation'
import { FaCircle } from 'react-icons/fa'

export const OrderPayment = (props: { orderId: string }) => {
  const router = useRouter()
  const { data: order } = useOrder(props.orderId)
  const { data: seller } = useQuery({
    queryFn: async () => {
      const user = await userService.getProfile({ userId: String(order?.sellerId) })
      return user as Profile
    },
    queryKey: ['user', order?.sellerId],
    enabled: !!order,
  })

  return (
    <>
      <div className='flex h-full w-[25%] flex-col items-center justify-center gap-6 py-24'>
        <div className='flex h-fit max-h-[50%] w-full items-start justify-start px-5 py-5 text-3xl font-bold'>
          <p className='overflow-ellipsis'>{order?.orderName}</p>
        </div>
        <div className='flex h-[50%] w-full flex-col gap-y-2 px-5 pt-5'>
          <div className='flex flex-col gap-3'>
            <div className='flex justify-between'>
              <p className='font-bold'>Date created</p>
              <div>
                <p>
                  {`${new Date(Date.parse(order?.orderDate)).toLocaleString('en-US', {
                    weekday: 'short',
                    day: 'numeric',
                    month: 'short',
                    year: 'numeric',
                    timeZone: 'Asia/Bangkok',
                    hour: 'numeric',
                    minute: '2-digit',
                    hour12: true,
                  })}`}
                </p>
              </div>
            </div>
            <div className='flex justify-between'>
              <p className='font-bold'>Seller</p>
              <div className='flex gap-2'>
                <Avatar src={seller?.avatarLink!} height={25} width={25} />
                <p>{seller?.username}</p>
              </div>
            </div>
            <div className='flex justify-between'>
              <p className='font-bold'>Amount</p>
              <div>
                <p>{`${order?.amount} VND`}</p>
              </div>
            </div>
            <div className='flex justify-between'>
              <p className='text-xl font-bold'>Status:</p>
              {order?.status === 1 && (
                <p className='flex items-center gap-1 text-xl text-orange-400'>
                  <FaCircle className='h-[12px] w-[12px]' />
                  Pending
                </p>
              )}
              {order?.status === 2 && (
                <p className='flex items-center gap-1 text-xl text-green-600'>
                  <FaCircle className='h-[12px] w-[12px]' />
                  Success
                </p>
              )}
              {order?.status === 3 && (
                <p className='flex items-center gap-1 text-xl text-red-500'>
                  <FaCircle className='h-[12px] w-[12px]' />
                  Failure
                </p>
              )}
            </div>
            {order?.status === 1 && (
              <div className='h-14 w-full mt-5'>
                <Button
                  variant='blue'
                  onClick={() => {
                    if (order.paymentLink) {
                      router.push(order.paymentLink)
                    }
                  }}
                  className='text-2xl font-bold flex h-full'
                >
                  Pay Now
                </Button>
              </div>
            )}
          </div>
        </div>
      </div>
    </>
  )
}
