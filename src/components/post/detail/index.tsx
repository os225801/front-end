'use client'
import { usePostInfo } from '@/hooks/use-post-info-query'
import { postService } from '@/services/post.service'
import { useEffect, useState } from 'react'
import PostContent from './post-content'
import PostImage from './post-image'

const DetailPost = ({ params }: { params: { id: string } }) => {
  const { data: postInfo, isLoading } = usePostInfo({ params })
  const [currentUser, setCurrentUser] = useState<CurrentUser | null>(null)

  const updateViewCount = async () => {
    await postService.updateViewCount({ postId: params.id })
  }

  useEffect(() => {
    if (typeof window !== 'undefined' && window.localStorage) {
      const userString = window.localStorage.getItem('user')
      if (userString) {
        setCurrentUser(JSON.parse(userString) as CurrentUser)
      }
    }
  }, [])

  useEffect(() => {
    if (postInfo) {
      updateViewCount()
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <div className='lg:relative lg:mt-[17px] lg:flex lg:flex-row lg:justify-between lg:space-x-6'>
      {isLoading ? (
        <div className='skeleton mt-4 flex aspect-[4/3] max-h-[600px] w-full flex-col lg:mt-0'></div>
      ) : (
        <PostImage
          imageUrl={postInfo?.artwork?.image}
          user={postInfo?.user}
          createdAt={postInfo?.createdAt}
          isLiked={postInfo?.isLiked}
          totalLikes={postInfo?.totalLikes}
          currentUser={currentUser}
          isDownloadable={postInfo?.isDownloaded}
          price={postInfo?.price}
          artwork={postInfo?.artwork}
          postId={params?.id}
        />
      )}
      {isLoading ? null : (
        <PostContent
          title={postInfo?.title}
          description={postInfo?.description}
          views={postInfo?.views}
          artwork={postInfo?.artwork}
          currentUser={currentUser}
          user={postInfo?.user}
          isDownloadable={postInfo?.isDownloaded}
          totalLikes={postInfo?.totalLikes}
          tags={postInfo?.tags}
          postId={params?.id}
          isLiked={postInfo?.isLiked}
        />
      )}
    </div>
  )
}

export default DetailPost
