import { CommentList } from '@/components/shared/comment-list'
import { useToast } from '@/components/shared/toast'
import { useCreateComment } from '@/hooks/use-comment-query'
import { useDeletePost } from '@/hooks/use-post-query'
import { useFollowAndUnfollowUser, useProfile } from '@/hooks/use-profile-query'
import { artworkService } from '@/services/artwork.service'
import { Menu, MenuButton, MenuItem, MenuItems } from '@headlessui/react'
import Image from 'next/image'
import Link from 'next/link'
import { useRouter } from 'next/navigation'
import { useState } from 'react'
import { useForm } from 'react-hook-form'
import { DeletePostModal } from '../../delete-post-modal'

const PostContent = ({
  title,
  description,
  views,
  totalLikes,
  tags,
  user,
  postId,
  isLiked,
  currentUser,
  isDownloadable,
  artwork,
}: {
  title: string
  description: string
  views: number
  totalLikes: number
  tags: PostTag[]
  user: User
  postId: string
  currentUser: CurrentUser | null
  isLiked: boolean
  isDownloadable: boolean
  artwork: Artwork
}) => {
  const { register, handleSubmit, reset } = useForm()
  const isMe = currentUser?.id === user?.userId
  const { showToast } = useToast()
  const { data: userToFollow } = useProfile({ params: { id: user?.userId } })
  const { mutateAsync: createComment, isPending } = useCreateComment()
  const [isModalOpen, setModalOpen] = useState(false)
  const { followAndUnfollow } = useFollowAndUnfollowUser()
  const { mutateAsync } = useDeletePost()
  const router = useRouter()
  const ListTags = ({ tags }: { tags: PostTag[] }) => {
    const [expanded, setExpanded] = useState(false)
    const tagsPerRow = 3 // Adjust based on your layout

    const handleToggle = () => {
      setExpanded(!expanded)
    }

    const displayedTags = expanded ? tags : tags?.slice(0, tagsPerRow)
    console.log('displayedTags', displayedTags)

    return (
      <div className='w-full'>
        <div className='flex flex-row flex-wrap items-center justify-start gap-2'>
          {displayedTags?.map((tag) => (
            <Link key={tag?.id} href={`/search?q=${tag?.id}`}>
              <div className='flex h-6 items-center rounded-[10px] border border-[#E6E6E6] bg-white px-2 hover:cursor-pointer'>
                <span className='text-[14px] font-normal leading-6 tracking-[-0.14px] text-[#242424]'>
                  {tag?.tagName}
                </span>
              </div>
            </Link>
          ))}
          {tags?.length > tagsPerRow && !expanded && (
            <div className='flex h-6 items-center rounded-[10px] border border-[#E6E6E6] bg-white px-2 hover:cursor-pointer'>
              <span
                onClick={handleToggle}
                className='text-[14px] font-normal leading-6 tracking-[-0.14px] text-[#242424]'
              >
                {'More...'}
              </span>
            </div>
          )}
        </div>
      </div>
    )
  }

  return (
    <div className='mt-8 flex w-full flex-col space-y-6 md:flex-row md:space-x-11 md:space-y-0 lg:sticky lg:top-2 lg:mt-0 lg:max-w-[324px] lg:flex-col lg:justify-between lg:space-x-0'>
      <div className='flex w-full flex-col space-y-6 md:space-y-3 lg:space-y-4'>
        <h1 className='line-clamp-1 truncate text-xl font-bold leading-6 tracking-[-0.2px] text-[#242424]'>{title}</h1>
        <span className='line-clamp-2 max-h-[150px] overflow-y-auto truncate whitespace-pre-line text-[15px] font-normal leading-6 tracking-[0.15px] text-[#424242] md:max-h-none lg:max-h-[200px] xl:max-h-[450px]'>
          {description}
        </span>
        <div className='block w-full flex-col items-start justify-start gap-4 space-y-[10px] rounded-lg border border-[#EFEFEF] p-2 md:w-[258px] lg:w-full'>
          <div className='flex w-full flex-row items-center justify-between'>
            <div className='flex flex-row space-x-4'>
              <div className='flex flex-row items-center space-x-1'>
                <div className='h-[18px] w-[18px] text-[#767676]'>
                  <svg
                    xmlns='http://www.w3.org/2000/svg'
                    width='18'
                    height='18'
                    viewBox='0 0 18 18'
                    fill={isLiked ? '#ff4343' : 'none'}
                    role='img'
                  >
                    <path
                      d='M10.7408 2C13.0889 2 14.6667 4.235 14.6667 6.32C14.6667 10.5425 8.11856 14 8.00004 14C7.88152 14 1.33337 10.5425 1.33337 6.32C1.33337 4.235 2.91115 2 5.2593 2C6.60745 2 7.48893 2.6825 8.00004 3.2825C8.51115 2.6825 9.39263 2 10.7408 2Z'
                      stroke={isLiked ? 'none' : 'currentColor'}
                      strokeWidth='1'
                      strokeLinecap='round'
                      strokeLinejoin='round'
                    ></path>
                  </svg>
                </div>
                <span className='text-[14px] font-normal leading-6 tracking-[-0.14px] text-[#242424]'>
                  {totalLikes}
                </span>
              </div>
              <div className='flex flex-row items-center space-x-1'>
                <svg
                  stroke='currentColor'
                  fill='currentColor'
                  strokeWidth='0'
                  viewBox='0 0 24 24'
                  className='h-5 w-5 text-[#767676]'
                  height='1em'
                  width='1em'
                  xmlns='http://www.w3.org/2000/svg'
                >
                  <path d='M12.0003 3C17.3924 3 21.8784 6.87976 22.8189 12C21.8784 17.1202 17.3924 21 12.0003 21C6.60812 21 2.12215 17.1202 1.18164 12C2.12215 6.87976 6.60812 3 12.0003 3ZM12.0003 19C16.2359 19 19.8603 16.052 20.7777 12C19.8603 7.94803 16.2359 5 12.0003 5C7.7646 5 4.14022 7.94803 3.22278 12C4.14022 16.052 7.7646 19 12.0003 19ZM12.0003 16.5C9.51498 16.5 7.50026 14.4853 7.50026 12C7.50026 9.51472 9.51498 7.5 12.0003 7.5C14.4855 7.5 16.5003 9.51472 16.5003 12C16.5003 14.4853 14.4855 16.5 12.0003 16.5ZM12.0003 14.5C13.381 14.5 14.5003 13.3807 14.5003 12C14.5003 10.6193 13.381 9.5 12.0003 9.5C10.6196 9.5 9.50026 10.6193 9.50026 12C9.50026 13.3807 10.6196 14.5 12.0003 14.5Z'></path>
                </svg>
                <span className='text-[14px] font-normal leading-6 tracking-[-0.14px] text-[#242424]'>{views}</span>
              </div>
            </div>
            <Menu>
              <MenuButton
                className=' text-md inline-flex h-8 w-8 min-w-max items-center justify-center rounded-full bg-[#FFF] p-0 font-semibold transition-colors hover:bg-[#ECECEC] focus:outline-none focus:ring-0 focus:ring-transparent focus:ring-offset-0 focus-visible:outline-none focus-visible:ring-0 focus-visible:ring-transparent focus-visible:ring-offset-2 disabled:pointer-events-none disabled:opacity-50'
                type='button'
                id='radix-:ra:'
                aria-haspopup='menu'
                aria-expanded='false'
                data-state='closed'
              >
                <div className='h-6 w-6 text-[#242424]'>
                  <svg viewBox='0 0 24 24' fill='currentColor' xmlns='http://www.w3.org/2000/svg'>
                    <path d='M5 10C3.9 10 3 10.9 3 12C3 13.1 3.9 14 5 14C6.1 14 7 13.1 7 12C7 10.9 6.1 10 5 10ZM19 10C17.9 10 17 10.9 17 12C17 13.1 17.9 14 19 14C20.1 14 21 13.1 21 12C21 10.9 20.1 10 19 10ZM12 10C10.9 10 10 10.9 10 12C10 13.1 10.9 14 12 14C13.1 14 14 13.1 14 12C14 10.9 13.1 10 12 10Z'></path>
                  </svg>
                </div>
              </MenuButton>
              <MenuItems
                transition
                anchor='bottom end'
                className='mt-1 w-52 origin-top-right rounded-xl border bg-white p-1 text-sm/6 text-black transition duration-100 ease-out [--anchor-gap:var(--spacing-1)] focus:outline-none data-[closed]:scale-95 data-[closed]:opacity-0'
              >
                {((isDownloadable && currentUser && artwork?.artworkId) ||
                  (!artwork?.isBuyable && currentUser?.id === user?.userId)) && (
                  <MenuItem>
                    <button
                      onClick={() => {
                        const filename = title.replaceAll(' ', '_').toLowerCase()
                        artworkService.downloadArtwork(artwork?.artworkId, filename)
                      }}
                      className='group flex w-full items-center gap-2 rounded-lg px-3 py-1.5 hover:bg-[#ECECEC]'
                    >
                      Download
                    </button>
                  </MenuItem>
                )}
                <MenuItem>
                  <button
                    onClick={() => {
                      navigator.clipboard.writeText(window.location.href)
                      showToast('Link copied to clipboard', 'success')
                    }}
                    className='group flex w-full items-center gap-2 rounded-lg px-3 py-1.5 hover:bg-[#ECECEC]'
                  >
                    Share
                  </button>
                </MenuItem>
                {isMe && (
                  <>
                    <MenuItem>
                      <Link
                        href={`/update-post/${postId}`}
                        className='group flex w-full items-center gap-2 rounded-lg px-3 py-1.5 hover:bg-[#ECECEC]'
                      >
                        Edit
                      </Link>
                    </MenuItem>
                    <MenuItem>
                      <button
                        onClick={() => {
                          setModalOpen(true)
                        }}
                        className='group flex w-full items-center gap-2 rounded-lg px-3 py-1.5 hover:bg-[#ECECEC]'
                      >
                        Delete
                      </button>
                    </MenuItem>
                  </>
                )}
              </MenuItems>
            </Menu>
          </div>
          <div className='flex w-full flex-row items-start space-x-1'>
            {/* List Tags */}
            <ListTags tags={tags} />
          </div>
        </div>
      </div>
      <div className='relative flex flex-1'>
        <CommentList postId={postId} />
      </div>
      <div className='flex w-full flex-col md:w-[258px] md:space-y-[15px] lg:w-full lg:space-y-4'>
        <div className='flex w-full items-center justify-between space-y-2 rounded-[10px] bg-[#F4F4F4] p-3 md:mx-1.5 md:mt-[28px] lg:mx-0'>
          <div className='flex flex-row items-center space-x-2'>
            <span className='relative h-[50px] w-[50px] flex-none select-none items-center justify-center overflow-hidden rounded-full align-middle selection:inline-flex'>
              <Image
                className='h-full w-full object-cover'
                alt='Avatar'
                loader={({ src }) => src}
                fill
                src={user?.avatar}
              />
            </span>
            <div className='flex flex-col space-y-0.5'>
              <span className='w-full truncate text-base font-semibold leading-6 tracking-[-0.16px] text-[#242424] md:w-[120px]'>
                {user?.username}
              </span>
              <div className='flex flex-row items-center justify-start gap-1'>
                <div className='truncate text-[15px] font-normal leading-[19px] text-green-600 md:w-[120px]'>
                  {user?.name}
                </div>
              </div>
            </div>
          </div>
          {!isMe && (
            <button
              onClick={() => {
                if (currentUser) {
                  followAndUnfollow(user?.userId)
                } else {
                  showToast('Please login to follow', 'error')
                }
              }}
              className='inline-flex h-8 min-w-[70px] items-center justify-center rounded-[10px] border border-[#D8D8D8] bg-[#FFF] px-3 py-2 text-base font-semibold transition-colors hover:bg-[#ECECEC] focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-offset-2 disabled:pointer-events-none disabled:opacity-50 md:w-[88px]'
              type='button'
            >
              <span className='text-sm font-medium uppercase'>{userToFollow?.isFollowed ? 'Following' : 'Follow'}</span>
            </button>
          )}
        </div>
        <form
          onSubmit={handleSubmit((e) => {
            if (currentUser) {
              if (e['content']?.trim()?.length > 0) {
                createComment({ postId: postId, content: e['content'] })
                reset()
              } else {
                reset()
              }
            } else {
              showToast('Please login to comment', 'error')
              reset()
            }
          })}
          className='flex w-full items-center rounded-[10px] border bg-white px-4 py-3'
        >
          <textarea
            disabled={isPending}
            {...register('content', { required: true })}
            className='h-full max-h-[176px] w-full resize-none overflow-y-auto border-none bg-transparent text-[14px] font-normal leading-6 tracking-[-0.14px] text-[#2D2D2D] outline-none placeholder:tracking-[-0.14px] placeholder:text-[#848484]'
            placeholder='Send a message...'
            rows={1}
            style={{ height: '24px' }}
          ></textarea>
          <button type='submit' className='ml-2 flex-none focus:outline-none'>
            <div className='inline-flex h-8 w-8 items-center justify-center rounded-full bg-[#1b1750]'>
              <div className='h-[18px] w-[18px] text-white'>
                <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 24 24' fill='currentColor'>
                  <path d='M3.478 2.405a.75.75 0 00-.926.94l2.432 7.905H13.5a.75.75 0 010 1.5H4.984l-2.432 7.905a.75.75 0 00.926.94 60.519 60.519 0 0018.445-8.986.75.75 0 000-1.218A60.517 60.517 0 003.478 2.405z'></path>
                </svg>
              </div>
            </div>
          </button>
        </form>
      </div>
      <DeletePostModal
        isOpen={isModalOpen}
        close={() => {
          setModalOpen(false)
        }}
        onClickConfirm={async () => {
          await mutateAsync({
            postId: postId,
          })
          router.push('/')
        }}
      />
    </div>
  )
}

export default PostContent
