import { useToast } from '@/components/shared/toast'
import { useLikeAndUnlikePost } from '@/hooks/use-post-query'
import { formatDate } from '@/lib/utils/formatDate'
import { formatVND } from '@/lib/utils/formatVND'
import { orderService } from '@/services/order.service'
import { usePostIsLoading } from '@/store/post.store'
import Image from 'next/image'
import Link from 'next/link'
import { useRouter } from 'next/navigation'
import React from 'react'
import { PurchaseArtworkModal } from '../../purchase-artwork-modal'

const PostImage = ({
  imageUrl,
  user,
  createdAt,
  totalLikes,
  isLiked,
  postId,
  artwork,
  isDownloadable,
  price,
  currentUser,
}: {
  imageUrl: string
  user: User
  createdAt: string
  totalLikes: number
  isLiked: boolean
  artwork: Artwork
  isDownloadable: boolean
  postId: string
  price: number
  currentUser: CurrentUser | null
}) => {
  const { likeOrUnlike } = useLikeAndUnlikePost(postId)
  const router = useRouter()
  const isLoading = usePostIsLoading()
  const { showToast } = useToast()
  const [isPurchaseModalOpen, setPurchaseModalOpen] = React.useState(false)

  return (
    <div className='mt-4 flex w-full flex-col lg:mt-0'>
      <div className='md:via-16% md:rounded-[10px] md:border md:border-[#E6E6E6] md:bg-gradient-to-br md:from-[#F3F3F3] md:via-transparent md:via-95% md:to-[rgba(243,243,243,0.21)] md:p-6 md:pt-[22px] lg:w-full'>
        <div className='flex w-full flex-row items-center justify-between'>
          <div className='flex w-full flex-row justify-start space-x-2 md:space-x-[12px]'>
            <Link
              href={`/profile/${user?.userId}`}
              className='relative h-8 w-8 flex-none select-none items-center justify-center overflow-hidden rounded-full align-middle selection:inline-flex hover:cursor-pointer md:h-10 md:w-10'
            >
              <span className='h-full w-full'>
                <Image
                  className='h-full w-full object-cover'
                  alt='Avatar'
                  loader={({ src }) => src}
                  fill
                  src={user?.avatar}
                />
              </span>
            </Link>
            <div className='flex h-full w-full flex-col space-y-0.5 whitespace-nowrap'>
              <Link
                href={`/profile/${user?.userId}`}
                className='text-base font-semibold leading-4 tracking-[-0.14px] text-[#242424] hover:cursor-pointer md:text-[15px] md:tracking-[-0.15px]'
              >
                {user?.username}
              </Link>
              <div className='flex w-full flex-row items-center space-x-1 whitespace-nowrap text-[13px] font-semibold leading-4 tracking-[-0.14px] text-[#6F6F6F] md:text-[14px] md:font-normal'>
                <span>{formatDate(createdAt)}</span>
              </div>
            </div>
          </div>
          <div className='flex w-full flex-row justify-end space-x-2 md:space-x-2.5'>
            {artwork?.isBuyable && !isDownloadable && (
              <button
                onClick={() => setPurchaseModalOpen(true)}
                className=' inline-flex h-10 min-w-[60px] items-center justify-center rounded-[10px] border border-[#D8D8D8] bg-[#FFF] px-3 py-2 text-base font-semibold transition-colors hover:bg-[#ECECEC] focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-offset-2 disabled:pointer-events-none disabled:opacity-50'
                type='button'
              >
                <span className='text-sm font-medium uppercase'>{formatVND(price)}</span>
              </button>
            )}
            <button
              onClick={() => {
                if (currentUser) {
                  likeOrUnlike(postId)
                } else {
                  showToast('Please login first', 'error')
                }
              }}
              className=' inline-flex h-10 w-[60px] items-center justify-center rounded-[10px] border border-[#D8D8D8] bg-[#FFF] px-3 py-2 text-base font-semibold transition-colors hover:bg-[#ECECEC] focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-offset-2 disabled:pointer-events-none disabled:opacity-50'
              type='button'
            >
              <div className='flex flex-row items-center space-x-1'>
                <div
                  className='relative'
                  style={{
                    width: '16px',
                    height: '16px',
                  }}
                >
                  <div className={`h-4 w-4 ${isLiked ? 'text-[#FF0000]' : 'text-black'}`}>
                    <svg
                      xmlns='http://www.w3.org/2000/svg'
                      width='16'
                      height='16'
                      viewBox='0 0 16 16'
                      fill='currentColor'
                      role='img'
                      className={`${!isLoading ? 'like-animation' : ''}`}
                    >
                      <path
                        d='M10.7408 2C13.0889 2 14.6667 4.235 14.6667 6.32C14.6667 10.5425 8.11856 14 8.00004 14C7.88152 14 1.33337 10.5425 1.33337 6.32C1.33337 4.235 2.91115 2 5.2593 2C6.60745 2 7.48893 2.6825 8.00004 3.2825C8.51115 2.6825 9.39263 2 10.7408 2Z'
                        stroke='currentColor'
                        strokeWidth='1'
                        strokeLinecap='round'
                        strokeLinejoin='round'
                      ></path>
                    </svg>
                  </div>
                </div>
                <span className='text-base font-medium uppercase leading-4'>{totalLikes}</span>
              </div>
            </button>
          </div>
        </div>
        <div className='flex flex-col gap-5'>
          <div className='relative mt-5 aspect-[4/3] max-h-[600px] w-full rounded-[10px] bg-transparent md:mt-[21px] lg:max-h-none'>
            <Image
              loader={({ src }) => src}
              src={imageUrl}
              alt='image'
              fill
              className='absolute inset-0 flex aspect-[4/3] max-h-full items-center justify-center rounded-[10px] object-cover'
            />
          </div>
        </div>
      </div>
      <PurchaseArtworkModal
        isOpen={isPurchaseModalOpen}
        close={() => {
          setPurchaseModalOpen(false)
        }}
        onClickConfirm={async () => {
          try {
            const res = await orderService.createOrder(artwork.artworkId)
            if (res.paymentLink) {
              router.push(res.paymentLink)
            }
          } catch (e) {
            showToast('You have already ordered this artwork', 'success')
            router.push('/orders')
          }
        }}
      />
    </div>
  )
}

export default PostImage
