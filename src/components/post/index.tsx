import { commentService } from '@/services/comment.service'
import { postService } from '@/services/post.service'
import { userService } from '@/services/user.service'
import { dehydrate, HydrationBoundary, QueryClient } from '@tanstack/react-query'
import PostContent from './post-content'
import PostRecommendation from './post-recommendation'

export default async function PostScreen({ params }: { params: { id: string } }) {
  const queryClient = new QueryClient()

  await queryClient.prefetchInfiniteQuery({
    queryKey: ['comments', params.id],
    queryFn: async () => {
      const posts = await commentService.getComments({ postId: params.id, pageNumber: 1, pageSize: 10 })
      return posts
    },
    initialPageParam: 1,
    getNextPageParam: (lastPage: { hasNextPage: boolean; pageNumber: number }) => {
      return lastPage?.hasNextPage ? lastPage?.pageNumber + 1 : undefined
    },
  })

  await queryClient.prefetchQuery({
    queryKey: ['current-user'],
    queryFn: async () => {
      const currentUser = await userService.getCurrentUser()
      return currentUser
    },
  })

  await queryClient.prefetchQuery({
    queryKey: ['post', params.id],
    queryFn: async () => {
      const postInfo = await postService.getPostInfo({
        postId: params.id,
      })
      return postInfo
    },
  })

  return (
    <HydrationBoundary state={dehydrate(queryClient)}>
      <div className='ml-[5%] mt-[50px] flex h-auto w-[90%] flex-col items-center'>
        <PostContent params={params} />
        <hr className='mb-4 mt-5 h-[2px] w-[90%] border-none bg-gray-300' />
        <PostRecommendation params={params} />
      </div>
    </HydrationBoundary>
  )
}
