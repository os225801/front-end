'use client'

import { useInfiniteComments } from '@/hooks/use-comment-query'
import { usePostInfo } from '@/hooks/use-post-info-query'
import PostArtwork from './post-artwork'
import PostComment from './post-comment'
import PostDescription from './post-description'

export default function PostContent({ params }: { params: { id: string } }) {
  const { data: postInfo } = usePostInfo({ params })
  const currentUser: CurrentUser | null = JSON.parse(localStorage.getItem('user') || 'null')
  const { data: comments } = useInfiniteComments({ postId: postInfo.id })

  return (
    <div className='flex h-full w-full'>
      <div className='flex h-full w-full flex-col rounded-lg pb-3 shadow-[0_0_3px_rgba(0,0,0,0.25)]'>
        <PostArtwork imageUrl={postInfo?.artwork?.image} />
        <hr className='h-[2px] border-none bg-gray-300' />
        <div className='flex flex-col px-10'>
          <PostDescription
            title={postInfo?.title}
            description={postInfo?.description}
            createdAt={postInfo?.createdAt}
            isLiked={postInfo?.isLiked}
            tags={postInfo?.tags}
            totalLikes={postInfo?.totalLikes}
            user={postInfo?.user}
            postId={params.id}
            currentUser={currentUser}
            artwork={postInfo.artwork}
            isDownloadable={postInfo.isDownloaded}
            views={postInfo.views}
            totalComments={comments?.pages[0].totalCount as number}
          />
          <hr className='my-5 h-[2px] w-[75%] self-center border-none bg-gray-300' />
          <PostComment user={currentUser} postId={params.id} />
        </div>
      </div>
    </div>
  )
}
