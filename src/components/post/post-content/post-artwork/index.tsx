import Image from 'next/image'

export default function PostArtwork({ imageUrl }: { imageUrl: string }) {
  return (
    <div className='h-full min-h-[750px] w-full px-5 py-1 md:px-10 md:py-3'>
      <div className='relative h-full min-h-[750px] w-full'>
        <Image
          className='h-auto w-full rounded-lg '
          loader={({ src }) => src}
          src={imageUrl}
          alt='Image'
          fill
          priority
          sizes='(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 33vw'
          style={{ objectFit: 'contain' }}
        />
      </div>
    </div>
  )
}
