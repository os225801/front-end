'use client'

import { Button } from '@/components/shared/button'
import { CommentList } from '@/components/shared/comment-list'
import { CommentForm } from '@/components/shared/form/comment-form'
import Link from 'next/link'

export default function PostComment(props: { user: CurrentUser | null; postId: string }) {
  let currentUrl = ''
  if (typeof window !== 'undefined') {
    currentUrl = window.location.href
  }
  return (
    <div>
      {props.user && <CommentForm avatar={props.user.avatarLink} isReply={false} postId={props.postId} />}
      {!props.user && (
        <div className='flex h-fit w-[%] flex-col items-center py-4'>
          <p className='font-bold'>Sign in to leave a comment</p>
          <div className='h-10 w-[25%] pt-3 font-bold'>
            <Link href={`${process.env.NEXT_PUBLIC_BASE_URL}/login?current-url=${currentUrl}`} className='block h-full'>
              <Button variant='blue' className='h-full'>
                Sign In
              </Button>
            </Link>
          </div>
        </div>
      )}
      <CommentList postId={props.postId} />
    </div>
  )
}
