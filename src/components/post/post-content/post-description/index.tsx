'use client'

import Avatar from '@/components/shared/avatar'
import { Button, BuyButton, DeleteButton, DownloadButton, EditButton, LikeButton } from '@/components/shared/button'
import { useFollowAndUnfollowUser, useProfile } from '@/hooks/use-profile-query'
import Link from 'next/link'
import { FaEye } from 'react-icons/fa'
import { IoMdHeart } from 'react-icons/io'
import { MdChatBubble } from 'react-icons/md'

export default function PostDescription(props: {
  title: string
  description: string
  createdAt: string
  user: User
  tags: PostTag[]
  totalLikes: number
  isLiked: boolean
  postId: string
  currentUser: CurrentUser | null
  artwork: Artwork
  isDownloadable: boolean
  views: number
  totalComments: number
}) {
  const { followAndUnfollow } = useFollowAndUnfollowUser()
  const { data: user } = useProfile({ params: { id: props.user.userId } })

  return (
    <>
      <div className='flex flex-col items-start pt-2'>
        <div className='flex w-full justify-between'>
          <div className='flex max-w-[70%] flex-col hyphens-auto text-wrap'>
            <p className='text-[30px] font-bold'>{props.title}</p>
            <Link href={`/profile/${props?.user?.userId}`} className='mt-3 flex items-center gap-2'>
              <p>By: </p>
              <Avatar src={props?.user?.avatar} width={45} height={45} />
              <p className='ml-1 text-base font-medium'>{props?.user?.username}</p>
            </Link>
            <div className='mt-5 flex items-center gap-2 text-base'>
              <div className='flex items-center gap-[4px] font-bold'>
                <IoMdHeart className='h-[12px] w-[12px]' /> {props?.totalLikes ? props.totalLikes : 0} Likes
              </div>
              <div className='flex items-center gap-[4px] font-bold'>
                <FaEye className='h-[12px] w-[12px]' /> {props?.views ? props.views : 0} Views
              </div>
              <div className='flex items-center gap-[4px] font-bold'>
                <MdChatBubble className='h-[12px] w-[12px]' /> {props?.totalComments ? props.totalComments : 0} Comments
              </div>
            </div>
          </div>
          <div className='flex flex-col items-end'>
            <div className='flex h-fit items-start justify-center gap-3 pt-3 '>
              {props.currentUser && <LikeButton postId={props.postId} isLiked={props.isLiked} />}
              {props.currentUser &&
                props.artwork.isBuyable &&
                props.currentUser.id !== props.user.userId &&
                !props.isDownloadable && <BuyButton artworkId={props.artwork.artworkId} />}
              {((!props.isDownloadable && props.currentUser && !props.artwork.isBuyable) ||
                (props.artwork.isBuyable && props.currentUser?.id === props.user.userId)) && (
                <DownloadButton artworkId={props.artwork.artworkId} title={props.title} />
              )}
              {props.currentUser && props.currentUser.id === props.user.userId && (
                <Link href={`/update-post/${props.postId}`}>
                  <EditButton />
                </Link>
              )}
              {props.currentUser && props.currentUser.id === props.user.userId && (
                <DeleteButton postId={props.postId} userId={props.currentUser.id} />
              )}
            </div>
            <p className='mt-5'>
              {`Published: ${new Date(props.createdAt).toLocaleString('en-US', {
                weekday: 'short',
                day: 'numeric',
                month: 'short',
                year: 'numeric',
                timeZone: 'Asia/Bangkok',
                hour: 'numeric',
                minute: '2-digit',
                hour12: true,
              })}`}
            </p>
          </div>
        </div>
      </div>
      <div className='my-5 flex flex-wrap justify-start gap-x-[4px] gap-y-[5px]'>
        {props.tags &&
          props.tags.map((tag, index) => {
            return (
              <Button key={index} variant='white' className='h-[28px] w-auto text-sm'>
                {tag?.tagName}
              </Button>
            )
          })}
      </div>
      <p className=' hyphens-auto text-wrap text-sm'>{props.description}</p>
      <div className='mt-2 flex w-full items-center justify-start gap-2'>
        <div className='flex gap-2'>
          {props.currentUser && user?.isFollowed && (
            <Button
              variant='blue'
              className='h-[30px] w-[90px] text-sm font-bold'
              onClick={() => {
                followAndUnfollow(user.id)
              }}
            >
              Follow
            </Button>
          )}
          {props.currentUser && user?.isFollowed && (
            <Button
              variant='white'
              className='h-[30px] w-[90px] text-sm font-bold'
              onClick={() => {
                followAndUnfollow(user.id)
              }}
            >
              Following
            </Button>
          )}
          {/* <Button
                            variant='white'
                            className='h-[30px] w-[80px] text-sm font-bold'
                          >
                            Message
                          </Button> */}
        </div>
      </div>
    </>
  )
}
