'use client'
import { Card } from '@/components/shared/card'
import Empty from '@/components/shared/list-posts/empty'
import { useInfinitePost } from '@/hooks/use-post-query'
import { Fragment, useEffect } from 'react'
import { useDebouncedCallback } from 'use-debounce'

export default function PostRecommendation({ params }: { params: { id: string } }) {
  const { data: posts, fetchNextPage, hasNextPage, isFetchingNextPage, isFetching } = useInfinitePost()

  const handleScroll = useDebouncedCallback(() => {
    const { scrollTop, clientHeight, scrollHeight } = document.documentElement
    if (isFetchingNextPage || !hasNextPage || isFetching) {
      return
    } else if (scrollTop + clientHeight >= scrollHeight - 250) {
      fetchNextPage()
    }
  }, 1000)

  useEffect(() => {
    window.addEventListener('scroll', handleScroll)
    return () => {
      window.removeEventListener('scroll', handleScroll)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isFetchingNextPage])

  return (
    <div className='my-[40px] flex w-full flex-col'>
      <p className='text-base font-bold'>You might also like</p>
      {posts?.pages[0]?.items ? (
        <div className='shots-grid w-full'>
          {posts?.pages?.map((group, i) => (
            <Fragment key={i}>
              {group?.items ? (
                <>
                  {group?.items
                    ?.filter((item: Post) => {
                      return item.id !== params.id
                    })
                    .map((post: Post) => (
                      <Fragment key={post.id}>
                        <Card {...post} />
                      </Fragment>
                    ))}
                </>
              ) : null}
            </Fragment>
          ))}
        </div>
      ) : (
        <Empty />
      )}
      {isFetchingNextPage && hasNextPage && (
        <p className='self-center text-lg font-semibold text-base-content'>Loading...</p>
      )}
    </div>
  )
}
