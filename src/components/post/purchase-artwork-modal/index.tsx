import { Button } from '@/components/shared/button'
import Modal from '@/components/shared/modal'

export const PurchaseArtworkModal = (props: { isOpen: boolean; close: () => void; onClickConfirm: () => void }) => {
  return (
    <Modal isOpen={props.isOpen} closeModal={props.close} title='Delete Post'>
      <div className='flex flex-col'>
        <p>Are you sure you want to purchase this artwork?</p>
        <div className='mt-1 flex gap-2 self-end'>
          <Button variant='blue' onClick={props.onClickConfirm}>
            Confirm
          </Button>
          <Button variant='white' onClick={props.close}>
            Cancel
          </Button>
        </div>
      </div>
    </Modal>
  )
}
