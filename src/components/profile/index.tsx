import { userService } from '@/services/user.service'
import { HydrationBoundary, QueryClient, dehydrate } from '@tanstack/react-query'

import { postService } from '@/services/post.service'
import ProfileSection from './profile-section'

const ProfileScreen = async ({ params }: { params: { id: string } }) => {
  const queryClient = new QueryClient()

  // Prefetch user profile
  await queryClient.prefetchQuery({
    queryKey: ['user', params.id],
    queryFn: async () => {
      const user = await userService.getProfile({ userId: String(params.id) }) // Pass an object with the userId property
      return user
    },
  })

  await queryClient.prefetchQuery({
    queryKey: ['posts', params.id],
    queryFn: async () => {
      const user = await postService.getPostsByUserId({ userId: String(params.id) })
      return user
    },
  })

  return (
    <HydrationBoundary state={dehydrate(queryClient)}>
      <ProfileSection />
    </HydrationBoundary>
  )
}

export default ProfileScreen
