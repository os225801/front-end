'use client'
import CollectionModal from '@/components/shared/card/collection-modal'
import { useProfile } from '@/hooks/use-profile-query'
import { useParams } from 'next/navigation'
import TabBar from '../tabs'
import UserProfile from '../user-profile'

const ProfileSection = () => {
  const params = useParams<{ id: string }>()
  const { data, isLoading, isError } = useProfile({ params })

  if (isError)
    return (
      <div className='flex h-screen w-full flex-col items-center justify-center'>
        <h1 className='font-satisfy text-9xl font-bold'>404</h1>
        <h2 className='text-4xl font-medium'>Whoops, that page is gone.</h2>
      </div>
    )
  return (
    <>
      <UserProfile data={data} isLoading={isLoading} />
      <TabBar />
      <CollectionModal />
    </>
  )
}

export default ProfileSection
