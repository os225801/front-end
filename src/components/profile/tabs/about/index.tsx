import Introduction from './introduction'
import Prices from './prices'

const About = () => {
  return (
    <div className=' flex w-full flex-col justify-between  space-y-6 md:flex-row md:space-x-8 md:space-y-0'>
      <Introduction />
      <Prices />
    </div>
  )
}

export default About
