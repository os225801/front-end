const Introduction = () => {
  return (
    <>
      <div className='top-20 flex h-fit w-full flex-col space-y-6 border-b-2 border-[#f3f3f4] pb-10 md:sticky md:w-4/12'>
        <h1 className='mb-[14px] text-[16px] font-semibold leading-[16px]'>About me</h1>
        <p className='text-[16px] leading-[28px]'>
          Kelvin Zero is a technology leader revolutionizing digital security with next-gen passwordless solutions built
          for critical organizations. With, Multi-Pass Pro, they are helping organizations eliminate passwords and
          enable secure access with phishing resistant MFA. For this project development we used our standard tech
          stack, including GSAP, ScrollTrigger and Three.js, and a new smooth scrolling library Lenis.
        </p>
        <div className='flex items-center gap-4'>
          {/* <button className='social-btn btn btn-outline'>Facebook</button>
          <button className='social-btn btn btn-outline'>X</button>
          <button className='social-btn btn btn-outline'>Copy</button> */}
        </div>
      </div>
    </>
  )
}

export default Introduction
