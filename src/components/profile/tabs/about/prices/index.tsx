import PriceItem from './price-item'

const Prices = () => {
  return (
    <div className='flex w-full flex-col md:w-8/12'>
      <h1 className='mb-[14px] text-[16px] font-semibold leading-[16px]'>Prices • 3 </h1>
      <div className='flex flex-col gap-4'>
        <PriceItem />
        <PriceItem />
        <PriceItem />
        <PriceItem />
        <PriceItem />
      </div>
    </div>
  )
}

export default Prices
