import { cn } from '@/lib/utils/cn'
import Image from 'next/image'

const GridImageItem = ({ className }: { className?: string }) => {
  return (
    <div
      className={cn('relative aspect-square h-full w-full overflow-hidden rounded-lg hover:brightness-90', className)}
    >
      <div className='absolute inset-0 m-0 box-border block overflow-hidden'>
        <Image
          className='h-auto w-full rounded-lg '
          src='https://img.daisyui.com/images/stock/photo-1534528741775-53994a69daeb.jpg'
          alt='Laundry App - Dark Mode'
          fill
          priority
          sizes='(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 33vw'
          style={{ objectFit: 'cover' }}
        />
      </div>
    </div>
  )
}

const GridImageItemMobile = () => {
  return (
    <div className='relative flex h-40 w-40 shrink-0 overflow-hidden rounded-lg'>
      <div className='absolute inset-0 m-0 box-border block overflow-hidden'>
        <Image
          className='h-auto w-full rounded-lg '
          src='https://img.daisyui.com/images/stock/photo-1534528741775-53994a69daeb.jpg'
          alt='Laundry App - Dark Mode'
          fill
          sizes='(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 33vw'
          priority
          style={{ objectFit: 'cover' }}
        />
      </div>
    </div>
  )
}

const PriceItem = () => {
  return (
    <article className='flex w-full flex-col items-start md:rounded-lg md:shadow-xl '>
      <div className='scrollbar-hide -ml-4 mb-4 flex w-screen overflow-x-auto md:hidden'>
        <div className='mr-4 flex h-40 space-x-2 pl-4 md:mr-0 md:space-x-3 md:pl-0'>
          <GridImageItemMobile />
          <GridImageItemMobile />
          <GridImageItemMobile />
          <GridImageItemMobile />
          <GridImageItemMobile />
        </div>
      </div>
      <div className='hidden h-full max-h-[calc(50vh-32px)] w-full grid-cols-5 grid-rows-2 gap-1 md:grid md:px-6 md:pt-6'>
        <GridImageItem className='col-span-2 row-span-2' />
        <GridImageItem className='col-span-2 row-span-2' />
        <GridImageItem />
        <GridImageItem />
      </div>
      {/* Button & Name */}
      <div className='flex w-full flex-col pb-4 md:pb-0'>
        <div className='flex w-full items-center justify-between'>
          <h3 className='w-full cursor-pointer text-[24px] font-semibold leading-[28px] transition-all hover:underline md:pl-6 md:pr-2 md:pt-6'>
            Illustration
          </h3>
          <div className='flex shrink-0 items-center space-x-2 md:pr-6 md:pt-6'>
            <button className='btn'>View details</button>
          </div>
        </div>
      </div>
      {/* Price list */}
      <div className='w-full pb-4 pt-4 md:px-6 md:pb-6'></div>
    </article>
  )
}

export default PriceItem
