import Image from 'next/image'
import Link from 'next/link'

const CollectionItem = ({ collection }: { collection: CollectionPaginated }) => {
  return (
    <Link href={`/collection/${collection.id}`} className='group cursor-pointer'>
      {/* Collection Thumbnails */}
      <div className='relative w-full pb-[100%] transition-all group-hover:brightness-105'>
        <div className='absolute inset-0 flex flex-col'>
          <div className='shot-image h-[75%] rounded-t-lg'>
            {collection?.coverImage?.length > 0 && (
              <Image
                loader={({ src }) => src}
                src={collection?.coverImage?.[0]?.imageLink}
                alt={'Collection Cover'}
                layout='fill'
                objectFit='cover'
                className='rounded-t-lg'
              />
            )}
          </div>
          <div className='mt-1 flex h-[25%]'>
            <div className='shot-image other-shot-image'>
              {collection?.coverImage?.[1]?.imageLink && (
                <Image
                  loader={({ src }) => src}
                  src={collection?.coverImage?.[1]?.imageLink}
                  alt={'Collection Cover'}
                  layout='fill'
                  objectFit='cover'
                  className='rounded-t-lg'
                />
              )}
            </div>
            <div className='shot-image other-shot-image'>
              {collection?.coverImage?.[2]?.imageLink && (
                <Image
                  loader={({ src }) => src}
                  src={collection?.coverImage?.[2]?.imageLink}
                  alt={'Collection Cover'}
                  layout='fill'
                  objectFit='cover'
                  className='rounded-t-lg'
                />
              )}
            </div>
            <div className='shot-image other-shot-image'>
              {collection?.coverImage?.[3]?.imageLink && (
                <Image
                  loader={({ src }) => src}
                  src={collection?.coverImage?.[3]?.imageLink}
                  alt={'Collection Cover'}
                  layout='fill'
                  objectFit='cover'
                  className='rounded-t-lg'
                />
              )}
            </div>
          </div>
        </div>
      </div>
      {/* Collection Name & Stat */}
      <div className='mt-4 text-center'>
        <div className='text-[16px] font-medium leading-[22px] group-hover:underline'>{collection?.name}</div>
      </div>
    </Link>
  )
}

export default CollectionItem
