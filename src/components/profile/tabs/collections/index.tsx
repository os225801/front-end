'use client'
import Empty from '@/components/shared/list-posts/empty'
import Loading from '@/components/shared/list-posts/loading'
import { useCollectionsByUserId } from '@/hooks/use-collection-query'
import { useParams } from 'next/navigation'
import CollectionItem from './collection-item'

const Collections = () => {
  const { id } = useParams()
  const { data, isLoading, isError } = useCollectionsByUserId({
    userId: String(id),
  })

  if (isError) return <Empty />
  if (isLoading) return <Loading />
  if (!data?.items?.length) return <Empty />
  return (
    <div className='w-full min-w-0'>
      <div className='shots-grid w-full'>
        {data?.items?.map((collection) => <CollectionItem key={collection.id} collection={collection} />)}
      </div>
    </div>
  )
}

export default Collections
