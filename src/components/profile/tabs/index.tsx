'use client'
import { Tab, TabGroup, TabList, TabPanel, TabPanels } from '@headlessui/react'
import { useMemo } from 'react'
import Collections from './collections'
import Works from './works'

const TabBar = () => {
  const profileMenu = useMemo(() => {
    return {
      // About: {
      //   id: 1,
      //   content: <About />,
      // },
      Works: {
        id: 2,
        content: <Works />,
      },
      Collections: {
        id: 3,
        content: <Collections />,
      },
    }
  }, [])

  return (
    <TabGroup>
      <div className='z-10 flex w-screen max-w-[1380px] flex-col px-4'>
        <TabList className='w-full border-b border-[#e7e7e9] py-[30px]'>
          <div className='ml-4 flex gap-2 md:ml-0'>
            {Object.keys(profileMenu).map((item) => {
              return (
                <Tab key={item} className={`outline-none`}>
                  {({ selected }) => (
                    <div
                      className={`relative z-10 flex h-9 items-center justify-between rounded-full px-4 text-[14px] font-semibold leading-[15px]
                      transition-all duration-300 ease-in-out hover:bg-[#f8f7f4]
                      ${selected ? 'bg-[#f8f7f4] text-[#0d0c22]' : 'bg-transparent'}`}
                    >
                      <p className=''>{item}</p>
                    </div>
                  )}
                </Tab>
              )
            })}
          </div>
        </TabList>
        <TabPanels className='mx-auto w-full px-[12px] pt-[48px] md:px-0'>
          {Object.values(profileMenu).map((item, idx) => (
            <TabPanel key={idx} className={`flex w-full justify-center`}>
              {item.content}
            </TabPanel>
          ))}
        </TabPanels>
      </div>
    </TabGroup>
  )
}

export default TabBar
