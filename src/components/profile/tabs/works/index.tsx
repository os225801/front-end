'use client'

import CardThumbnail from '@/components/shared/card/card-thumbnail'
import Empty from '@/components/shared/list-posts/empty'
import Loading from '@/components/shared/list-posts/loading'
import { usePostsByUserId } from '@/hooks/use-post-query'
import { useParams } from 'next/navigation'

const Works = () => {
  const { id } = useParams()
  const { data, isError, isLoading } = usePostsByUserId(String(id))

  if (isLoading) return <Loading />
  if (isError) return <Empty />
  if (!data?.items?.length) return <Empty />
  return (
    <div className='w-full min-w-0'>
      <div className='shots-grid shots-grid-with-large-shots !gap-[48px]'>
        {data?.items?.map((post: Post) => <CardThumbnail key={post.id} {...post} />)}
      </div>
    </div>
  )
}

export default Works
