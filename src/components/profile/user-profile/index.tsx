'use client'
import EditProfileModal from '@/components/edit-profile/edit-profile-modal'
import Avatar from '@/components/shared/avatar'
import { useFollowAndUnfollowUser } from '@/hooks/use-profile-query'
import { useProfileStoreActions } from '@/store/profile.store'
import Image from 'next/image'
import { useEffect, useState } from 'react'
import { Gallery, Item } from 'react-photoswipe-gallery'

const UserProfile = ({ data, isLoading }: { data: Profile; isLoading: boolean }) => {
  const { followAndUnfollow, isPending } = useFollowAndUnfollowUser()
  const [currentUser, setCurrentUser] = useState<CurrentUser | null>(null)
  useEffect(() => {
    if (typeof window !== 'undefined' && window.localStorage) {
      const userString = window.localStorage.getItem('user')
      if (userString) {
        setCurrentUser(JSON.parse(userString) as CurrentUser)
      }
    }
  }, [])
  const isCurrentUser = String(currentUser?.id) === String(data?.id)
  const { setIsOpenEditProfileModal } = useProfileStoreActions()
  const handleFollowAndUnfollow = async () => {
    await followAndUnfollow(data?.id)
  }

  return (
    <section className='relative mb-10 block w-full max-w-[1440px] '>
      <EditProfileModal />
      <div className='flex w-full flex-col-reverse items-start justify-between px-0'>
        <div className='mb-4 w-full px-4 md:px-16'>
          <div className='flex flex-wrap justify-between'>
            {isLoading && !data?.avatarLink ? (
              <div className='skeleton z-10  mt-[-50%] h-[100px] w-[100px] shrink-0 rounded-full md:h-[200px] md:w-[200px]'></div>
            ) : (
              <Avatar
                src={data?.avatarLink}
                width={200}
                height={200}
                className='z-10 mt-[-50%] w-[100px] border-[8px] border-[#FCFCFD] md:w-[200px]'
              />
            )}
          </div>
          <div className='mt-8 flex w-full items-start justify-between'>
            {isLoading ? (
              <div>
                <div className='skeleton mb-[10px] h-[38px] w-full rounded-[4px]'></div>
                <div className='skeleton h-[56px] w-full rounded-[4px]'></div>
                <div className='skeleton mt-4 h-[29px] w-full rounded-[4px] '></div>
                <div className='skeleton mt-6 h-[62px] w-full rounded-[4px]'></div>
              </div>
            ) : (
              <div>
                <h1 className='mb-2 text-[20px] font-black leading-[120%] md:text-[39px]'>{data?.username}</h1>
                <h2 className='max-w-[600px] truncate whitespace-normal text-[16px] font-medium leading-[120%] '>
                  {data?.name}
                </h2>

                <div className='mt-2 flex flex-wrap text-sm font-bold leading-[28px] text-[#6e6d7a] md:text-[16px]'>
                  <div className='mr-6'>{data?.totalFollowers} followers</div>
                  <div>{data?.totalFollowings} following</div>
                </div>
              </div>
            )}

            {!isCurrentUser ? (
              <div className='z-10 flex flex-wrap transition-opacity'>
                <div className='flex'>
                  <button
                    disabled={isPending}
                    onClick={handleFollowAndUnfollow}
                    className='profile-action-item btn btn-outline'
                  >
                    {data.isFollowed ? 'Following' : 'Follow'}
                  </button>
                </div>
              </div>
            ) : (
              <div className='z-10 flex flex-wrap transition-opacity'>
                <div className='flex'>
                  <button
                    onClick={() => setIsOpenEditProfileModal(true)}
                    className='profile-action-item btn btn-outline'
                  >
                    Edit profile
                  </button>
                </div>
              </div>
            )}
          </div>
        </div>

        {isLoading && !data?.bannerLink ? (
          <div className='relative flex h-full w-full flex-grow'>
            <div className='flex h-full w-full justify-end'>
              <div className='relative max-h-[340px] w-full rounded-[16px] before:block before:pt-[75%] md:min-w-[326px]'></div>
            </div>
          </div>
        ) : (
          <div className='relative flex h-full w-full flex-grow'>
            <div className='flex h-full w-full justify-end'>
              <div className='relative max-h-[340px] w-full rounded-[16px] before:block before:pt-[75%] md:min-w-[326px]'>
                <Gallery
                  id='banner'
                  options={{
                    zoom: true,
                  }}
                >
                  <Item original={data?.bannerLink} thumbnail={data?.bannerLink} width='1024' height='768'>
                    {({ ref, open }) => (
                      <Image
                        className='h-full w-full rounded-[36px] object-cover'
                        fill
                        loader={({ src }) => src}
                        style={{ objectFit: 'cover' }}
                        ref={ref}
                        onClick={open}
                        src={data?.bannerLink}
                        priority
                        alt='banner'
                      />
                    )}
                  </Item>
                </Gallery>
              </div>
            </div>
          </div>
        )}
      </div>
    </section>
  )
}

export default UserProfile
