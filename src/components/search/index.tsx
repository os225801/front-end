import { postService } from '@/services/post.service'
import { dehydrate, HydrationBoundary, QueryClient } from '@tanstack/react-query'
import ListPosts from '../shared/list-posts'
import SearchSection from './search-section'

const SearchScreen = async () => {
  const queryClient = new QueryClient()

  await queryClient.prefetchQuery({
    queryKey: ['posts'],
    queryFn: async () => {
      const posts = await postService.getAllPosts({
        page: 1,
        size: 10,
        keyword: '',
        sortBy: 'createdAt',
        sortOrder: 'DESC',
        isBuyable: false
      })
      return posts
    },
  })

  return (
    <>
      <HydrationBoundary state={dehydrate(queryClient)}>
        <SearchSection />
        <ListPosts />
      </HydrationBoundary>
    </>
  )
}

export default SearchScreen
