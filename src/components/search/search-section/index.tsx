'use client'

import { useDebouncedValue } from '@/hooks/use-debounce-value'
import { useInfiniteTags } from '@/hooks/use-tag-query'
import { useTagNameShow, useTagStoreActions } from '@/store/tag.store'
import Link from 'next/link'
import { useRouter } from 'next/navigation'
import { useState } from 'react'

const SearchSection = () => {
  const searchValue = useTagNameShow()
  const [debounced] = useDebouncedValue(searchValue, 200)
  const [isInputFocused, setIsInputFocused] = useState(false)
  const { setTagNameShow, setId, setTagName } = useTagStoreActions()
  const router = useRouter()
  const { data } = useInfiniteTags({
    searchTerm: debounced,
  })

  return (
    <div className='relative flex w-full flex-col items-center'>
      <div className='background-bar'></div>
      <div className='box-border w-full px-4 text-center'>
        <div className='mx-auto max-w-[628px] -translate-y-1/2'>
          <div className='relative box-border flex h-16 items-center rounded-lg bg-[#FFF] px-5 shadow-[0px_8px_20px_rgba(0,0,0,0.06)]'>
            <svg
              xmlns='http://www.w3.org/2000/svg'
              width='16'
              height='16'
              viewBox='0 0 16 16'
              fill='none'
              role='img'
              className='icon search-icon fill-current'
            >
              <path
                fillRule='evenodd'
                clipRule='evenodd'
                d='M10.6002 12.0498C9.49758 12.8568 8.13777 13.3333 6.66667 13.3333C2.98477 13.3333 0 10.3486 0 6.66667C0 2.98477 2.98477 0 6.66667 0C10.3486 0 13.3333 2.98477 13.3333 6.66667C13.3333 8.15637 12.8447 9.53194 12.019 10.6419C12.0265 10.6489 12.0338 10.656 12.0411 10.6633L15.2935 13.9157C15.6841 14.3063 15.6841 14.9394 15.2935 15.33C14.903 15.7205 14.2699 15.7205 13.8793 15.33L10.6269 12.0775C10.6178 12.0684 10.6089 12.0592 10.6002 12.0498ZM11.3333 6.66667C11.3333 9.244 9.244 11.3333 6.66667 11.3333C4.08934 11.3333 2 9.244 2 6.66667C2 4.08934 4.08934 2 6.66667 2C9.244 2 11.3333 4.08934 11.3333 6.66667Z'
              ></path>
            </svg>
            <form action='/search' className='absolute inset-0'>
              <input
                id='search'
                type='search'
                name='q'
                value={searchValue}
                onChange={(e) => setTagNameShow(e.target.value)}
                onFocus={() => setIsInputFocused(true)}
                onBlur={() => setTimeout(() => setIsInputFocused(false), 200)}
                placeholder='Search...'
                className='h-full w-full bg-transparent pl-[60px] pr-6 focus:outline-none'
              ></input>
            </form>
            {isInputFocused && searchValue.length > 0 && data && (
              <>
                <div className='absolute left-[0px] top-[100%] z-10 mt-2 max-h-[500px] w-full overflow-hidden rounded-xl bg-base-100 shadow-lg'>
                  {data?.pages?.map((page) => (
                    <div key={page.pageNumber}>
                      {page?.items?.map((tag: Tag) => (
                        <Link key={tag.id} href={`/search?q=${tag.id}`}>
                          <div
                            onClick={(e) => {
                              e.preventDefault()
                              e.stopPropagation()
                              setTagNameShow(tag.tagName)
                              setId('')
                              setTagName('')
                              router.push(`/search?q=${tag.id}`) // Modify this line
                            }}
                            className='flex items-center justify-between px-2 py-4 text-base font-medium hover:bg-base-200'
                          >
                            <span>{tag.tagName}</span>
                            <svg viewBox='0 0 24 24' className='icon max-h-[16px] max-w-[16px] fill-current'>
                              <path
                                d='M3.41421 2H9C9.55228 2 10 1.55228 10 1C10 0.447715 9.55228 0 9 0H0V9.25C0 9.80229 0.447715 10.25 1 10.25C1.55229 10.25 2 9.80229 2 9.25V3.41421L9.29289 10.7071C9.68342 11.0976 10.3166 11.0976 10.7071 10.7071C11.0976 10.3166 11.0976 9.68342 10.7071 9.29289L3.41421 2Z'
                                transform='translate(6 6)'
                              ></path>
                            </svg>
                          </div>
                        </Link>
                      ))}
                    </div>
                  ))}
                </div>
              </>
            )}
          </div>
        </div>
        <h1 className='text-[32px] font-bold leading-[38px]'>{debounced}</h1>
        <p className='mx-auto my-3 max-w-[700px] px-4 text-center text-base'>
          Amazing {debounced} work, designs, illustrations, and graphic elements
        </p>
      </div>
    </div>
  )
}

export default SearchSection
