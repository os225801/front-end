'use client'
import { cn } from '@/lib/utils/cn'
import Image from 'next/image'

type AvatarProps = {
  src: string
  width: number
  height: number
  className?: string
}

const Avatar: React.FC<AvatarProps> = ({ src, width, height, className }) => {
  return (
    <div className='avatar'>
      <div className={cn('w-6 rounded-full', className)}>
        {src && <Image loader={({ src }) => src} src={src} alt='ava' width={width} height={height} unoptimized />}
      </div>
    </div>
  )
}

export default Avatar
