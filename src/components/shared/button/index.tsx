'use client'

import { DeletePostModal } from '@/components/post/delete-post-modal'
import { PurchaseArtworkModal } from '@/components/post/purchase-artwork-modal'
import { useDeletePost, useLikeAndUnlikePost } from '@/hooks/use-post-query'
import { artworkService } from '@/services/artwork.service'
import { orderService } from '@/services/order.service'
import clsx from 'clsx'
import { useRouter } from 'next/navigation'
import { PropsWithChildren, useState } from 'react'
import { FaHeart, FaRegHeart } from 'react-icons/fa'
import { GoDownload } from 'react-icons/go'
import { MdDeleteOutline, MdOutlineShoppingCart } from 'react-icons/md'
import { TiEdit } from 'react-icons/ti'
import { useToast } from '../toast'

type ButtonProps = {
  className?: string
  onClick?: () => void
  variant?: 'blue' | 'white' | 'black' | 'reply' | 'info' | 'white-no-border'
  type?: 'button' | 'submit' | 'reset' | undefined
  disabled?: boolean
}

export const Button = (ButtonProps: PropsWithChildren<ButtonProps>) => {
  const { className, onClick, variant, children, type, disabled } = ButtonProps

  return (
    <div className={`${className} flex items-center justify-center`}>
      <button
        className={clsx('flex h-full w-full items-center justify-center rounded-full', {
          'bg-sky-500 px-3 text-white hover:bg-sky-400': variant === 'blue' && !disabled,
          'bg-sky-300 px-3 text-white': variant === 'blue' && disabled,
          'border-2 border-[#D9D9D9] px-3': variant === 'white',
          'border-0 px-3 py-1 transition-colors duration-200 ease-in-out hover:bg-gray-300':
            variant === 'white-no-border',
          'bg-slate-900 px-3 font-bold text-white hover:bg-slate-800': variant === 'black',
          'px-3 text-sky-500': variant === 'reply',
          'bg-sky-500 px-[5px] py-[8px] text-white hover:bg-sky-400': variant === 'info',
        })}
        onClick={onClick}
        type={type}
        disabled={disabled}
      >
        {children}
      </button>
    </div>
  )
}

export const BuyButton = (props: { artworkId: string }) => {
  const [isModalOpen, setModalOpen] = useState(false)
  const { showToast } = useToast()
  const router = useRouter()
  return (
    <>
      <MdOutlineShoppingCart
        className='h-[25px] w-[25px] cursor-pointer'
        onClick={async () => {
          setModalOpen(true)
        }}
      />
      <PurchaseArtworkModal
        isOpen={isModalOpen}
        close={() => {
          setModalOpen(false)
        }}
        onClickConfirm={async () => {
          try {
            const res = await orderService.createOrder(props.artworkId)
            if (res.paymentLink) {
              router.push(res.paymentLink)
            }
          } catch (e) {
            showToast('You have already ordered this artwork', 'success')
            router.push('/orders')
          }
        }}
      />
    </>
  )
}

export const EditButton = () => {
  return <TiEdit className='h-[25px] w-[25px]' />
}

export const DeleteButton = (props: { postId: string; userId: string }) => {
  const [isModalOpen, setModalOpen] = useState(false)

  const { mutateAsync } = useDeletePost()

  const router = useRouter()

  return (
    <div>
      <MdDeleteOutline className='h-[25px] w-[25px] cursor-pointer' onClick={() => setModalOpen(true)} />
      <DeletePostModal
        isOpen={isModalOpen}
        close={() => {
          setModalOpen(false)
        }}
        onClickConfirm={async () => {
          await mutateAsync({
            postId: props.postId,
          })
          router.push('/')
        }}
      />
    </div>
  )
}

export const LikeButton = (props: { postId: string; isLiked: boolean }) => {
  const { likeOrUnlike } = useLikeAndUnlikePost(props.postId)
  return (
    <>
      {!props.isLiked && (
        <FaRegHeart
          className='h-[25px] w-[25px] cursor-pointer'
          onClick={async () => {
            await likeOrUnlike(props.postId)
          }}
        />
      )}
      {props.isLiked && (
        <FaHeart
          className='h-[25px] w-[25px] cursor-pointer text-[#ff2f2f]'
          onClick={async () => {
            await likeOrUnlike(props.postId)
          }}
        />
      )}
    </>
  )
}

export const DownloadButton = (props: { artworkId: string; title: string }) => {
  return (
    <GoDownload
      className='h-[25px] w-[25px] cursor-pointer'
      onClick={() => {
        const filename = props.title.replaceAll(' ', '_').toLowerCase()
        artworkService.downloadArtwork(props.artworkId, filename)
      }}
    />
  )
}
