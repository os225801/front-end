import { useSession } from '@/context/session-context'
import { useLikeAndUnlikePost } from '@/hooks/use-post-query'
import { redirectLogin } from '@/lib/utils/redirectLogin'
import { useCollectionStoreActions } from '@/store/collection.store'
import { usePostIsLoading } from '@/store/post.store'
import { useQueryClient } from '@tanstack/react-query'
import Image from 'next/image'
import Link from 'next/link'

const CardThumbnail: React.FC<Post> = (post) => {
  const { likeOrUnlike } = useLikeAndUnlikePost(post.id)
  const { setIsOpenCollectionModal, setPostIdToBeAdded } = useCollectionStoreActions()
  const isLoading = usePostIsLoading()
  const { userToken } = useSession() as SessionData
  const queryClient = useQueryClient()

  const openCollectionModal = () => {
    queryClient.invalidateQueries({
      queryKey: ['collections'],
    })
    setIsOpenCollectionModal(true)
  }

  return (
    <Link href={`/posts/${post.id}`}>
      <div className='group relative'>
        <div className='h-0 overflow-hidden pb-[75%]'>
          <figure className=' relative before:block before:pt-[75%]'>
            {post.artwork.image ? (
              <Image
                className='h-auto w-full rounded-lg '
                loader={({ src }) => src}
                src={post.artwork.image}
                alt='Thumbnail'
                fill
                priority
                unoptimized
                style={{ objectFit: 'cover' }}
              />
            ) : (
              <div className='skeleton h-auto w-full rounded-lg'></div>
            )}
          </figure>
        </div>
        {/* Layer 2 */}
        <div className='absolute inset-0 flex items-end justify-between rounded-lg bg-gradient-to-b from-transparent to-black opacity-0 transition-opacity duration-300 group-hover:opacity-100'>
          <div className='flex w-full items-center justify-between p-5 text-white '>
            <h3 className='truncate text-[16px] font-semibold leading-[22px] '>{post.title}</h3>
            <div className='flex items-center justify-between '>
              <button
                onClick={async (e) => {
                  e.preventDefault()
                  if (userToken) {
                    setPostIdToBeAdded(post.id)
                    openCollectionModal()
                  } else {
                    redirectLogin()
                  }
                }}
                className='group/btn-mark ml-3 flex h-10 w-10 items-center justify-center rounded-full bg-white'
              >
                <svg
                  xmlns='http://www.w3.org/2000/svg'
                  width='16'
                  height='16'
                  viewBox='0 0 16 16'
                  fill='none'
                  role='img'
                  className='icon group-hover/btn-mark:opacity-50'
                >
                  <path
                    d='M3.33337 5.2C3.33337 4.0799 3.33337 3.51984 3.55136 3.09202C3.74311 2.71569 4.04907 2.40973 4.42539 2.21799C4.85322 2 5.41327 2 6.53337 2H9.46671C10.5868 2 11.1469 2 11.5747 2.21799C11.951 2.40973 12.257 2.71569 12.4487 3.09202C12.6667 3.51984 12.6667 4.0799 12.6667 5.2V14L8.00004 11.3333L3.33337 14V5.2Z'
                    stroke='#0d0c22'
                    strokeWidth='1.5'
                    strokeLinecap='round'
                    strokeLinejoin='round'
                  ></path>
                </svg>
              </button>
              <button
                onClick={async (e) => {
                  e.preventDefault()
                  if (userToken) {
                    await likeOrUnlike(post.id)
                  } else {
                    redirectLogin()
                  }
                }}
                className='group/btn-like ml-3 flex h-10 w-10 items-center justify-center rounded-full bg-white'
              >
                {!isLoading ? (
                  <svg
                    xmlns='http://www.w3.org/2000/svg'
                    width='16'
                    height='16'
                    viewBox='0 0 16 16'
                    fill={post.isLiked ? '#ff2f2f' : 'none'}
                    role='img'
                    className={`ion-icon group-hover/btn-like:opacity-50 ${!isLoading ? 'like-animation' : ''}`}
                  >
                    <path
                      d='M10.7408 2C13.0889 2 14.6667 4.235 14.6667 6.32C14.6667 10.5425 8.11856 14 8.00004 14C7.88152 14 1.33337 10.5425 1.33337 6.32C1.33337 4.235 2.91115 2 5.2593 2C6.60745 2 7.48893 2.6825 8.00004 3.2825C8.51115 2.6825 9.39263 2 10.7408 2Z'
                      stroke={post.isLiked ? '#ff2f2f' : '#0d0c22'}
                      strokeWidth='1.5'
                      strokeLinecap='round'
                      strokeLinejoin='round'
                    ></path>
                  </svg>
                ) : (
                  <div className='flex'>
                    <span className='loading loading-spinner loading-xs bg-base-content'></span>
                  </div>
                )}
              </button>
            </div>
          </div>
        </div>
      </div>
    </Link>
  )
}

export default CardThumbnail
