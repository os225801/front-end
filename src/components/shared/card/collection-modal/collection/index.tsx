import { useAddOrRemovePostFromCollection } from '@/hooks/use-collection-query'
import check from '@/images/icon-check.png'
import minus from '@/images/icon-minus.png'
import { usePostIdToBeAdded } from '@/store/collection.store'
import Image from 'next/image'
import { useState } from 'react'

const CollectionItem = ({ collection }: { collection: Collection }) => {
  const { addOrRemovePostFromCollection } = useAddOrRemovePostFromCollection()
  const postIdToBeAdded = usePostIdToBeAdded()
  const [isHovered, setIsHovered] = useState(false)
  return (
    <article
      onMouseEnter={() => setIsHovered(true)}
      onMouseLeave={() => setIsHovered(false)}
      onClick={() => addOrRemovePostFromCollection({ collectionId: collection.id, postId: postIdToBeAdded })}
      className='m-0 mt-1 text-sm'
    >
      <div className='relative block min-h-[68px] cursor-pointer overflow-hidden rounded-[5px] border bg-transparent	p-[6px] pl-[86px]	transition-all hover:opacity-65 '>
        {collection.coverImage ? (
          <div className='absolute left-[6px] top-[4px] h-[58px] w-[60px]'>
            <Image
              loader={({ src }) => src}
              src={collection.coverImage}
              alt={collection.name}
              width={60}
              height={60}
              unoptimized
              className='rounded-[5px]'
            />
          </div>
        ) : (
          <div className='skeleton absolute left-[6px] top-[4px] h-[58px] w-[60px] rounded-[5px] bg-gray-200' />
        )}
        <strong className='font-bold'>{collection.name}</strong>
        <p className='mt-1 text-xs text-base-content'>{collection.totalPosts} shots</p>
        {collection.status ? (
          <Image
            src={isHovered ? minus : check}
            alt={isHovered ? 'minus' : 'check'}
            width={40}
            height={40}
            className='absolute right-[6px] top-1/2 -translate-y-1/2 transform'
          />
        ) : null}
      </div>
    </article>
  )
}

export default CollectionItem
