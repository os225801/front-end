'use client'
import FormFieldInput from '@/components/shared/form-input'
import Textarea from '@/components/shared/form/textarea'
import { CreateCollectionSchema } from '@/schema/create-collection.schema'
import { collectionService } from '@/services/collection.service'
import { useCollectionStoreActions } from '@/store/collection.store'
import { zodResolver } from '@hookform/resolvers/zod'
import { useQueryClient } from '@tanstack/react-query'
import { SubmitHandler, useForm } from 'react-hook-form'
import { z } from 'zod'

type FormFields = z.infer<typeof CreateCollectionSchema>

const CreateCollectionForm = () => {
  const {
    register,
    handleSubmit,
    formState: { errors, isSubmitting },
  } = useForm<FormFields>({
    resolver: zodResolver(CreateCollectionSchema),
  })

  const { setFormCreateCollection } = useCollectionStoreActions()
  const queryClient = useQueryClient()
  const onSubmit: SubmitHandler<FormFields> = async (data) => {
    const payload = {
      name: data.title,
      description: data.description,
    }
    const newCollection = await collectionService.createCollection({ data: payload })
    if (!newCollection) {
      //handle error
      return
    } else {
      //handle success
      queryClient.invalidateQueries({
        queryKey: ['collections'],
      })
      setFormCreateCollection(false)
    }
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <div className='relative mb-[25px] h-[300px] overflow-auto'>
        <FormFieldInput
          lable='Title'
          name='title'
          error={errors.title?.message}
          placeholder='Enter title'
          type='text'
          register={register}
        />
        <Textarea
          lable='Description'
          name='description'
          error={errors.description?.message}
          placeholder='Enter description'
          register={register}
        />
      </div>
      <div className='flex w-full items-center gap-2'>
        <button type='submit' disabled={isSubmitting} className='btn btn-outline'>
          {isSubmitting ? 'Loading...' : 'Create collection'}
        </button>
        <button type='button' onClick={() => setFormCreateCollection(false)} className='btn btn-active'>
          Cancel
        </button>
      </div>
    </form>
  )
}

export default CreateCollectionForm
