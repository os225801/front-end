import Modal from '@/components/shared/modal'
import { useCollections } from '@/hooks/use-collection-query'
import {
  useCollectionIsOpenCollectionModal,
  useCollectionStoreActions,
  useFormCreateCollection,
} from '@/store/collection.store'
import { Fragment } from 'react'
import Empty from '../../list-posts/empty'
import CollectionItem from './collection'
import CreateCollectionForm from './create-collection-form'

const CollectionModal = () => {
  const isOpen = useCollectionIsOpenCollectionModal()
  const { setIsOpenCollectionModal, setFormCreateCollection } = useCollectionStoreActions()
  const formCreateCollection = useFormCreateCollection()

  const close = () => {
    setIsOpenCollectionModal(false)
  }

  const { data, isError, isLoading } = useCollections()

  return (
    <Modal title={formCreateCollection ? 'Create a new collection' : 'Collection'} closeModal={close} isOpen={isOpen}>
      {!formCreateCollection ? (
        <ol className='relative mb-[25px] h-[300px] overflow-auto'>
          {isError && (
            <div className='flex h-full w-full items-center justify-center'>
              <Empty />
            </div>
          )}
          {isLoading && (
            <Fragment>
              {Array.from({ length: 4 }).map((_, index) => (
                <div
                  key={index}
                  className='skeleton mt-1 block min-h-[68px] cursor-pointer overflow-hidden rounded-[5px] p-[6px]'
                ></div>
              ))}
            </Fragment>
          )}
          {data?.items
            ?.slice()
            .reverse()
            .map((collection: Collection) => <CollectionItem key={collection.id} collection={collection} />)}
        </ol>
      ) : (
        <CreateCollectionForm />
      )}
      {!formCreateCollection && (
        <div className='flex w-full justify-between'>
          <button onClick={() => setFormCreateCollection(true)} className='btn btn-outline'>
            Create a new collection
          </button>
          <button onClick={close} className='btn btn-active'>
            Done
          </button>
        </div>
      )}
    </Modal>
  )
}

export default CollectionModal
