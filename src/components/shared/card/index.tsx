import CardDetail from './card-detail'
import CardThumbnail from './card-thumbnail'

export const Card: React.FC<Post> = (post) => {
  return (
    <article className='flex flex-col'>
      <CardThumbnail {...post} />
      <CardDetail {...post} />
    </article>
  )
}
