'use client'

import { useDeleteComment, useInfiniteSubComments } from '@/hooks/use-comment-query'
import { timeAgo } from '@/lib/utils/formatTimeAgo'
import clsx from 'clsx'
import { Fragment, useEffect, useState } from 'react'
import Avatar from '../../avatar'
import { Button } from '../../button'
import { CommentForm } from '../../form/comment-form'

type CommentProps = {
  avatar: string
  content?: string
  parentId: string
  isRoot: boolean
  createdAt: string
  postId: string
  username: string
  userId: string
}

export const Comment = (CommentProps: CommentProps) => {
  const { avatar, content, parentId, isRoot, createdAt, postId, username, userId } = CommentProps
  const [showPopup, setShowPopup] = useState(false)
  const { mutateAsync: deleteComment } = useDeleteComment(parentId)
  const [currentUser, setCurrentUser] = useState<CurrentUser | null>(null)
  useEffect(() => {
    if (typeof window !== 'undefined' && window.localStorage) {
      const userString = window.localStorage.getItem('user')
      if (userString) {
        setCurrentUser(JSON.parse(userString) as CurrentUser)
      }
    }
  }, [])
  const [isReplying, setIsReplying] = useState(false)

  const time = new Date(createdAt)

  const {
    data: subComments,
    fetchNextPage,
    hasNextPage,
    isFetching,
    isFetchingNextPage,
  } = useInfiniteSubComments({ parentId: parentId })

  return (
    <div
      className={clsx('mt-3 flex flex-col', {
        'ml-5': isRoot === false,
      })}
    >
      <div className='flex h-auto'>
        <div className='mr-2'>
          <div className='flex items-center'>
            <Avatar src={avatar} width={24} height={24} />
          </div>
        </div>
        <div className='flex h-auto flex-col'>
          <div className='flex items-center gap-2'>
            <p className='text-xs font-bold'>{username}</p>
            <p className='over h-auto w-fit hyphens-auto whitespace-pre-line text-wrap break-all text-base'>
              {content}
            </p>
          </div>
          <div className=' flex gap-x-3 text-xs'>
            <p className='pt-[4px]'>{timeAgo(String(time))}</p>
            <Button
              variant='reply'
              className='h-[23px] w-[60px]'
              onClick={() => {
                setIsReplying(true)
              }}
            >
              Reply
            </Button>
            {currentUser?.id === userId && (
              <Button
                variant='reply'
                className='h-[23px] w-[60px]'
                onClick={() => {
                  setShowPopup(true)
                }}
              >
                Delete
              </Button>
            )}
          </div>
          {showPopup && (
            <div className='flex gap-x-3 font-bold'>
              <p>Are you sure you want to delete this comment?</p>
              <Button
                variant='reply'
                onClick={() => {
                  deleteComment({ commentId: parentId })
                }}
              >
                Yes
              </Button>
              <Button
                variant='reply'
                onClick={() => {
                  setShowPopup(false)
                }}
              >
                No
              </Button>
            </div>
          )}
        </div>
      </div>
      {isReplying && (
        <CommentForm
          avatar={avatar}
          postId={postId}
          isReply={true}
          onCancel={() => {
            setIsReplying(false)
          }}
          onSend={() => {
            setIsReplying(false)
          }}
          parentId={parentId}
        ></CommentForm>
      )}
      {subComments?.pages[0]?.items ? (
        <>
          {subComments?.pages?.map((group, i) => (
            <Fragment key={i}>
              {group?.items ? (
                <>
                  {group?.items?.map((subComment: Comment) => (
                    <Fragment key={subComment.id}>
                      <Comment
                        avatar={subComment.user.avatar}
                        createdAt={subComment.createdAt}
                        isRoot={false}
                        postId={subComment.postId}
                        username={subComment.user.username}
                        content={subComment.content}
                        parentId={subComment.id}
                        userId={subComment.user.userId}
                      />
                    </Fragment>
                  ))}
                </>
              ) : null}
            </Fragment>
          ))}
          {hasNextPage && !isFetchingNextPage && (
            <div className='flex justify-start pl-12'>
              <button
                className='mt-4 rounded-full bg-base-200 px-[20px] py-[10px] text-sm font-semibold text-base-content hover:opacity-60'
                onClick={() => fetchNextPage()}
                disabled={!hasNextPage || isFetchingNextPage}
              >
                {isFetching ? 'Loading...' : 'Load more replies'}
              </button>
            </div>
          )}
        </>
      ) : (
        <></>
      )}
    </div>
  )
}
