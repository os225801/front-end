'use client'

import { useInfiniteComments } from '@/hooks/use-comment-query'
import { Fragment } from 'react'
import { Comment } from './comment'

type CommentsProps = {
  postId: string
}

export const CommentList = (props: CommentsProps) => {
  const {
    data: comments,
    fetchNextPage,
    hasNextPage,
    isFetching,
    isFetchingNextPage,
  } = useInfiniteComments({ postId: props.postId })

  return (
    <div className='flex max-h-[500px] flex-1 flex-col items-start overflow-y-scroll'>
      {comments?.pages[0]?.items ? (
        <>
          {comments?.pages?.map((group, i) => (
            <Fragment key={i}>
              {group?.items ? (
                <>
                  {group?.items?.map((comment: Comment) => (
                    <Fragment key={comment.id}>
                      <Comment
                        avatar={comment.user.avatar}
                        createdAt={comment.createdAt}
                        isRoot={true}
                        postId={comment.postId}
                        username={comment.user.username}
                        content={comment.content}
                        parentId={comment.id}
                        userId={comment.user.userId}
                      />
                    </Fragment>
                  ))}
                </>
              ) : null}
            </Fragment>
          ))}
        </>
      ) : (
        <div className='flex flex-grow flex-col items-center justify-center'>
          <div className='mt-8 text-center text-4xl font-bold text-base-content'>No comments found</div>
        </div>
      )}
      {hasNextPage && !isFetchingNextPage && (
        <div className='flex justify-center'>
          <button
            className='mt-4 rounded-full bg-base-200 px-[20px] py-[10px] text-sm font-semibold text-base-content hover:opacity-60'
            onClick={() => fetchNextPage()}
            disabled={!hasNextPage || isFetchingNextPage}
          >
            {isFetching ? 'Loading...' : 'Load more comments'}
          </button>
        </div>
      )}
    </div>
  )
}
