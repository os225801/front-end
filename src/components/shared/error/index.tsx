const Error = () => {
  return (
    <section className='flex h-full w-full flex-1 items-center justify-center'>
      <div className='text-center'>
        <h1 className='text-4xl font-bold'>Error</h1>
        <p className=''>An error occurred while fetching data</p>
      </div>
    </section>
  )
}

export default Error
