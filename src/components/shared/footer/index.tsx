import OmniStroke from '@/images/Omnistroke.png'
import Image from 'next/image'
import Link from 'next/link'

const Footer = () => {
  return (
    <footer className='z-50 hidden w-full justify-between border-t px-[24px] py-[7.5px] md:flex'>
      <div className='flex items-center gap-6 text-[12px] font-bold'>
        <Link href={'/privacy-policy'} className='link-hover link'>
          Privacy Policy
        </Link>
        <Link href={'/terms-of-service'} className='link-hover link'>
          Terms of Service
        </Link>
        <Link
          target='_blank'
          rel='noopener noreferrer'
          href={'https://www.facebook.com/profile.php?id=61560444870606'}
          className='link-hover link'
        >
          Facebook
        </Link>
        <Link target='_blank' rel='noopener noreferrer' href={'https://x.com/OmniStrokes'} className='link-hover link'>
          Twitter
        </Link>
      </div>
      <div className='flex items-center gap-2'>
        <Image src={OmniStroke} alt='Omnistroke' width={20} height={20} />
        <span className='text-[12px] font-bold '>© 2024 OmniStroke</span>
      </div>
    </footer>
  )
}

export default Footer
