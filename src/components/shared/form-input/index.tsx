import { useIsDisable } from '@/store/disable.store'

const FormFieldInput: React.FC<FormFieldProps> = ({
  prefix,
  subfix,
  type,
  placeholder,
  name,
  register,
  error,
  valueAsNumber,
  lable,
}) => {
  const isDisable = useIsDisable()
  return (
    <div className='w-full'>
      <div className='label'>
        <span className='label-text'>{lable}</span>
      </div>
      <label className='input input-bordered flex w-[100%] items-center gap-2'>
        {prefix && prefix}
        <input
          {...register(name, { valueAsNumber })}
          min={0}
          type={type}
          id=''
          className='grow text-[16px]'
          placeholder={placeholder}
          disabled={isDisable}
        />
        {subfix && subfix}
      </label>
      <div className='label'>
        <span className='label-text-alt text-[12px] text-red-500'>{error}</span>
      </div>
    </div>
  )
}

export default FormFieldInput
