'use client'

import { Button } from '../../button'

import { useCreateComment } from '@/hooks/use-comment-query'
import clsx from 'clsx'
import { useEffect, useState } from 'react'
import { useForm } from 'react-hook-form'
import { useToast } from '../../toast'

type FormProps = {
  avatar: string
  postId: string
  isReply: boolean
  parentId?: string
  onCancel?: () => void
  onSend?: () => void
}

export const CommentForm = (FormProps: FormProps) => {
  const { postId, isReply, parentId, onCancel, onSend } = FormProps
  const { register, handleSubmit, reset } = useForm()
  const [currentUser, setCurrentUser] = useState<CurrentUser | null>(null)
  useEffect(() => {
    if (typeof window !== 'undefined' && window.localStorage) {
      const userString = window.localStorage.getItem('user')
      if (userString) {
        setCurrentUser(JSON.parse(userString) as CurrentUser)
      }
    }
  }, [])
  const { mutateAsync: createComment, isPending } = useCreateComment()
  const { showToast } = useToast()
  return (
    <form
      className={clsx('mt-2 flex flex-col items-start gap-2 ', {
        'pl-10': isReply,
      })}
      onSubmit={handleSubmit((e) => {
        if (currentUser) {
          if (e['content'].trim().length > 0) {
            createComment({ postId: postId, content: e['content'], parentCommentId: parentId })
            if (onSend) {
              onSend()
            }
            reset()
          } else {
            reset()
          }
        } else {
          showToast('Please login to comment', 'error')
          reset()
        }
      })}
    >
      <textarea
        id='cm-txt'
        rows={1}
        wrap='soft'
        className='mr-2 h-[28px] max-h-[106px] min-h-[106px] w-full resize-none overflow-auto rounded-lg border px-3 py-[5px] text-sm md:w-full'
        placeholder='Leave a comment'
        disabled={isPending}
        {...register('content', { required: true })}
      />
      <div className='flex '>
        {isReply === true && (
          <Button className='mr-2 h-[28px] w-20 text-sm font-bold' variant='white' type='button' onClick={onCancel}>
            Cancel
          </Button>
        )}
        <Button className='h-[28px] w-20 min-w-[80px] text-sm font-bold' variant='blue' type='submit'>
          {' '}
          Send{' '}
        </Button>
      </div>
    </form>
  )
}
