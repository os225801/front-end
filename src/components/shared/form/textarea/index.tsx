const Textarea: React.FC<FormFieldProps> = ({
  prefix,
  subfix,
  placeholder,
  name,
  register,
  error,
  valueAsNumber,
  lable,
}) => {
  return (
    <div className='w-full'>
      <div className='label'>
        <span className='label-text'>{lable}</span>
      </div>
      <label className='textarea textarea-bordered flex w-[100%] items-center gap-2'>
        {prefix && prefix}
        <textarea
          {...register(name, { valueAsNumber })}
          className='w-full grow resize-none text-[16px] focus:outline-none'
          rows={3}
          maxLength={255}
          placeholder={placeholder}
        ></textarea>
        {subfix && subfix}
      </label>
      <div className='label'>
        <span className='label-text-alt text-[12px] text-red-500'>{error}</span>
      </div>
    </div>
  )
}

export default Textarea
