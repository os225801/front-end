/* eslint-disable @typescript-eslint/no-explicit-any */
import PhotoSwipeLightbox from 'photoswipe/lightbox'
import 'photoswipe/style.css'
import { useEffect } from 'react'

export default function SimpleGallery(props: any) {
  useEffect(() => {
    const lightbox = new PhotoSwipeLightbox({
      gallery: '#' + props.galleryID,
      children: 'a',
      pswpModule: () => import('photoswipe'),
    })
    lightbox.init()

    return () => {
      lightbox.destroy()
    }
  }, [props.galleryID])

  return (
    <div className='pswp-gallery' id={props.galleryID}>
      {props.images.map((image: any, index: number) => (
        <a
          href={image.largeURL}
          data-pswp-width={image.width}
          data-pswp-height={image.height}
          key={props.galleryID + '-' + index}
          target='_blank'
          rel='noreferrer'
        >
          <img src={image.thumbnailURL} alt='' />
        </a>
      ))}
    </div>
  )
}
