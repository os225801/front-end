import { useCurrentUser } from '@/hooks/use-profile-query'
import { Menu, MenuButton, MenuItem, MenuItems } from '@headlessui/react'
import Link from 'next/link'
import { useEffect } from 'react'
import Avatar from '../../avatar'

const DropDown = () => {
  const { data } = useCurrentUser()

  const handleLogout = async () => {
    // await userService.logOut()
    localStorage.clear()
    window.location.href = '/'
  }

  useEffect(() => {
    if (data) {
      localStorage.setItem('user', JSON.stringify(data))
    }
  }, [data])

  return (
    <Menu>
      <MenuButton>
        <Avatar src={data?.avatarLink} width={48} height={48} className='w-12 cursor-pointer' />
      </MenuButton>
      <MenuItems
        transition
        anchor='bottom end'
        className='[--anchor-gap:var(--spacing-1)]focus:outline-none z-[1] w-80 origin-top-right rounded-box border bg-white p-8 shadow-lg transition duration-100 ease-out data-[closed]:scale-95 data-[closed]:opacity-0 '
      >
        <MenuItem>
          <div className='flex w-full flex-col items-center justify-center pb-5'>
            <Avatar src={data?.avatarLink} width={80} height={80} className='w-20' />
            <span className='mt-4 text-sm font-medium'>{data?.name}</span>
          </div>
        </MenuItem>
        <MenuItem>
          <Link href={'/create-post'}>
            <div className='cursor-pointer rounded-lg px-2 py-3 text-[#626262] transition-all hover:bg-[#ECECEC]'>
              Upload design work
            </div>
          </Link>
        </MenuItem>
        <MenuItem>
          <Link href={`/profile/${data?.id}`}>
            <div className='cursor-pointer rounded-lg px-2 py-3 text-[#626262] transition-all hover:bg-[#ECECEC]'>
              Profile
            </div>
          </Link>
        </MenuItem>
        <MenuItem>
          <Link href={`/wallet`}>
            <div className='cursor-pointer rounded-lg px-2 py-3 text-[#626262] transition-all hover:bg-[#ECECEC]'>
              My wallet
            </div>
          </Link>
        </MenuItem>
        {/* <div className='flex items-center justify-between py-3'>
          <span>Theme</span>
          <input
            type='checkbox'
            onChange={toggleTheme}
            className='theme-controller toggle col-span-2 col-start-1 row-start-1 border-sky-400 bg-amber-300 [--tglbg:theme(colors.sky.500)] checked:border-blue-800 checked:bg-blue-300 checked:[--tglbg:theme(colors.blue.900)]'
          />
        </div> */}
        <MenuItem>
          <button
            onClick={handleLogout}
            className='mt-2 w-full rounded-lg bg-[#d2d2d2] py-3 text-black transition-all hover:opacity-70'
          >
            Sign out
          </button>
        </MenuItem>
      </MenuItems>
    </Menu>
  )
}

export default DropDown
