'use client'
import { useSession } from '@/context/session-context'
import OmniStroke from '@/images/Omnistroke.png'
import { cn } from '@/lib/utils/cn'
import { useTagName } from '@/store/tag.store'
import Image from 'next/image'
import Link from 'next/link'
import { useSearchParams } from 'next/navigation'
import { useEffect, useState } from 'react'
import DropDown from './drop-down'
import SearchBar from './search-bar'

export const Header = () => {
  const session = useSession()

  let currentUrl = ''
  if (typeof window !== 'undefined') {
    currentUrl = window.location.href
  }
  const searchParams = useSearchParams()

  const linking = [
    {
      name: 'Explore',
      link: '/',
    },
    {
      name: 'Shop',
      link: '/shop',
    },
    // {
    //   name: 'Commissions',
    //   link: '/commissions',
    // },
  ]
  const [user, setUser] = useState<string | null>('')
  useEffect(() => {
    const userId = searchParams.get('userId')
    const role = searchParams.get('role')
    const userToken = searchParams.get('userToken')

    if (userId && role && userToken) {
      // Store in session
      ;(async () => {
        localStorage.setItem('userId', userId)
        localStorage.setItem('role', role)
        localStorage.setItem('userToken', userToken)

        // Optionally, redirect to clean the URL
        window.location.href = window.location.origin
      })()
    }

    setUser(localStorage.getItem('userId'))
  }, [searchParams])

  const searchValue = useTagName()

  return (
    <>
      {searchValue.trim() !== '' && <div className='fixed inset-0 z-10 h-screen w-screen bg-[rgba(0,0,0,0.4)]'></div>}
      <div
        className={cn(
          'relative z-50 box-border  flex w-full items-center justify-between gap-5 bg-base-100 px-6 py-[24px] opacity-[1] transition-all duration-300 ease-in-out lg:px-[40px]'
        )}
      >
        <div className='flex items-center'>
          <label className='btn btn-circle swap swap-rotate lg:hidden'>
            {/* this hidden checkbox controls the state */}
            <input type='checkbox' />

            {/* hamburger icon */}
            <svg
              className='swap-off fill-current'
              xmlns='http://www.w3.org/2000/svg'
              width='32'
              height='32'
              viewBox='0 0 512 512'
            >
              <path d='M64,384H448V341.33H64Zm0-106.67H448V234.67H64ZM64,128v42.67H448V128Z' />
            </svg>

            {/* close icon */}
            <svg
              className='swap-on fill-current'
              xmlns='http://www.w3.org/2000/svg'
              width='32'
              height='32'
              viewBox='0 0 512 512'
            >
              <polygon points='400 145.49 366.51 112 256 222.51 145.49 112 112 145.49 222.51 256 112 366.51 145.49 400 256 289.49 366.51 400 400 366.51 289.49 256 400 145.49' />
            </svg>
          </label>
          <Link href={'/'} className=' flex w-fit items-center gap-2'>
            <Image src={OmniStroke} alt='Omnistroke' className='hidden lg:flex' width={40} height={40} />
            <h1 className='ml-2 flex font-satisfy text-[24px] font-bold leading-[36px] lg:hidden'>OmniStroke</h1>
          </Link>
        </div>
        <div className='hidden h-[10px] items-center gap-8 lg:flex lg:grow'>
          {linking.map((link, index) => (
            <Link key={index} href={link.link}>
              <h2 className='text-[16px] font-[600] leading-[20px] transition-all hover:opacity-75'>{link.name}</h2>
            </Link>
          ))}
          {user && (
            <Link href='/orders'>
              <h2 className='text-[16px] font-[600] leading-[20px] transition-all hover:opacity-75'>Orders</h2>
            </Link>
          )}
        </div>
        <div className='relative flex items-center gap-10'>
          <SearchBar />
          {!session?.userToken ? (
            <Link href={`${process.env.NEXT_PUBLIC_BASE_URL}/login?current-url=${currentUrl}`}>
              <button className='h-12 rounded-full bg-base-content px-6 font-semibold text-base-100 transition-all hover:opacity-70'>
                <span className='whitespace-nowrap text-base'>Sign in</span>
              </button>
            </Link>
          ) : (
            <DropDown />
          )}
        </div>
      </div>
    </>
  )
}
