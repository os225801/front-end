import { useClickOutside } from '@/hooks/use-click-outside'
import { useDebouncedValue } from '@/hooks/use-debounce-value'
import { useInfiniteTags } from '@/hooks/use-tag-query'
import { useTagName, useTagStoreActions } from '@/store/tag.store'
import Link from 'next/link'
import { useRouter } from 'next/navigation'

const SearchBar = () => {
  const { setTagName, setTagNameShow, setId } = useTagStoreActions()
  const router = useRouter()
  const searchValue = useTagName()
  const [debounced] = useDebouncedValue(searchValue, 200)
  const { data } = useInfiniteTags({
    searchTerm: debounced,
  })
  const ref = useClickOutside(() => setTagName(''))

  return (
    <>
      <form
        onSubmit={(e) => {
          e.preventDefault()
          router.push(`/search?q=${searchValue}`)
          setTagNameShow(searchValue)
          setId('')
          setTagName('')
        }}
        ref={ref}
        className='relative hidden h-12 items-center lg:inline-flex'
      >
        <svg
          xmlns='http://www.w3.org/2000/svg'
          width='17'
          height='17'
          viewBox='0 0 17 17'
          fill='none'
          role='img'
          className='icon nav-v2-search__icon z-20'
        >
          <path
            d='M1.5 7.75C1.5 9.4076 2.15848 10.9973 3.33058 12.1694C4.50269 13.3415 6.0924 14 7.75 14C9.4076 14 10.9973 13.3415 12.1694 12.1694C13.3415 10.9973 14 9.4076 14 7.75C14 6.0924 13.3415 4.50269 12.1694 3.33058C10.9973 2.15848 9.4076 1.5 7.75 1.5C6.0924 1.5 4.50269 2.15848 3.33058 3.33058C2.15848 4.50269 1.5 6.0924 1.5 7.75V7.75Z'
            stroke='currentColor'
            strokeWidth='1.5'
            strokeLinecap='round'
            strokeLinejoin='round'
          ></path>
          <path
            d='M12.814 12.8132L15.5 15.4999'
            stroke='currentColor'
            strokeWidth='1.5'
            strokeLinecap='round'
            strokeLinejoin='round'
          ></path>
        </svg>
        <input
          type='search'
          value={searchValue}
          onChange={(e) => setTagName(e.target.value)}
          className='z-10 h-full w-[500px]  rounded-[99999px] bg-[#f4f5fb] pl-12 pr-6 focus:outline-none'
          placeholder='Search...'
        />
        {searchValue.length > 0 && (
          <div className='absolute top-[100%] z-10 mt-2 max-h-[500px] w-full overflow-hidden rounded-xl bg-base-100 shadow-lg'>
            {data?.pages?.map((page) => (
              <div key={page?.pageNumber}>
                {page?.items?.map((tag: Tag) => (
                  <Link key={tag.id} href={`/search?q=${tag.id}`}>
                    <div
                      onClick={() => {
                        setTagNameShow(tag.tagName)
                        setId('')
                        setTagName('')
                      }}
                      className='m-2 flex items-center justify-between rounded-lg px-2 py-4 text-base font-medium hover:bg-base-200'
                    >
                      <span>{tag.tagName}</span>
                      <svg viewBox='0 0 24 24' className='icon max-h-[16px] max-w-[16px] fill-current'>
                        <path
                          d='M3.41421 2H9C9.55228 2 10 1.55228 10 1C10 0.447715 9.55228 0 9 0H0V9.25C0 9.80229 0.447715 10.25 1 10.25C1.55229 10.25 2 9.80229 2 9.25V3.41421L9.29289 10.7071C9.68342 11.0976 10.3166 11.0976 10.7071 10.7071C11.0976 10.3166 11.0976 9.68342 10.7071 9.29289L3.41421 2Z'
                          transform='translate(6 6)'
                        ></path>
                      </svg>
                    </div>
                  </Link>
                ))}
              </div>
            ))}
          </div>
        )}
      </form>
      <Link href={'/search'} className='mr-6 flex lg:hidden'>
        <svg
          xmlns='http://www.w3.org/2000/svg'
          width='17'
          height='17'
          viewBox='0 0 17 17'
          fill='none'
          role='img'
          className='icon nav-v2-search__icon'
        >
          <path
            d='M1.5 7.75C1.5 9.4076 2.15848 10.9973 3.33058 12.1694C4.50269 13.3415 6.0924 14 7.75 14C9.4076 14 10.9973 13.3415 12.1694 12.1694C13.3415 10.9973 14 9.4076 14 7.75C14 6.0924 13.3415 4.50269 12.1694 3.33058C10.9973 2.15848 9.4076 1.5 7.75 1.5C6.0924 1.5 4.50269 2.15848 3.33058 3.33058C2.15848 4.50269 1.5 6.0924 1.5 7.75V7.75Z'
            stroke='currentColor'
            strokeWidth='1.5'
            strokeLinecap='round'
            strokeLinejoin='round'
          ></path>
          <path
            d='M12.814 12.8132L15.5 15.4999'
            stroke='currentColor'
            strokeWidth='1.5'
            strokeLinecap='round'
            strokeLinejoin='round'
          ></path>
        </svg>
      </Link>
    </>
  )
}

export default SearchBar
