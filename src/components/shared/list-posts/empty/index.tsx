import empty from '@/images/empty.svg'
import Image from 'next/image'

const Empty = () => {
  return (
    <div className='flex flex-grow flex-col items-center justify-center'>
      <Image src={empty} alt='No posts found' className='mb-10 scale-150' />
      <div className='mt-8 text-center text-4xl font-bold text-base-content'>No data found</div>
      <p className='mt-4 text-base'>There is no data to show you right now</p>
    </div>
  )
}

export default Empty
