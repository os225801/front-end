'use client'
import { useInfinitePost } from '@/hooks/use-post-query'
import { useTagId } from '@/store/tag.store'
import { useSearchParams } from 'next/navigation'
import { Fragment } from 'react'
import { Card } from '../card'
import CollectionModal from '../card/collection-modal'
import Loading from '../loading'
import Empty from './empty'
import Tags from './tags'

const ListPosts = () => {
  const tagId = useTagId()
  const searchParams = useSearchParams()
  const q = searchParams.get('q')

  const fetchParam = tagId || q

  const {
    data: posts,
    isError,
    isLoading,
    fetchNextPage,
    hasNextPage,
    isFetchingNextPage,
    status,
    isFetching,
  } = useInfinitePost({ tagIds: fetchParam })

  if (isLoading) return <Loading />
  if (isError) return <Empty />

  if (status === 'success')
    return (
      <>
        <Tags />
        {posts?.pages[0]?.items ? (
          <div className='shots-grid w-full p-4 md:p-8'>
            {posts?.pages?.map((group, i) => (
              <Fragment key={i}>
                {group?.items ? (
                  <>
                    {group?.items?.map((post: Post) => (
                      <Fragment key={post.id}>
                        <Card {...post} />
                      </Fragment>
                    ))}
                  </>
                ) : null}
              </Fragment>
            ))}
          </div>
        ) : (
          <Empty />
        )}
        {hasNextPage && !isFetchingNextPage && (
          <div className='flex justify-center'>
            <button
              className='mt-4 rounded-full bg-base-200 px-[20px] py-[10px] text-sm font-semibold text-base-content hover:opacity-60'
              onClick={() => fetchNextPage()}
              disabled={!hasNextPage || isFetchingNextPage}
            >
              {isFetching ? 'Loading...' : 'Load more work'}
            </button>
          </div>
        )}
        <CollectionModal />
      </>
    )
}

export default ListPosts
