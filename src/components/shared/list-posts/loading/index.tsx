const Loading = () => {
  return (
    <div className='w-full min-w-0'>
      <div className='shots-grid shots-grid-with-large-shots !gap-[48px]'>
        {Array.from({ length: 6 }).map((_, i) => (
          <div key={i} className='skeleton h-0 overflow-hidden pb-[75%]'>
            <figure className='relative before:block before:pt-[75%]'>
              <div className='h-auto w-full rounded-lg'></div>
            </figure>
          </div>
        ))}
      </div>
    </div>
  )
}

export default Loading
