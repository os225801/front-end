import { useTags } from '@/hooks/use-tag-query'
import { cn } from '@/lib/utils/cn'
import { useTagId, useTagStoreActions } from '@/store/tag.store'
import React from 'react'

interface TagProps {
  tag: Tag
}

const Tag: React.FC<TagProps> = ({ tag }) => {
  const { setId } = useTagStoreActions()
  const tagId = useTagId()

  return (
    <div onClick={() => setId(tag.id)} className='inline-block'>
      <div
        className={cn(
          'inline-flex min-h-9 cursor-pointer items-center rounded-lg bg-base-200 px-4 py-2 text-sm font-semibold text-base-content transition-all hover:opacity-60',
          tagId === tag.id && 'bg-base-content text-base-100'
        )}
      >
        {tag.tagName}
      </div>
    </div>
  )
}

const Tags = () => {
  const {
    data: tags,
    isLoading,
    isError,
  } = useTags({
    page: 1,
    size: 10,
    searchTerm: '',
  })

  if (isLoading) return <div>Loading...</div>
  if (isError) return null

  return (
    <div className='flex w-full flex-row-reverse items-center px-4 py-3  md:flex-row md:px-6 md:py-4 '>
      <div className='scrollbar-hide flex flex-grow  space-x-3 overflow-scroll scroll-smooth whitespace-nowrap px-[2px] md:justify-center md:space-x-4'>
        <Tag key='all' tag={{ id: '', tagName: 'All', tagDescription: '' }} />
        {tags?.items?.map((tag) => <Tag key={tag.id} tag={tag} />)}
      </div>
    </div>
  )
}

export default Tags
