const Loading = () => {
  return (
    <section className='flex h-full w-full flex-1 items-center justify-center'>
      <span className='loading loading-ring loading-lg'></span>
    </section>
  )
}

export default Loading
