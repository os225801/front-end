import { ThemeContext } from '@/context/theme-context'
import { Dialog, DialogPanel, DialogTitle } from '@headlessui/react'
import React, { useContext } from 'react'

const Modal = ({
  isOpen,
  closeModal,
  title,
  children,
}: {
  isOpen: boolean
  closeModal: () => void
  title: string
  children: JSX.Element | JSX.Element[] | string | string[] | React.ReactNode | React.ReactNode
}) => {
  const { theme } = useContext(ThemeContext)

  return (
    <Dialog data-theme={theme} open={isOpen} as='div' className='relative z-50 focus:outline-none' onClose={closeModal}>
      <div className='fixed inset-0 z-50 w-screen overflow-y-auto bg-black/25 '>
        <div className='flex min-h-full items-center justify-center p-4'>
          <DialogPanel
            transition
            className='data-[closed]:transform-[scale(95%)] w-full max-w-md rounded-xl bg-white p-6 duration-300 ease-out data-[closed]:opacity-0'
          >
            <DialogTitle as='h3' className='text-base/7 font-semibold '>
              {title}
            </DialogTitle>
            <div className='mt-4'>{children}</div>
          </DialogPanel>
        </div>
      </div>
    </Dialog>
  )
}

export default Modal
