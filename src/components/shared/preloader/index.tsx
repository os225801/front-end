const Preloader = () => {
  return (
    <div className='flex h-screen w-full items-center justify-center bg-white'>
      <span className='loading loading-ring loading-lg'></span>
    </div>
  )
}

export default Preloader
