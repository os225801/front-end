import toast from 'react-hot-toast'

export const useToast = () => {
  const showToast = (message: string, type: 'success' | 'error' | 'loading' | 'blank' = 'success') => {
    switch (type) {
      case 'success':
        toast.success(message)
        break
      case 'error':
        toast.error(message)
        break
      case 'loading':
        toast.loading(message)
        break
      case 'blank':
        toast(message)
        break
    }
  }

  return { showToast }
}
