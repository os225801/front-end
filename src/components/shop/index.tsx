import { postService } from '@/services/post.service'
import { dehydrate, HydrationBoundary, QueryClient } from '@tanstack/react-query'
import { Suspense } from 'react'
import ShopContent from './shop-content'
import { ShopSideBar } from './shop-sidebar'

export default async function ShopScreen() {
  const queryClient = new QueryClient()

  await queryClient.prefetchQuery({
    queryKey: ['posts'],
    queryFn: async () => {
      const posts = await postService.getAllPosts({
        page: 1,
        size: 20,
        keyword: '',
        sortBy: 'createdAt',
        sortOrder: 'DESC',
        isBuyable: true,
      })
      return posts
    },
  })

  return (
    <HydrationBoundary state={dehydrate(queryClient)}>
      <div className='flex h-full w-full flex-col'>
        <div className='flex h-full w-full'>
          <Suspense>
            <ShopSideBar />
          </Suspense>
          <Suspense>
            <ShopContent />
          </Suspense>
        </div>
      </div>
    </HydrationBoundary>
  )
}
