import ShopCardsContainer from './shop-cards-container'

export default function ShopContent() {
  return <ShopCardsContainer />
}
