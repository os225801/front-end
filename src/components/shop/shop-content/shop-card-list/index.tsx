'use client'

import Empty from '@/components/shared/list-posts/empty'
import Loading from '@/components/shared/loading'
import { useInfinitePost } from '@/hooks/use-post-query'
import { useSearchParams } from 'next/navigation'
import { Fragment, useEffect } from 'react'
import { useDebouncedCallback } from 'use-debounce'
import { ShopCard } from '../shop-card'

export const ShopCardList = () => {
  const searchParams = useSearchParams()
  const tagIds = searchParams.get('tagIds')
  const searchTerm = searchParams.get('searchTerm') ? searchParams.get('searchTerm') : ''
  const sortBy = searchParams.get('sortBy') ? searchParams.get('sortBy') : ''
  const sortOrder = searchParams.get('sortOrder') ? searchParams.get('sortOrder') : ''
  const buyablePosts: Post[] = []

  const {
    data: posts,
    isError,
    isLoading,
    fetchNextPage,
    hasNextPage,
    isFetchingNextPage,
    status,
    isFetching,
  } = useInfinitePost({ tagIds: tagIds, sortBy: sortBy!, sortOrder: sortOrder!, isBuyable: true })

  const handleScroll = useDebouncedCallback(() => {
    const { scrollTop, clientHeight, scrollHeight } = document.documentElement
    if (isFetchingNextPage || !hasNextPage || isFetching) {
      return
    } else if (scrollTop + clientHeight >= scrollHeight - 250) {
      fetchNextPage()
    }
  }, 1000)

  useEffect(() => {
    window.addEventListener('scroll', handleScroll)
    return () => {
      window.removeEventListener('scroll', handleScroll)
    }
  }, [isFetchingNextPage])

  if (isLoading) return <Loading />
  if (isError) return <Empty />
  return (
    <>
      {posts?.pages[0]?.items ? (
        <div className='shots-grid w-full'>
          {posts?.pages?.map((group, i) => (
            <Fragment key={i}>
              {group?.items ? (
                <>
                  {group?.items
                    ?.filter((post: Post) => post.title.includes(searchTerm!))
                    .map((post: Post) => (
                      <Fragment key={post.id}>
                        <ShopCard
                          avatar={post.user.avatar}
                          username={post.user.username}
                          imageSrc={post.artwork.image}
                          likes={post.totalLikes}
                          title={post.title}
                          price={post.price}
                          postId={post.id}
                          isLiked={post.isLiked}
                          views={post.views}
                        />
                      </Fragment>
                    ))}
                </>
              ) : null}
            </Fragment>
          ))}
        </div>
      ) : (
        <Empty />
      )}
      {isFetchingNextPage && hasNextPage && (
        <p className='self-center text-lg font-semibold text-base-content'>Loading...</p>
      )}
    </>
  )
}
