import CardDetail from './card-detail'
import CardThumbnail from './card-thumbnail'

export const ShopCard = (props: {
  avatar: string
  username: string
  likes: number
  imageSrc: string
  title: string
  price: number
  postId: string
  isLiked: boolean
  views: number
}) => {
  return (
    <article className='flex flex-col'>
      <CardThumbnail imageSrc={props.imageSrc} title={props.title} price={props.price} postId={props.postId} isLiked={props.isLiked}/>
      <CardDetail avatar={props.avatar} username={props.username} likes={props.likes} views={props.views}/>
    </article>
  )
}
