'use client'

import { useInfinitePost } from '@/hooks/use-post-query'
import { ShopCardList } from '../shop-card-list'

export default function ShopCardsContainer() {
  const {data: posts} = useInfinitePost({isBuyable: true})
  return (
    <div className='font-sarala flex h-full w-full flex-col gap-[24px] p-[32px]'>
      <p className='text-[16px] font-[700]'>{`${posts?.pages[0].totalCount} Artworks`}</p>
      {/* {loading && <ShopSkeletonCardList/>} */}
      <ShopCardList />
    </div>
  )
}
