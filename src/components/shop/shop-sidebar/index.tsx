import { Suspense } from 'react'
import { BiCategory } from 'react-icons/bi'
import { GrPrevious } from 'react-icons/gr'
import { MdSort } from 'react-icons/md'
import { TbArrowsSort } from 'react-icons/tb'
import { ShopSearchInput } from './search-input'
import { ShopSortSelect } from './sort-input'
import { ShopSortOrderSelect } from './sort-order-input'
import { ShopTagInput } from './tag-input'

export const ShopSideBar = () => {
  return (
    <div className='flex h-screen w-[330px] flex-col items-center px-[24px] py-[18px]'>
      <Suspense>
        <ShopSearchInput />
      </Suspense>
      <hr className='mb-5 mt-5 h-[2px] w-[90%] border-none' />
      <div className='flex w-full flex-col items-start'>
        <div className='mb-3 flex w-full items-center justify-between'>
          <div className='flex items-center gap-1'>
            <BiCategory className='h-[16px] w-[16px]' />
            <p className='font-bold'>Tags</p>
          </div>
          <GrPrevious className='h-[10px] w-[10px] rotate-[90deg]' />
        </div>
        <div className='w-[295px]'>
          <Suspense>
            <ShopTagInput />
          </Suspense>
        </div>
      </div>
      <hr className='mb-5 mt-5 h-[2px] w-[90%] border-none' />
      <div className='flex w-full flex-col items-start'>
        <div className='mb-3 flex w-full items-center justify-between'>
          <div className='flex items-center gap-1'>
            <MdSort className='h-[16px] w-[16px]' />
            <p className='font-bold'>Sort</p>
          </div>
          <GrPrevious className='h-[10px] w-[10px] rotate-[90deg]' />
        </div>
        <div className='w-[295px]'>
          <ShopSortSelect />
        </div>
      </div>
      <hr className='mb-5 mt-5 h-[2px] w-[90%] border-none' />
      <div className='flex w-full flex-col items-start'>
        <div className='mb-3 flex w-full items-center justify-between'>
          <div className='flex items-center gap-1'>
            <TbArrowsSort className='h-[16px] w-[16px]' />
            <p className='font-bold'>Sort Order</p>
          </div>
          <GrPrevious className='h-[10px] w-[10px] rotate-[90deg]' />
        </div>
        <div className='w-[295px]'>
          <ShopSortOrderSelect />
        </div>
      </div>
    </div>
  )
}
