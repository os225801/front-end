'use client'

import { useQueryClient } from '@tanstack/react-query'
import { usePathname, useRouter, useSearchParams } from 'next/navigation'
import { CiSearch } from 'react-icons/ci'
import { useDebouncedCallback } from 'use-debounce'
// import { useQueryClient } from 'react-query';
// import { useDebouncedCallback } from 'use-debounce';

export const ShopSearchInput = () => {
  const pathname = usePathname()
  const searchParams = useSearchParams()
  const { replace } = useRouter()
  const queryClient = useQueryClient()

  const handleSearch = useDebouncedCallback((term: string) => {
    const params = new URLSearchParams(searchParams.toString())
    if (term) {
      params.set('searchTerm', term)
    } else {
      params.delete('searchTerm')
    }
    replace(`${pathname}?${params.toString()}`)
  }, 500)

  return (
    <div className='flex items-center justify-center'>
      <CiSearch className='absolute left-7 h-[20px] w-[20px] justify-self-start' />
      <input
        className='h-[36px] w-[295px] rounded-full bg-[#F9F9F9] pl-[35px] pr-[20px] outline outline-2 outline-offset-0 outline-[#E8E8E8] placeholder:text-gray-500 focus:outline-gray-300'
        placeholder='Search art'
        onChange={(e) => {
          handleSearch(e.target.value)
        }}
        defaultValue={searchParams.get('searchTerm')?.toString()}
      />
    </div>
  )
}
