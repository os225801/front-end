'use client'

import { useQueryClient } from '@tanstack/react-query'
import { usePathname, useRouter, useSearchParams } from 'next/navigation'
import { useDebouncedCallback } from 'use-debounce'

export const ShopSortSelect = () => {
  const queryClient = useQueryClient()
  const searchParams = useSearchParams()
  const pathname = usePathname()
  const { replace } = useRouter()

  const handleSelect = useDebouncedCallback((e: string) => {
    const params = new URLSearchParams(searchParams.toString())
    if (e) {
      params.set('sortBy', e)
    } else {
      params.delete('sortBy')
    }
    queryClient.invalidateQueries({ queryKey: ['posts'] })
    replace(`${pathname}?${params.toString()}`)
  })

  return (
    <div>
      <input
        type='radio'
        name='sort'
        value='createdAt'
        id='recent'
        onChange={(e) => handleSelect(e.target.value)}
        className='bg-gray-300'
      />
      <label htmlFor='recent' className='font-sarala ml-2'>
        Recent
      </label>
      <br />
      <input
        type='radio'
        name='sort'
        value='price'
        id='price'
        onChange={(e) => handleSelect(e.target.value)}
        className='bg-gray-300'
      />
      <label htmlFor='price' className='font-sarala ml-2'>
        Price
      </label>
      <br />
      <input
        type='radio'
        name='sort'
        value='totalLikes'
        id='popular'
        onChange={(e) => handleSelect(e.target.value)}
        className='bg-gray-300'
      />
      <label htmlFor='popular' className='font-sarala ml-2'>
        Popular
      </label>
      <br />
    </div>
  )
}
