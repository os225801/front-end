'use client'

import { useQueryClient } from '@tanstack/react-query'
import { usePathname, useRouter, useSearchParams } from 'next/navigation'
import { useDebouncedCallback } from 'use-debounce'

export const ShopSortOrderSelect = () => {
  const queryClient = useQueryClient()
  const searchParams = useSearchParams()
  const pathname = usePathname()
  const { replace } = useRouter()

  const handleSelect = useDebouncedCallback((e: string) => {
    const params = new URLSearchParams(searchParams.toString())
    if (e) {
      params.set('sortOrder', e.toUpperCase())
    } else {
      params.delete('sortOrder')
    }

    queryClient.invalidateQueries({ queryKey: ['posts'] })
    replace(`${pathname}?${params.toString()}`)
  })

  return (
    <div>
      <input
        type='radio'
        name='sort-order'
        value='ASC'
        id='ascending'
        onChange={(e) => handleSelect(e.target.value)}
        className='bg-gray-300'
      />
      <label htmlFor='ascending' className='font-sarala ml-2'>
        Ascending
      </label>
      <br />
      <input
        type='radio'
        name='sort-order'
        value='DESC'
        id='descending'
        onChange={(e) => handleSelect(e.target.value)}
        className='bg-gray-300'
      />
      <label htmlFor='descending' className='font-sarala ml-2'>
        Descending
      </label>
    </div>
  )
}
