'use client'

import { useTags } from '@/hooks/use-tag-query'
import { cn } from '@/lib/utils/cn'
import { useQueryClient } from '@tanstack/react-query'
import { usePathname, useRouter, useSearchParams } from 'next/navigation'
import { ChangeEvent, useEffect, useState } from 'react'
import { IoMdClose } from 'react-icons/io'

export const ShopTagInput = () => {
  const pathname = usePathname()
  const searchParams = useSearchParams()
  const { replace } = useRouter()
  const queryClient = useQueryClient()
  const [show, setShow] = useState(false)
  const [tags, setTags] = useState<Tag[]>([])
  const [searchTerm, setSearchTerm] = useState('')

  useEffect(() => {
    const purifyParams = new URLSearchParams(searchParams)
    purifyParams.delete('tagIds')
    replace(`${pathname}?${purifyParams.toString()}`)
  }, [])

  const { data } = useTags({ page: 1, size: 10, searchTerm: searchTerm })

  const handleSearch = (e: ChangeEvent<HTMLInputElement>) => {
    setSearchTerm(e.target.value)
  }

  const handleSelectTag = (tag: Tag) => {
    const params = new URLSearchParams(searchParams.toString())
    if (!tags.includes(tag)) {
      setTags([...tags, tag])
    }
    const tagParam = searchParams.get('tagIds')
    if (tagParam && !tagParam.includes(tag.id)) {
      params.set('tagIds', tagParam.concat(` ${tag.id}`))
    } else if (!tagParam) {
      params.set('tagIds', tag.id)
    }

    replace(`${pathname}?${params.toString()}`)
  }

  const hide = () => {
    setTimeout(() => {
      setShow(false)
    }, 150)
  }

  const handleEliminate = (tag: Tag) => {
    const params = new URLSearchParams(searchParams.toString())
    const newTag = tags.filter((i) => i.id !== tag.id)
    setTags([...newTag])
    const tagParam = params.get('tagIds')
    params.set('tagIds', tagParam!.replace(tag.id, '').trim())
    if (params.get('tagIds') === '') params.delete('tagIds')

    queryClient.invalidateQueries({ queryKey: ['posts'] })
    replace(`${pathname}?${params.toString()}`)
  }

  return (
    <div>
      <div className='relative h-full'>
        <div className={cn({ 'hidden w-full flex-wrap items-center  gap-1': true }, { 'mt-[8px] flex': tags.length })}>
          {tags.map((tag) => {
            return (
              <div
                key={tag.id}
                className='flex h-[24px] items-center justify-center gap-[5px] rounded-[12px] border border-[#E8E8E8] px-4'
              >
                <p className='font-sarala text-[13px] font-[400] text-[#191919]'>{tag.tagName}</p>
                <button className=''>
                  <IoMdClose className='w-[13px] ' onClick={() => handleEliminate(tag)} />
                </button>
              </div>
            )
          })}
        </div>
        <input
          type='search'
          placeholder='Search Tag'
          className='font-sarala mt-[8px] h-[40px] w-full rounded-[8px] border-none bg-[#FEFEFE] px-[10px] text-[12px] outline outline-2 outline-offset-0 outline-[#E8E8E8] focus:outline-gray-300'
          autoComplete={'off'}
          onFocus={() => setShow(true)}
          onChange={(e) => handleSearch(e)}
          onBlur={hide}
        />
        <div
          className={cn(
            {
              'absolute z-10 mt-[2px] hidden h-min w-full rounded-[8px] border border-[#0000001A] bg-white py-2': true,
            },
            { block: show && data && data.items && data.items.length }
          )}
        >
          {data && data.items && data.items.map((tag: Tag) => {
            return (
              <div
                className='z-1 px-4 py-[2px] hover:cursor-pointer hover:bg-gray-300'
                key={tag.id}
                onClick={() => handleSelectTag(tag)}
              >
                <p className='font-sarala w-full text-[12px]'>{tag.tagName}</p>
              </div>
            )
          })}
        </div>
      </div>
    </div>
  )
}
