'use client'

import FormInput from '@/components/create-post/create-post-form'
import { UpdatePostSchema } from '@/schema/update-post.schema'
import { postService } from '@/services/post.service'
import { useIsDisableStoreActions } from '@/store/disable.store'
import { zodResolver } from '@hookform/resolvers/zod'
import { QueryClient } from '@tanstack/react-query'
import { useRouter } from 'next/navigation'
import { FormProvider, useForm } from 'react-hook-form'
import { useToast } from '../../shared/toast'
const UpdatePostScreen = ({ post }: { post: PostRes }) => {
  // const { data } = usePostQueryById(postId)
  const { setIsDisable } = useIsDisableStoreActions()
  const { showToast } = useToast()
  const router = useRouter()
  const queryClient = new QueryClient()

  const param: UpdatePostForm = {
    title: post.title || '',
    description: post.description || '',
    isBuyable: post.artwork.isBuyable || false,
    price: post.price || 0,
    tags: post.tags || [],
  }

  const methods = useForm<UpdatePostForm>({ defaultValues: param, resolver: zodResolver(UpdatePostSchema) })
  const {
    handleSubmit,
    formState: { errors },
    setValue,
    getValues,
  } = methods

  const submitForm = async (form: UpdatePostForm) => {
    setIsDisable(true)
    const temptagsid: string[] = []

    form.tags.forEach((x: TagInput) => {
      temptagsid.push(x.id)
    })

    const payload: UpdatePostPayload = {
      title: form.title,
      description: form.description,
      isBuyable: form.isBuyable,
      price: form.price,
      tagIds: temptagsid,
    }

    const updatedPost = await postService.updatePost(post.id, payload)

    if ((updatedPost as any).resultCode !== '00') {
      //hanlde error

      showToast('Can not update post !', 'error')
      setIsDisable(false)

      return
    }
    showToast('Update post successful ! Redirecting to post', 'success')
    setIsDisable(false)
    queryClient.invalidateQueries({ queryKey: ['post', post.id] })
    router.push(`/posts/${post.id}`)
  }

  return (
    <div className='relative w-full'>
      <FormProvider {...methods}>
        <form className='mt-10 flex w-full flex-wrap items-start justify-evenly' onSubmit={handleSubmit(submitForm)}>
          <div className='relative flex max-w-[700px] flex-col items-center justify-center sm:w-full'>
            <div className='flex h-[450px] w-full min-w-[375px] max-w-[600px] flex-col items-center justify-center rounded-[32px]'>
              <div className='relative h-min max-h-[450px] border-[1px] border-dashed border-[#0000001A]'>
                <img className='h-min max-h-[450px]' src={post?.artwork?.image as string} alt='Upload preview' />
              </div>
            </div>
          </div>
          <FormInput />
        </form>
      </FormProvider>
    </div>
  )
}

export default UpdatePostScreen
