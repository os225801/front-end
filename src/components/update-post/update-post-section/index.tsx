'use client'
import { usePostQueryById } from '@/hooks/use-post-query'
import UpdatePostScreen from '../update-post-form'

const UpdatePostSection = ({ id }: { id: string }) => {
  const { data, isError, isLoading } = usePostQueryById(id)

  if (isError)
    return (
      <div className='flex h-screen w-full flex-col items-center justify-center'>
        <h1 className='font-satisfy text-9xl font-bold'>404</h1>
        <h2 className='text-4xl font-medium'>Whoops, that page is gone.</h2>
      </div>
    )

  return (
    <>
      {data && !isLoading && (
        <main className='flex min-h-screen flex-col items-center justify-center p-4 md:p-8'>
          <UpdatePostScreen post={data} />
        </main>
      )}
    </>
  )
}

export default UpdatePostSection
