'use client'

import WithdrawButton from '@/components/wallet/withdraw-button'
import WithdrawModel from '@/components/wallet/withdraw-model'
import { walletService } from '@/services/wallet.service'
import { useQuery } from '@tanstack/react-query'
const Balance = () => {
  const { data } = useQuery({ queryKey: ['wallets'], queryFn: walletService.getWallet })

  return (
    <div className='mt-3 flex flex-col items-start'>
      <p className='text-[45px] font-bold'>Balance</p>
      <p className='max-w-[800px] text-[16px]'>
        OmniStroke Balance allows you to unlock premium brushes, purchase additional drawing prompts, reward fellow
        artists, or simply send a heartfelt gift to friends.
      </p>
      <br />
      <p className='mt-5'>Your Current Balance</p>
      <p className='text-[30px] font-[500]'>{data ? Number(data.balance).toLocaleString() : 0} VND</p>

      <div>
        <WithdrawButton />
        <WithdrawModel />
      </div>
    </div>
  )
}

export default Balance
