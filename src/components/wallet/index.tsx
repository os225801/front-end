import Balance from './balance'
import TransactionHistory from './transaction-history'

const WalletScreen = () => {
  return (
    <>
      <div className='flex w-full flex-col flex-wrap items-start justify-between gap-5'>
        <Balance />
        <TransactionHistory />
      </div>
    </>
  )
}

export default WalletScreen
