'use client'

import Empty from '@/components/shared/list-posts/empty'
import { transactionService } from '@/services/transactions.service'
import { useQuery } from '@tanstack/react-query'
import { useState } from 'react'

enum TransactionType {
  BUY = 1,
  RECEIVE = 2,
  WITHDRAW = 3,
}

enum TransactionStatus {
  PENDING = 1,
  SUCCESS = 2,
  FAILURE = 3,
}

const GenerateAmount = ({ amount, type }: { amount: number; type: number }) => {
  switch (type) {
    case TransactionType.WITHDRAW: {
      return <p className='text-[20px] font-semibold text-red-500'> - {Number(amount).toLocaleString()} VND</p>
    }
    case TransactionType.BUY: {
      return <p className='text-[20px] font-semibold text-red-500'> - {Number(amount).toLocaleString()} VND</p>
    }
    case TransactionType.RECEIVE: {
      return <p className='text-[20px] font-semibold text-green-500'> + {Number(amount).toLocaleString()} VND</p>
    }
    default:
      return <></>
  }
}

const GenerateStatus = ({ status }: { status: number }) => {
  switch (status) {
    case TransactionStatus.PENDING: {
      return (
        <div className='flex h-[30px] w-[120px] items-center justify-center	rounded-full	bg-[#F97316]'>
          <p className='font-semibold'>Processing</p>
        </div>
      )
    }
    case TransactionStatus.FAILURE: {
      return (
        <div className='flex h-[30px] w-[120px] items-center justify-center	rounded-full	bg-[#EF4444]'>
          <p className='font-semibold'>Fail</p>
        </div>
      )
    }
    case TransactionStatus.SUCCESS: {
      return (
        <div className='flex h-[30px] w-[120px] items-center justify-center rounded-full	bg-[#22c55e]'>
          <p className='font-semibold'>Success</p>
        </div>
      )
    }
    default:
      return <></>
  }
}

const TransactionRowDetail = (props: TransactionsHistoryRes) => {
  const generateHeader = (status: number) => {
    switch (status) {
      case TransactionType.BUY: {
        return 'Buy an artwork'
      }
      case TransactionType.RECEIVE: {
        return 'Selling an artwork'
      }
      case TransactionType.WITHDRAW: {
        return 'Withdraw money'
      }
      default:
        return ''
    }
  }

  return (
    <div className='flex w-full items-center justify-between border-b-2 pb-5'>
      <div>
        <p className='font-bold'>{generateHeader(props.type)}</p>
        <p className='text-[13px]'>
          {`Transaction Date: ${new Date(Date.parse(props.transactionDate)).toLocaleString('en-US', {
            hour: 'numeric',
            minute: '2-digit',
            hour12: true,
            weekday: 'short',
            day: 'numeric',
            month: 'short',
            year: 'numeric',
            timeZone: 'Asia/Bangkok',
          })}`}
        </p>
      </div>
      <div>
        <GenerateStatus status={props.status} />
      </div>
      <div className='flex w-full max-w-[300px] justify-end '>
        <GenerateAmount amount={props.amount} type={props.type} />
      </div>
    </div>
  )
}

const TransactionHistory = () => {
  const [value, setValue] = useState<number>(4)

  const param: TransactionsHistoryParam = {
    page: 1,
    size: 10,
    soryBy: 'transactionDate',
    sortOrder: 'DESC',
    keyword: 'type',
    value: value,
  }

  const tabs = [
    { name: 'ALL', value: 4 },

    {
      name: 'BUYING',
      value: 1,
    },
    {
      name: 'SELLING',
      value: 2,
    },
    {
      name: 'WITHDRAW',
      value: 3,
    },
  ]

  const buildParam = (param: TransactionsHistoryParam) => {
    if (param.value === 4) {
      return {
        keyword: param.keyword,
        page: param.page,
        size: param.size,
        sortOrder: param.sortOrder,
        soryBy: param.soryBy,
        // value: '',
      } as TransactionsHistoryParam
    } else {
      return param
    }
  }

  const { data } = useQuery({
    queryKey: ['transaction', value],
    queryFn: () => transactionService.getTransactionHistory(buildParam(param)),
  })
  return (
    <>
      <p className='mt-5 text-[35px] font-semibold'>Transaction History</p>
      <div role='tablist' className='tabs tabs-bordered tabs-lg w-full px-5' key={'tabmoney'}>
        {tabs.map((x, index) => {
          return (
            <>
              <input
                key={`input${x.name}${x.value}`}
                type='radio'
                name={x.name}
                role='tab'
                className='tab'
                aria-label={x.name}
                checked={value === x.value}
                onChange={() => setValue(x.value)}
              />
              <div
                key={`div1${x.name}${x.value}`}
                role='tabpanel'
                className='tab-content flex gap-3 rounded-b-box border-base-300 bg-base-100 p-6'
              >
                <div key={`div2${x.name}${x.value}`} className='flex w-full flex-col gap-5'>
                  {data?.items ? (
                    data?.items.map((x: TransactionsHistoryRes) => {
                      return <TransactionRowDetail key={x.id} {...x} />
                    })
                  ) : (
                    <div className='mt-10'>
                      <Empty key={`empty${x.name}${x.value}`} />
                    </div>
                  )}
                </div>
              </div>
            </>
          )
        })}
      </div>
    </>
  )
}

export default TransactionHistory
