'use client'

import { useWithdrawStoreActions } from '@/store/withdraw.store'

const WithdrawButton = () => {
  const { setIsOpenWithDrawModel } = useWithdrawStoreActions()

  const open = () => {
    setIsOpenWithDrawModel(true)
  }

  return (
    <button className='btn btn-outline w-[400px]' onClick={open}>
      <div className=''>withdraw</div>
    </button>
  )
}

export default WithdrawButton
