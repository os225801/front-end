'use client'

import FormFieldInput from '@/components/shared/form-input'
import Modal from '@/components/shared/modal'
import { useToast } from '@/components/shared/toast'
import { useCurrentUser } from '@/hooks/use-profile-query'
import { WithdrawSchema } from '@/schema/withdraw.schema'
import { transactionService } from '@/services/transactions.service'
import { useWithdrawIsOpenWithDrawModel, useWithdrawStoreActions } from '@/store/withdraw.store'
import { zodResolver } from '@hookform/resolvers/zod'
import Link from 'next/link'
import { useState } from 'react'
import { useForm } from 'react-hook-form'

const WithdrawModel = () => {
  const isOpen = useWithdrawIsOpenWithDrawModel()

  const [open, setOpen] = useState<boolean>(false)

  const { setIsOpenWithDrawModel } = useWithdrawStoreActions()
  const { showToast } = useToast()
  const param: WithdrawForm = {
    amount: 0,
  }
  const { data: user } = useCurrentUser()
  const {
    formState: { errors },
    register,
    handleSubmit,
    getValues,
  } = useForm<WithdrawForm>({ defaultValues: param, resolver: zodResolver(WithdrawSchema) })

  const close = () => {
    setIsOpenWithDrawModel(false)
    setOpen(false)
  }

  const handleWithdraw = async () => {
    if (!user || user.accountNumber === '' || user.accountName === '') {
      setOpen(true)
      setIsOpenWithDrawModel(false)
      return
    }

    const data = await transactionService.withdraw(getValues('amount'))
    if (!data) {
      showToast('Withdraw money unsuccessful !', 'error')
    }
    showToast('Withdraw money successful !', 'success')
    setIsOpenWithDrawModel(false)
  }

  return (
    <>
      <Modal title={'How much do you want to withdraw ?'} closeModal={close} isOpen={isOpen}>
        <form onSubmit={handleSubmit(handleWithdraw)}>
          <FormFieldInput
            error={errors.amount?.message}
            lable='Amount'
            name={'amount'}
            placeholder='Enter number of money'
            register={register}
            type='number'
            valueAsNumber
          />
          <div className='box-border flex w-full flex-col gap-2'>
            <button className='btn btn-outline w-full' type='submit'>
              <div className=''>withdraw</div>
            </button>
            <button onClick={close} className='btn btn-active w-full'>
              Cancel
            </button>
          </div>
        </form>
      </Modal>

      <Modal title={''} closeModal={close} isOpen={open}>
        <p className='text-center text-[22px] font-bold'>
          Your account is not connected to the bank. Please add your bank account to your profile!
        </p>
        <div className='mt-7 flex justify-between'>
          <button className='btn' onClick={close}>
            <div className='w-[150px]'>Cancel</div>
          </button>
          <Link href={`/edit-profile/${user?.id}`} className='btn btn-neutral'>
            <div className='w-[150px]'>Add Now !</div>
          </Link>
        </div>
      </Modal>
    </>
  )
}

export default WithdrawModel
