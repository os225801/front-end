'use client'

import { useContext, useEffect, useState } from 'react'

import Preloader from '@/components/shared/preloader'
import { ThemeContext } from './theme-context'

export default function ClientThemeWrapper({ children }: { children: React.ReactNode }) {
  const { theme } = useContext(ThemeContext)
  const [isMounted, setIsMounted] = useState(false)

  useEffect(() => {
    setTimeout(() => {
      setIsMounted(true)
    }, 1000)
  }, [])

  if (!isMounted) return <Preloader />
  return <div data-theme={theme}>{children}</div>
}
