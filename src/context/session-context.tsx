'use client'
// context/SessionContext.js
import { createContext, useContext, useEffect, useState } from 'react'

const SessionContext = createContext<{
  userId: string | null
  role: string | null
  userToken: string | null
} | null>(null)

export const SessionProvider = ({ children }: { children: React.ReactNode }) => {
  const [session, setSession] = useState<{
    userId: string | null
    role: string | null
    userToken: string | null
  }>({
    userId: null,
    role: null,
    userToken: null,
  })

  useEffect(() => {
    const userId = localStorage.getItem('userId')
    const role = localStorage.getItem('role')
    const userToken = localStorage.getItem('userToken')

    if (userId && role && userToken) {
      setSession({ userId, role, userToken })
    }
  }, [])

  return <SessionContext.Provider value={session}>{children}</SessionContext.Provider>
}

export const useSession = () => {
  return useContext(SessionContext)
}
