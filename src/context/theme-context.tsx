/* eslint-disable @typescript-eslint/no-unused-vars */
'use client'

import Preloader from '@/components/shared/preloader'
import React, { createContext, useEffect, useState } from 'react'

export const ThemeContext = createContext({
  theme: 'light',
  // eslint-disable-next-line no-unused-vars
  changeTheme: (theme: string) => {},
})

export const ThemeProvider = ({ children }: { children: React.ReactNode }) => {
  const [theme, setTheme] = useState('light')
  const [isMounted, setIsMounted] = useState(false)

  useEffect(() => {
    setTimeout(() => {
      setIsMounted(true)
    }, 1000)
    const storedTheme = localStorage.getItem('theme') || 'light'
    setTheme(storedTheme)
    localStorage.setItem('theme', storedTheme || 'light')
  }, [])

  if (!isMounted) return <Preloader />

  const changeTheme = (theme: string) => {
    setTheme(theme)
    localStorage.setItem('theme', theme)
  }

  return <ThemeContext.Provider value={{ theme, changeTheme }}>{children}</ThemeContext.Provider>
}
