import { useToast } from '@/components/shared/toast'
import { collectionService } from '@/services/collection.service'
import { useCollectionIsOpenCollectionModal, usePostIdToBeAdded } from '@/store/collection.store'
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query'
import { useRouter } from 'next/navigation'

const fetchCollectionsByUserId = async (userId: string) => {
  const respone = await collectionService.getCollectionsByUserId({ userId })
  return respone
}

export const useCollectionsByUserId = ({ userId }: { userId: string }) => {
  const { data, isLoading, isError } = useQuery({
    queryFn: () => fetchCollectionsByUserId(userId),
    queryKey: ['collections', userId],
  })

  return { data, isLoading, isError } as {
    data: CollectionsPaginated
    isLoading: boolean
    isError: boolean
  }
}

const fetchCollections = async (postId: string) => {
  const response = await collectionService.getCollections({
    postId,
  })
  return response
}

export const useCollections = () => {
  const isOpen = useCollectionIsOpenCollectionModal()
  const postIdToBeAdded = usePostIdToBeAdded()
  const { data, isLoading, isError } = useQuery({
    queryFn: () => fetchCollections(postIdToBeAdded),
    queryKey: ['collections'],
    enabled: isOpen,
  })

  return { data, isLoading, isError } as {
    data: Collections
    isLoading: boolean
    isError: boolean
  }
}

const addOrRemovePost = async (params: { collectionId: string; postId: string }) => {
  const response = await collectionService.addOrRemovePostFromCollection(params)
  return response
}

export const useAddOrRemovePostFromCollection = () => {
  const queryClient = useQueryClient()
  const { mutateAsync: addOrRemovePostFromCollection } = useMutation({
    mutationFn: addOrRemovePost,
    onSuccess: () => {
      queryClient.invalidateQueries({
        queryKey: ['collections'],
      })
    },
  })

  return { addOrRemovePostFromCollection }
}

const getCollectionDetail = async (collectionId: string) => {
  const response = await collectionService.getCollectionDetail({ collectionId })
  return response
}

export const useCollectionDetail = ({ collectionId }: { collectionId: string }) => {
  const { data, isLoading, isError } = useQuery({
    queryFn: () => getCollectionDetail(collectionId),
    queryKey: ['collection', collectionId],
  })

  return { data, isLoading, isError } as {
    data: CollectionDetail
    isLoading: boolean
    isError: boolean
  }
}

export const deleteCollection = async (collectionId: string) => {
  const response = await collectionService.deleteCollection({ collectionId })
  return response
}

export const useDeleteCollection = () => {
  const router = useRouter()
  const { showToast } = useToast()
  const queryClient = useQueryClient()
  const { mutateAsync: deleteCurrentCollection } = useMutation({
    mutationFn: deleteCollection,
    onSuccess: () => {
      queryClient.invalidateQueries({
        queryKey: ['collections'],
      })
      router.back()
      showToast('Collection deleted successfully', 'success')
    },
  })

  return { deleteCurrentCollection }
}
