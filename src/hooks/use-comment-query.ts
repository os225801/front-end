import { commentService } from '@/services/comment.service'
import { useInfiniteQuery, useMutation, useQueryClient } from '@tanstack/react-query'

const fetchComments = async ({ pageParam, postId }: { pageParam: number; postId: string }) => {
  const comments = await commentService.getComments({
    postId: postId,
    pageNumber: pageParam,
    pageSize: 10,
  })
  return comments
}

export const useInfiniteComments = (params: { postId: string }) => {
  const { data, isError, isLoading, fetchNextPage, hasNextPage, isFetching, isFetchingNextPage, status } =
    useInfiniteQuery({
      queryKey: ['comments', params.postId],
      queryFn: ({ pageParam = 1 }) => fetchComments({ pageParam, postId: params.postId }),
      initialPageParam: 1,
      getNextPageParam: (lastPage) => {
        return lastPage?.hasNextPage ? lastPage?.pageNumber + 1 : undefined
      },
      enabled: Boolean(params.postId),
    })

  return { data, isError, isLoading, fetchNextPage, hasNextPage, isFetching, isFetchingNextPage, status }
}

const fetchSubComments = async ({ pageParam, parentId }: { pageParam: number; parentId: string }) => {
  const comments = await commentService.getSubComments({
    parentId: parentId,
    pageNumber: pageParam,
    pageSize: 5,
  })
  return comments
}

export const useInfiniteSubComments = (params: { parentId: string }) => {
  const { data, isError, isLoading, fetchNextPage, hasNextPage, isFetching, isFetchingNextPage, status } =
    useInfiniteQuery({
      queryKey: ['sub-comments', params.parentId],
      queryFn: ({ pageParam = 1 }) => fetchSubComments({ pageParam, parentId: params.parentId }),
      initialPageParam: 1,
      getNextPageParam: (lastPage) => {
        return lastPage?.hasNextPage ? lastPage?.pageNumber + 1 : undefined
      },
    })

  return { data, isError, isLoading, fetchNextPage, hasNextPage, isFetching, isFetchingNextPage, status }
}

const deleteComment = async (params: { commentId: string }) => {
  const res = await commentService.deleteComment(params)
  return res
}

export const useDeleteComment = (parentId: string) => {
  const queryClient = useQueryClient()
  const { mutateAsync, isPending, isSuccess } = useMutation({
    mutationFn: deleteComment,
    onSuccess: () => {
      queryClient.removeQueries({
        queryKey: ['sub-comments', parentId],
      })
      queryClient.invalidateQueries({
        queryKey: ['comments'],
      })
      queryClient.invalidateQueries({
        queryKey: ['sub-comments'],
      })
    },
  })

  return { mutateAsync, isPending, isSuccess }
}

const createComment = async (params: { postId: string; content: string; parentCommentId?: string }) => {
  const res = await commentService.createComment(params)
  return res
}

export const useCreateComment = () => {
  const queryClient = useQueryClient()
  const { mutateAsync, isPending, isSuccess } = useMutation({
    mutationFn: createComment,
    onSuccess: () => {
      queryClient.invalidateQueries({
        queryKey: ['comments'],
      })
      queryClient.invalidateQueries({
        queryKey: ['sub-comments'],
      })
    },
  })

  return { mutateAsync, isPending, isSuccess }
}
