import { orderService } from '@/services/order.service'
import { useInfiniteQuery, useQuery } from '@tanstack/react-query'

export const useOrder = (orderId: string) => {
  const { data, isError, isSuccess } = useQuery({
    queryKey: ['order', orderId],
    queryFn: async () => {
      const order = await orderService.getOrderById(orderId)

      return order
    },
    
  })

  return { data } as { data: Order }
}


const fetchOrderHistory = async (params: {pageNumber: number, status: number}) => {
  const orders = await orderService.getOrderHistory({
    pageNumber: params.pageNumber,
    pageSize: 10,
    status: params.status
  })
  return orders
}

export const useInfiniteOrderHistory = (userId: string, orderStatus: number) => {
  const { data, isError, isLoading, fetchNextPage, hasNextPage, isFetching, isFetchingNextPage, status } =
    useInfiniteQuery({
      queryKey: ['orders', userId, orderStatus],
      queryFn: ({ pageParam = 1 }) => fetchOrderHistory({ pageNumber: pageParam, status: orderStatus }), // Modify this line
      initialPageParam: 1,
      getNextPageParam: (lastPage) => {
        return lastPage?.hasNextPage ? lastPage?.pageNumber + 1 : undefined
      },
    })

  return { data, isError, isLoading, fetchNextPage, hasNextPage, isFetching, isFetchingNextPage, status }
}