import { postService } from '@/services/post.service'
import { useQuery } from '@tanstack/react-query'

export const usePostInfo = ({ params }: { params: { id: string } }) => {
  const { data, isLoading, isError } = useQuery({
    queryKey: ['post', params.id],
    queryFn: async () => {
      const postInfo = await postService.getPostInfo({
        postId: params.id,
      })

      return postInfo
    },
  })

  return { data, isLoading, isError } as {
    data: Post
    isLoading: boolean
    isError: boolean
  }
}
