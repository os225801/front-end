import { postService } from '@/services/post.service'
import { usePostStoreActions } from '@/store/post.store'
import { useInfiniteQuery, useMutation, useQuery, useQueryClient } from '@tanstack/react-query'

export function usePostQueryById(postId: string) {
  const queryFn = async () => {
    const posts = await postService.getPostById(postId)
    return posts as PostRes
  }

  const queryKey = ['post', postId]

  return useQuery({
    queryFn,
    queryKey,
  })
}

// export default usePostQuery

const fetchPosts = async ({
  pageParam,
  tagIds,
  sortBy,
  sortOrder,
  isBuyable,
}: {
  pageParam: number
  tagIds?: string | null
  sortBy: string
  sortOrder: string
  isBuyable: boolean
}) => {
  const posts = await postService.getAllPosts({
    page: pageParam,
    size: 20,
    keyword: '',
    sortBy: sortBy,
    sortOrder: sortOrder,
    tagIds: tagIds ? tagIds : '',
    isBuyable: isBuyable,
  })
  return posts
}

export const useTopPosts = () => {
  const { data, isError, isLoading } = useQuery({
    queryFn: () => fetchPosts({ pageParam: 1, sortBy: 'totalLikes', sortOrder: 'DESC', isBuyable: false }),
    queryKey: ['posts'],
  })
  return { data, isError, isLoading }
}

export const useInfinitePost = (
  params: {
    tagIds?: string | null
    sortBy?: string
    sortOrder?: string
    isBuyable?: boolean
  } = { sortBy: 'createdAt', sortOrder: 'ASC', isBuyable: false }
) => {
  const { data, isError, isLoading, fetchNextPage, hasNextPage, isFetching, isFetchingNextPage, status } =
    useInfiniteQuery({
      queryKey: ['posts', params],
      queryFn: ({ pageParam = 1 }) =>
        fetchPosts({
          pageParam,
          tagIds: params.tagIds,
          sortBy: params.sortBy!,
          sortOrder: params.sortOrder!,
          isBuyable: params.isBuyable!,
        }), // Modify this line
      initialPageParam: 1,
      getNextPageParam: (lastPage) => {
        return lastPage?.hasNextPage ? lastPage?.pageNumber + 1 : undefined
      },
    })

  return { data, isError, isLoading, fetchNextPage, hasNextPage, isFetching, isFetchingNextPage, status }
}

const likeaAndUnlikePost = async (postId: string) => {
  const data = { postId }
  const res = await postService.likeAndUnlikePost(data)
  return res
}

export const useLikeAndUnlikePost = (postId: string) => {
  const { setIsLoading } = usePostStoreActions()
  const queryClient = useQueryClient()
  const { mutateAsync: likeOrUnlike } = useMutation({
    mutationFn: likeaAndUnlikePost,
    onMutate: () => {
      setIsLoading(true)
    },
    onSuccess: () => {
      setTimeout(() => {
        setIsLoading(false)
      }, 500)
      queryClient.invalidateQueries({
        queryKey: ['posts'],
      })
      queryClient.invalidateQueries({
        queryKey: ['post', postId],
      })
    },
  })

  return { likeOrUnlike }
}

const fetchPostsByUserId = async (userId: string) => {
  const posts = await postService.getPostsByUserId({ userId })
  return posts
}

export const usePostsByUserId = (userId: string) => {
  const { data, isError, isLoading } = useQuery({
    queryFn: () => fetchPostsByUserId(userId),
    queryKey: ['posts', userId],
  })
  return { data, isError, isLoading }
}

const fetchPostsInCollection = async (collectionId: string) => {
  const posts = await postService.getPostsInCollection({ collectionId })
  return posts
}

export const usePostsInCollection = (collectionId: string) => {
  const { data, isError, isLoading } = useQuery({
    queryFn: () => fetchPostsInCollection(collectionId),
    queryKey: ['posts', collectionId],
  })
  return { data, isError, isLoading }
}

const DeletePost = async (params: { postId: string }) => {
  const res = await postService.deletePost(params.postId)

  return res
}

export const useDeletePost = () => {
  const queryClient = useQueryClient()
  const { mutateAsync, isPending, isSuccess, isError } = useMutation({
    mutationFn: DeletePost,
    onSuccess: () => {
      queryClient.invalidateQueries({ queryKey: ['posts'] })
    },
  })

  return { mutateAsync, isPending, isSuccess, isError }
}
