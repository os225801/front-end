import { userService } from '@/services/user.service'
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query'

export const useProfile = ({ params }: { params: { id: string } }) => {
  const { data, isLoading, isError } = useQuery({
    queryFn: async () => {
      const user = await userService.getProfile({ userId: String(params.id) }) // Pass an object with the userId property
      return user
    },
    queryKey: ['user', params.id],
  })

  return { data, isLoading, isError } as {
    data: Profile
    isLoading: boolean
    isError: boolean
  }
}

export const useCurrentUser = () => {
  const { data, isLoading, isError } = useQuery({
    queryFn: async () => {
      const user = await userService.getCurrentUser()
      return user
    },
    queryKey: ['current-user'],
    refetchOnWindowFocus: false,
  })

  return { data, isLoading, isError } as {
    data: CurrentUser
    isLoading: boolean
    isError: boolean
  }
}

const followAndUnfollowUser = async (userId: string) => {
  const data = { userId }
  const res = await userService.followAndUnfollowUser(data)
  return res
}

export const useFollowAndUnfollowUser = () => {
  const queryClient = useQueryClient()
  const { mutateAsync: followAndUnfollow, isPending } = useMutation({
    mutationFn: followAndUnfollowUser,
    onSuccess: () => {
      queryClient.invalidateQueries({
        queryKey: ['user'],
      })
    },
  })

  return { followAndUnfollow, isPending }
}
