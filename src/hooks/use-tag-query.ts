import { postService } from '@/services/post.service'
import { useInfiniteQuery, useQuery } from '@tanstack/react-query'

export const useTags = (params: GetTagOptions) => {
  const { data, isLoading, isError } = useQuery({
    queryFn: async () => {
      const tags = await postService.getTags(params)
      return tags
    },
    queryKey: ['tags', params],
  })

  return { data, isLoading, isError } as {
    data: TagsRes
    isLoading: boolean
    isError: boolean
  }
}

async function fetchTags({ pageParam, params }: { pageParam: number; params: Pick<GetTagOptions, 'searchTerm'> }) {
  const tags = await postService.getTags({
    page: pageParam,
    size: 10,
    searchTerm: params.searchTerm,
  })
  return tags
}

export const useInfiniteTags = (params: Pick<GetTagOptions, 'searchTerm'>) => {
  const { data, isError, isLoading, fetchNextPage, hasNextPage, isFetching, isFetchingNextPage, status } =
    useInfiniteQuery({
      queryKey: ['tags', params],
      queryFn: ({ pageParam = 1 }) => fetchTags({ pageParam, params }),
      initialPageParam: 1,
      getNextPageParam: (lastPage) => {
        return lastPage?.hasNextPage ? lastPage?.pageNumber + 1 : undefined
      },
      enabled: params.searchTerm.trim() !== '',
    })

  return { data, isError, isLoading, fetchNextPage, hasNextPage, isFetching, isFetchingNextPage, status }
}
