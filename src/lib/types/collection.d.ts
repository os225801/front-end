interface CreateCollectionForm {
  title: string
  description: string
}

interface Collection {
  id: string
  name: string
  description: string
  totalPosts: number
  coverImage: string
  status: boolean
}

interface CollectionPaginated {
  id: string
  name: string
  coverImage: {
    imageLink: string
  }[]
}

interface Collections {
  items: Collection[]
}

interface CollectionsPaginated {
  items: CollectionPaginated[]
}

interface CollectionDetail {
  id: string
  collectionName: string
  userId: string
  description: string
  name: string
  username: number
  avatarLink: string
}
