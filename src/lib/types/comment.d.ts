type GetCommentsOptions = {
  postId: string
  pageNumber: number
  pageSize: number
}

type GetSubCommentsOptions = {
  parentId: string
  pageNumber: number
  pageSize: number
}

interface Comment {
  id: string
  content: string
  createdAt: string
  postId: string
  user: User
}
