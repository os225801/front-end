type FormFieldProps = {
  type?: string
  placeholder: string
  name: ValidFieldNames
  register: UseFormRegister<FormData>
  prefix?: string
  subfix?: string
  error: FieldError | undefined
  valueAsNumber?: boolean
  lable: string
  isDisable?: boolean
}
