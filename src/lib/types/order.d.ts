interface Order {
  id: string
  orderDate: string
  order_date: string
  amount: number
  status: number
  buyerId: string
  sellerId: string
  artworkId: string
  image: string
  orderName: string
  postId: string
  transferContent: string
  paymentLink: string
}
