/* eslint-disable no-unused-vars */
/* eslint-disable @typescript-eslint/no-explicit-any */
type TPaginatedOptions = {
  queryKey: QueryKey
  params?: Record<string, unknown>
  defaultLimit?: number
  fetchData: (params: Record<string, unknown>) => Promise<any>
}
