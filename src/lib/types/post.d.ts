type GetPostOptions = {
  page: number
  size: number
  keyword: string
  sortBy: string
  sortOrder: string
  tagIds?: string
  isBuyable: boolean
}

type GetPostInfoOptions = {
  postId: string
}

interface Artwork {
  artworkId: string
  image: string
  type: number
  isBuyable: boolean
}

interface Post {
  id: string
  title: string
  description: string
  totalLikes: number
  createdAt: string
  price: number
  user: User
  views: number
  artwork: Artwork
  tags: PostTag[]
  isLiked: boolean
  isDownloaded: boolean
}

interface Posts {
  items: Post[]
}

interface CreatePostForm {
  isBuyable: boolean
  price: number
  tags: TagInput[]
  title: string
  description: string
  image: File[]
}

interface CreatePostPayload {
  title: string
  description: string
  price: number
  isBuyable: boolean
  artworkId: string
  tagIds: string[]
}

interface UpdatePostForm {
  isBuyable: boolean
  price: number
  tags: TagInput[]
  title: string
  description: string
}

interface UpdatePostPayload {
  title: string
  description: string
  isBuyable: boolean
  tagIds: string[]
  price: number
}

interface PostRes {
  id: string
  title: string
  description: string
  createdAt: string
  price: number
  user: User
  artwork: Artwork
  tags: TagInput[]
  totalLikes: number
  isLiked: boolean
}
