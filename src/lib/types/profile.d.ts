interface Profile {
  id: string
  username: string
  name: string
  dateOfBirth: string
  avatarLink: string
  bannerLink: string
  totalFollowers: number
  isFollowed: boolean
  totalFollowings: number
}

interface CurrentUser {
  id: string
  username: string
  name: string
  dateOfBirth: string
  avatarLink: string
  bannerLink: string
  accountNumber: string
  bankName: string
  accountName: string
}

interface UpdateProfileForm {
  name: string
  accountNumber: string
  accountName: string
  bankName: string
}
