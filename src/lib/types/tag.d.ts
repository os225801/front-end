type GetTagOptions = {
  page: number
  size: number
  searchTerm: string
}

// Define the type for an individual item
interface Tag {
  id: string
  tagName: string
  tagDescription: string
}

interface PostTag {
  id: string
  tagName: string
}

// Define the type for the data response
interface Tags {
  items: Tag[]
  totalCount: number
  hasNextPage: boolean
  hasPrevPage: boolean
  totalPages: number
  pageNumber: number
  pageSize: number
  sortBy: string
  sortOrder: string
}

interface TagRes {
  id: string
  tag_name: string
  tag_description: string
}

interface TagInput {
  id: string
  tagName: string
}

interface TagsRes {
  items: {
    id: string
    tagDescription: string
    tagName: string
  }[]
  totalCount: number
  hasNextPage: boolean
  hasPrevPage: boolean
  totalPages: number
  pageNumber: number
  pageSize: number
  sortBy: string
  sortOrder: string
}
