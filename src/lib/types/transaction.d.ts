interface TransactionsHistoryParam {
  page: number
  size: number
  soryBy: string
  sortOrder: 'DESC' | 'ACS'
  keyword: string
  value?: number | string
}

interface TransactionsHistoryRes {
  amount: number
  id: string
  orderId: string
  status: number
  transactionDate: string
  type: number
  userId: string
}

interface Transaction {
  id: string
  type: number
  status: number
  transactionDate: string
  amount: number
  userId: string
}
