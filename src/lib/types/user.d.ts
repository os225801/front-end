interface User {
  userId: string
  username: string
  name: string
  avatar: string
}

interface SessionData {
  userId: string | null
  role: string | null
  userToken: string | null
}
