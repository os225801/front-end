export function formatDate(isoString: string) {
  // Create a Date object from the string
  const date = new Date(isoString)

  // Define an array of month names
  const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

  // Format the date to the desired format
  const formattedDate = `${monthNames[date.getUTCMonth()]} ${date.getUTCDate()}, ${date.getUTCFullYear()}`

  return formattedDate
}
