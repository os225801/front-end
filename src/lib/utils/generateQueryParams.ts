/**
 *
 * @param {T} payload The request parametter
 * @return {string}
 */
export function generateQueryParams(payload: any): string {
  const keys = Object.keys(payload)
  /**
   * Looking at the RFC-3986 standard for URIs there's the following information in 2.3
   * https://github.com/axios/axios/issues/1111
   */
  return keys.map((key, i) => `${i === 0 ? '?' : ''}${[key]}=${encodeURIComponent(payload[key])}`).join('&')
}
