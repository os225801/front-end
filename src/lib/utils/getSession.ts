export const isClient = () => {
  return typeof window !== 'undefined' && typeof window.localStorage !== 'undefined'
}

export const getSession = () => {
  if (isClient()) {
    const userId = localStorage.getItem('userId')
    const role = localStorage.getItem('role')
    const userToken = localStorage.getItem('userToken')
    const user = localStorage.getItem('user')

    return { userId, role, userToken, user }
  }
  return { userId: null, role: null, userToken: null, user: null }
}
