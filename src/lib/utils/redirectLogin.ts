export const redirectLogin = () => {
  let currentUrl = ''
  if (typeof window !== 'undefined') {
    currentUrl = window.location.href
  }
  window.location.href = `${process.env.NEXT_PUBLIC_BASE_URL}/login?redirect=${currentUrl}`
}
