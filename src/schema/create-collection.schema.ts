import { z, ZodType } from 'zod'

export const CreateCollectionSchema: ZodType<CreateCollectionForm> = z.object({
  title: z.string().min(1, { message: 'Title can not be empty' }),
  description: z.string().min(1, { message: 'Description can not be empty' }),
})
