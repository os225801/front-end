import { z, ZodType } from 'zod' // Add new import

export const CreatePostSchema: ZodType<CreatePostForm> = z
  .object({
    title: z
      .string()
      .min(1, { message: 'Title can not be empty' })
      .max(50, { message: 'Title can not greater than 50 characters' }),
    description: z
      .string()
      .min(1, { message: 'Description can not be empty' })
      .max(200, { message: 'Description can not greater than 50 characters' }),
    tags: z
      .array(
        z.object({
          id: z.string(),
          tagName: z.string(),
        })
      )
      .nonempty({
        message: 'Tags Can not be empty !',
      }),
    price: z.number({ message: 'Please enter your price' }).int({ message: 'Please enter a number !' }),
    image: z.instanceof(File).array().nonempty({
      message: 'Image can not be empty !',
    }),
    isBuyable: z.boolean(),
  })
  .refine(
    (z) => {
      if (z.isBuyable === true && z.price < 9999) {
        return false
      } else {
        return true
      }
    },
    { message: 'Price must be greater than 10,000 VND', path: ['price'] }
  )
