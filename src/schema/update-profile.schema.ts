import { z, ZodType } from 'zod'

export const UpdateProfileSchema: ZodType<UpdateProfileForm> = z.object({
  name: z.string().min(1, { message: 'Name can not be empty' }),
  accountNumber: z.string().min(1, { message: 'Account Number can not be empty' }),
  accountName: z.string().min(1, { message: 'Account Name can not be empty' }),
  bankName: z.string().min(1, { message: 'Bank Name can not be empty' }),
})
