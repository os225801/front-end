import { walletService } from '@/services/wallet.service'
import { z, ZodType } from 'zod'
//
export const WithdrawSchema: ZodType<WithdrawForm> = z
  .object({
    amount: z
      .number({ message: 'Please enter a number !' })
      .int({ message: 'Please enter a number !' })
      .min(100000, 'Please enter number greater than 100,000 VND'),
  })
  .refine(
    async (x) => {
      const data = await walletService.getWallet()

      if (x.amount > data.balance) {
        return false
      } else {
        return true
      }
    },
    { message: 'Your wallet is not enought money to withdraw !!!', path: ['amount'] }
  )
