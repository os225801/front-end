import axiosClient from './axios-client'

export const artworkService = {
  uploadArtWork: async (artwork: File) => {
    const formData = new FormData()
    formData.append('image', artwork)
    return (await axiosClient.post('/artworks', formData)).data
  },
  uploadArtWorkV2: async (chunk: Blob, chunkIndex: number, totalChunks: number) => {
    const formData = new FormData()
    formData.append('chunk', chunk)
    formData.append('chunkIndex', `${chunkIndex}`)
    formData.append('totalChunks', `${totalChunks}`)
    return (await axiosClient.post('/artworks/v2', formData)).data
  },

  deleteArtwork: async (artworkId: string) => {
    return (await axiosClient.delete(`/artworks/${artworkId}`)).data
  },

  downloadArtwork: async (artworkId: string, filename: string) => {
    const data = await axiosClient.get(`/artworks/${artworkId}/download`, { responseType: 'blob' })
    const url = URL.createObjectURL(data as unknown as Blob)
    const link = document.createElement('a')
    link.setAttribute('href', url)
    link.setAttribute('download', `${filename}.jpg`)
    link.style.display = 'none'
    document.body.appendChild(link)

    link.click()

    document.body.removeChild(link)
    URL.revokeObjectURL(url)
  },
}
