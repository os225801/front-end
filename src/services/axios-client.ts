/* eslint-disable @typescript-eslint/no-explicit-any */
// import axios from 'axios'

// const baseUrl: string = String(process.env.NEXT_PUBLIC_API_ENDPOINT)
// export const baseUrlService: string = String(process.env.NEXT_PUBLIC_API_ENDPOINT_SERVICE)

// const axiosClient = axios.create({
//   baseURL: baseUrl,
//   headers: { 'Content-Type': 'application/json' },
//   timeout: 300000,
// })

// axiosClient.interceptors.request.use(async (config) => {
//   let token = store.getState().auth.accessToken

//   if (token) {
//     if (isTokenExpired(token)) {
//       const refreshToken = localStorage.getItem(REFRESH_TOKEN_KEY)
//       if (refreshToken) {
//         try {
//           const newToken = (await authService.refreshToken(refreshToken)).accessToken
//           token = newToken
//           store.dispatch(updateAccessToken(newToken))
//           config.headers['Authorization'] = `Bearer ${token}`
//         } catch (err) {
//           console.log('err when refresh token: ', err)
//         }
//       }
//     } else {
//       if (!config.headers['Authorization']) {
//         config.headers['Authorization'] = `Bearer ${token}`
//       }
//     }
//   }

//   return config
// })

// export default axiosClient

import { getSession } from '@/lib/utils/getSession'
import axios, { AxiosResponse } from 'axios'
import { v4 } from 'uuid'

const defaultHeader = {
  'Access-Control-Allow-Origin': '*',
  // 'Content-Type': 'application/json',
  // Accept: 'application/json',
  'X-Request-ID': v4(),
}

const baseURL: string = String(process.env.NEXT_PUBLIC_BASE_URL)

const axiosClient = axios.create({
  baseURL: baseURL,
  headers: defaultHeader,
})

// Add a request interceptor
axiosClient.interceptors.request.use(
  async (config) => {
    const session = getSession()

    if (session?.userToken) {
      config.headers['Authorization'] = `${session?.userToken}`
    }
    return config
  },
  (error) => {
    Promise.reject(error)
  }
)

//Add a response interceptor
axiosClient.interceptors.response.use(
  async (response) => {
    return handleResponse(response)
  },
  async (error) => {
    if (error.response.status === 403 || error.response.status === 401) {
      localStorage.clear()
      window.location.href = '/'
    }
    throw error
  }
)

const handleResponse = (res: AxiosResponse<any>) => {
  if (res && res.data) {
    return res.data
  }
  return res
}

export default axiosClient
