import axiosClient from './axios-client'

export const collectionService = {
  getCollectionsByUserId: async (params: { userId: string }) =>
    (await axiosClient.get(`/collections/users/${params.userId}`)).data,

  getCollections: async (params: { postId: string }) =>
    (await axiosClient.get(`/collections/posts/${params.postId}`)).data,

  createCollection: async (params: { data: { name: string; description: string } }) =>
    (await axiosClient.post('/collections', params)).data,

  addOrRemovePostFromCollection: async (params: { collectionId: string; postId: string }) =>
    (
      await axiosClient.put(`/collections/${params.collectionId}/posts`, {
        data: { postId: params.postId },
      })
    ).data,

  getCollectionDetail: async (params: { collectionId: string }) =>
    (await axiosClient.get(`/collections/${params.collectionId}`)).data,

  deleteCollection: async (params: { collectionId: string }) =>
    (await axiosClient.delete(`/collections/${params.collectionId}`)).data,

  updateCollection: async (params: { collectionId: string; data: { name: string; description: string } }) => {
    return await axiosClient.put(`/collections/${params.collectionId}`, {
      data: params.data,
    })
  },
}
