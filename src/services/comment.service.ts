import axiosClient from './axios-client'

export const commentService = {
  getComments: async (params: GetCommentsOptions) =>
    (await axiosClient.get(`/comments/posts/${params.postId}?page=${params.pageNumber}&size=${params.pageSize}`)).data,
  getSubComments: async (params: GetSubCommentsOptions) =>
    (
      await axiosClient.get(
        `/comments/parent-comments/${params.parentId}?page=${params.pageNumber}&size=${params.pageSize}`
      )
    ).data,
  deleteComment: async (params: { commentId: string }) =>
    (await axiosClient.delete(`/comments/${params.commentId}`)).data,
  createComment: async (params: { postId: string; content: string; parentCommentId?: string }) =>
    (await axiosClient.post(`/comments`, { data: params })).data,
}
