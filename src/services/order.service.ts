import axiosClient from './axios-client'

export const orderService = {
  createOrder: async (artworkId: string) =>
    (await axiosClient.post(`/orders`, { data: { artworkId: artworkId } })).data,

  getOrderById: async (orderId: string) => (await axiosClient.get(`/orders/${orderId}`)).data,
  getOrderHistory: async (params: { pageNumber: number; pageSize: number; status: number }) =>
    (
      await axiosClient.get(
        `/users/orders?page=${params.pageNumber}&size=${params.pageSize}&sortBy=orderDate&sortOrder=DESC&status=${params.status}`
      )
    ).data,
}
