import axiosClient from './axios-client'

export const postService = {
  getAllPosts: async (params: GetPostOptions) =>
    (
      await axiosClient.get(
        `/posts?page=${params.page}&size=${params.size}&keyword=${params.keyword}&sortBy=${params.sortBy}&sortOrder=${params.sortOrder}&tagIds=${params.tagIds}&isBuyable=${params.isBuyable}`
      )
    ).data,

  getPostById: async (id: string) => (await axiosClient.get(`/posts/${id}`)).data,

  getTags: async (params: GetTagOptions) =>
    (await axiosClient.get(`/tags?page=${params.page}&size=${params.size}&searchTerm=${params.searchTerm}`)).data,

  likeAndUnlikePost: async (params: { postId: string }) =>
    (await axiosClient.post(`/posts/${params.postId}/likes`)).data,

  updateViewCount: async (params: { postId: string }) => (await axiosClient.post(`/posts/${params.postId}/views`)).data,

  getPostsByUserId: async (params: { userId: string }) => (await axiosClient.get(`/posts/users/${params.userId}`)).data,

  createPost: async (payload: CreatePostPayload) => (await axiosClient.post('/posts', { data: payload })).data,

  getPostInfo: async (params: GetPostInfoOptions) => (await axiosClient.get(`/posts/${params.postId}`)).data,

  deletePost: async (postId: string) => await axiosClient.delete(`/posts/${postId}`),

  updatePost: async (postId: string, payload: UpdatePostPayload) =>
    await axiosClient.put(`/posts/${postId}`, { data: payload }),

  getPostsInCollection: async (params: { collectionId: string }) =>
    (await axiosClient.get(`/posts/collections/${params.collectionId}`)).data,
}
