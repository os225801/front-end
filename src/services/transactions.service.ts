import { generateQueryParams } from '@/lib/utils/generateQueryParams'
import axiosClient from './axios-client'

export const transactionService = {
  getTransactionById: async (transactionId: string) =>
    (await axiosClient.get(`/transactions/${transactionId}`)).data,

  withdraw: async (amount: number) => (await axiosClient.post(`/transactions`, { data: { amount } })).data,

  getTransactionHistory: async (transactionsHistoryParam: TransactionsHistoryParam) =>
    (await axiosClient.get(`/users/transactions${generateQueryParams(transactionsHistoryParam)}`)).data,
}
