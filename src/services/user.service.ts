import axiosClient from './axios-client'

export const userService = {
  getProfile: async (params: Pick<User, 'userId'>) => (await axiosClient.get(`/users/${params.userId}`)).data,
  getCurrentUser: async () => (await axiosClient.get('/users/current-user')).data,
  followAndUnfollowUser: async (params: { userId: string }) =>
    (await axiosClient.post(`/users/${params.userId}/follows`)).data,
  logOut: async () => (await axiosClient.get('/logout')).data,
  updateProfilePicture: async (params: FormData) => (await axiosClient.put('/users/image', params)).data,
  updateProfileInfo: async (params: UpdateProfileForm) =>
    (
      await axiosClient.put('/users/info', {
        data: params,
      })
    ).data,
}
