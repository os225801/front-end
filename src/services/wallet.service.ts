import axiosClient from './axios-client'

export const walletService = {
  getWallet: async () => (await axiosClient.get(`/wallets`)).data,
}
