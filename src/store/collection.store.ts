/* eslint-disable no-unused-vars */
'use client'

import { create } from 'zustand'

interface CollectionStore {
  isOpenCollectionModal: boolean
  isOpenUpdateCollectionModal: boolean
  postIdToBeAdded: string
  formCreateCollection: boolean
  actions: CollectionStoreActions
}

interface CollectionStoreActions {
  setIsOpenCollectionModal: (isOpenCollectionModal: boolean) => void
  setIsOpenUpdateCollectionModal: (idOpenUpdateCollectionModal: boolean) => void
  setPostIdToBeAdded: (postId: string) => void
  setFormCreateCollection: (formCreateCollection: boolean) => void
}

const usePostStore = create<CollectionStore>((set) => ({
  isOpenCollectionModal: false,
  isOpenUpdateCollectionModal: false,
  postIdToBeAdded: '',
  formCreateCollection: false,
  actions: {
    setIsOpenCollectionModal: (isOpenCollectionModal: boolean) => set({ isOpenCollectionModal }),
    setIsOpenUpdateCollectionModal: (isOpenUpdateCollectionModal: boolean) => set({ isOpenUpdateCollectionModal }),
    setPostIdToBeAdded: (postId: string) => set({ postIdToBeAdded: postId }),
    setFormCreateCollection: (formCreateCollection: boolean) => set({ formCreateCollection }),
  },
}))

export const useCollectionStoreActions = () => usePostStore((state) => state.actions)
export const useCollectionIsOpenCollectionModal = () => usePostStore((state) => state.isOpenCollectionModal)
export const usePostIdToBeAdded = () => usePostStore((state) => state.postIdToBeAdded)
export const useFormCreateCollection = () => usePostStore((state) => state.formCreateCollection)
export const useIsOpenUpdateCollectionModal = () => usePostStore((state) => state.isOpenUpdateCollectionModal)
