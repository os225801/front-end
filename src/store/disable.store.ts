/* eslint-disable no-unused-vars */
'use client'

import { create } from 'zustand'

interface DisableStore {
  isDisable: boolean
  actions: DisableStoreAction
}

interface DisableStoreAction {
  setIsDisable: (isDisable: boolean) => void
}

const useDisableStore = create<DisableStore>((set) => ({
  isDisable: false,
  actions: {
    setIsDisable: (isDisable: boolean) => set({ isDisable }),
  },
}))

export const useIsDisableStoreActions = () => useDisableStore((state) => state.actions)
export const useIsDisable = () => useDisableStore((state) => state.isDisable)
