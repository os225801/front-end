/* eslint-disable no-unused-vars */
'use client'

import { create } from 'zustand'

interface PostStore {
  isLoading: boolean
  actions: PostStoreActions
}

interface PostStoreActions {
  setIsLoading: (isLoading: boolean) => void
}

const usePostStore = create<PostStore>((set) => ({
  isLoading: false,
  actions: {
    setIsLoading: (isLoading: boolean) => set({ isLoading }),
  },
}))

export const usePostStoreActions = () => usePostStore((state) => state.actions)
export const usePostIsLoading = () => usePostStore((state) => state.isLoading)
