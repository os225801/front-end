/* eslint-disable no-unused-vars */
'use client'

import { create } from 'zustand'

interface ProfileStore {
  id: string
  isOpenEditProfileModal: boolean
  actions: ProfileStoreActions
}

interface ProfileStoreActions {
  setId: (id: string) => void
  setIsOpenEditProfileModal: (isOpenEditProfileModal: boolean) => void
}

const useProfileStore = create<ProfileStore>((set) => ({
  id: '',
  isOpenEditProfileModal: false,
  actions: {
    setId: (id: string) => set({ id }),
    setIsOpenEditProfileModal: (isOpenEditProfileModal: boolean) => set({ isOpenEditProfileModal }),
  },
}))

export const useProfileStoreActions = () => useProfileStore((state) => state.actions)
export const useProfileId = () => useProfileStore((state) => state.id)
export const useIsOpenEditProfileModal = () => useProfileStore((state) => state.isOpenEditProfileModal)
