/* eslint-disable no-unused-vars */
'use client'

import { create } from 'zustand'

interface TagStore {
  id: string
  tagName: string
  tagNameShow: string
  actions: TagStoreActions
}

interface TagStoreActions {
  setId: (id: string) => void
  setTagName: (tagName: string) => void
  setTagNameShow: (tagName: string) => void
}

const useTagStore = create<TagStore>((set) => ({
  id: '',
  tagName: '',
  tagNameShow: '',
  actions: {
    setId: (id: string) => set({ id }),
    setTagName: (tagName: string) => set({ tagName }),
    setTagNameShow: (tagName: string) => set({ tagNameShow: tagName }),
  },
}))

export const useTagStoreActions = () => useTagStore((state) => state.actions)
export const useTagId = () => useTagStore((state) => state.id)
export const useTagName = () => useTagStore((state) => state.tagName)
export const useTagNameShow = () => useTagStore((state) => state.tagNameShow)
