/* eslint-disable no-unused-vars */
'use client'

import { create } from 'zustand'

interface WithdrawStore {
  isOpenWithDrawModel: boolean
  actions: WithdrawStoreAction
}

interface WithdrawStoreAction {
  setIsOpenWithDrawModel: (isOpenWithDrawModel: boolean) => void
}

const useWithdrawStore = create<WithdrawStore>((set) => ({
  isOpenWithDrawModel: false,
  actions: {
    setIsOpenWithDrawModel: (isOpenWithDrawModel: boolean) => set({ isOpenWithDrawModel }),
  },
}))

export const useWithdrawStoreActions = () => useWithdrawStore((state) => state.actions)
export const useWithdrawIsOpenWithDrawModel = () => useWithdrawStore((state) => state.isOpenWithDrawModel)
